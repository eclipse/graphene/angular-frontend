# Use an official Node.js runtime as a parent image
FROM node:18-alpine as build

# Set the working directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy the Angular app files to the working directory
COPY . .

# Build the Angular app
#RUN npm run ng build --configuration=prod

RUN npm run ng -- build --configuration=dev02


# Use a smaller base image for the final image
FROM nginx:alpine

# Copy the built app from the 'build' stage to the current stage
COPY --from=build /usr/src/app/dist/graphene-ui/browser /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Start Nginx
CMD ["nginx", "-g", "daemon off;"]
