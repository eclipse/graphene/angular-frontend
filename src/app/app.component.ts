import { Component } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  standalone: true,
  imports: [RouterModule],
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'graphene-ui';

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.checkRememberMeAndRedirect();
  }

  checkRememberMeAndRedirect() {
    const userDetails = localStorage.getItem('userDetails');

    if (userDetails) {
      // User details are available, redirect to the dashboard
      this.router.navigate(['/dashboard']);
    }
  }
}
