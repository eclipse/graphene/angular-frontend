import { Routes } from '@angular/router';
import { LandingPageComponent } from './features/landing-page/landing-page.component';
import { authGuard } from './core/services/auth/auth-guard.service';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { MarketplaceComponent } from './features/marketplace/marketplace.component';
import { HomeComponent } from './shared/components/home/home.component';
import { ModelDetailsComponent } from './features/model-details/model-details.component';
import { ModelDetailsDescriptionComponent } from './shared/components/model-details-description/model-details-description.component';
import { ModelDetailsSignatureComponent } from './shared/components/model-details-signature/model-details-signature.component';
import { ModelDetailsDocumentsComponent } from './shared/components/model-details-documents/model-details-documents.component';
import { ModelDetailsAuthorPublisherComponent } from './shared/components/model-details-author-publisher/model-details-author-publisher.component';
import { ModelDetailsArtifactsComponent } from './shared/components/model-details-artifacts/model-details-artifacts.component';
import { ModelDetailsLicenseProfileComponent } from './shared/components/model-details-license-profile/model-details-license-profile.component';
import { CatalogsComponent } from './features/dashboard/catalogs/catalogs.component';
import { MyModelsComponent } from './features/dashboard/my-models/my-models.component';
import { OnBoardingModelComponent } from './features/dashboard/on-boarding-model/on-boarding-model.component';
import { DesignStudioComponent } from './features/dashboard/design-studio/design-studio.component';
import { SiteAdminComponent } from './features/dashboard/site-admin/site-admin.component';
import { ManageLicenseComponent } from './features/dashboard/manage-license/manage-license.component';
import { PublishRequestComponent } from './features/dashboard/publish-request/publish-request.component';
import { QAndAComponent } from './features/dashboard/q-and-a/q-and-a.component';
import { ModelManagementComponent } from './shared/components/model-management/model-management.component';
import { ShareWithTeamPageComponent } from './shared/components/share-with-team-page/share-with-team-page.component';
import { PublishToMarketplacePageComponent } from './shared/components/publish-to-marketplace-page/publish-to-marketplace-page.component';
import { ManagePublisherAuthorsPageComponent } from './shared/components/manage-publisher-authors-page/manage-publisher-authors-page.component';
import { DeleteModelPageComponent } from './shared/components/delete-model-page/delete-model-page.component';

const routeConfig: Routes = [
  {
    path: '',
    component: LandingPageComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'marketplace', component: MarketplaceComponent },
      {
        path: 'marketSolutions/model-details/:solutionId/:revisionId',
        component: ModelDetailsComponent,
        children: [
          {
            path: 'modelDescription',
            component: ModelDetailsDescriptionComponent,
          },
          {
            path: 'modelSignature',
            component: ModelDetailsSignatureComponent,
          },
          {
            path: 'modelDocuments',
            component: ModelDetailsDocumentsComponent,
          },
          {
            path: 'modelArtifacts',
            component: ModelDetailsArtifactsComponent,
          },
          {
            path: 'modelAuthor',
            component: ModelDetailsAuthorPublisherComponent,
          },
          {
            path: 'licenseProfile',
            component: ModelDetailsLicenseProfileComponent,
          },
          {
            path: '',
            redirectTo: 'modelDescription',
            pathMatch: 'full',
          },
        ],
      },
    ],
  },
  {
    path: 'dashboard',
    canActivate: [authGuard],
    component: DashboardComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'marketplace', component: MarketplaceComponent },
      {
        path: 'marketSolutions/model-details/:solutionId/:revisionId',
        component: ModelDetailsComponent,
        children: [
          {
            path: 'modelDescription',
            component: ModelDetailsDescriptionComponent,
          },
          {
            path: 'modelSignature',
            component: ModelDetailsSignatureComponent,
          },
          {
            path: 'modelDocuments',
            component: ModelDetailsDocumentsComponent,
          },
          {
            path: 'modelArtifacts',
            component: ModelDetailsArtifactsComponent,
          },
          {
            path: 'modelAuthor',
            component: ModelDetailsAuthorPublisherComponent,
          },
          {
            path: 'licenseProfile',
            component: ModelDetailsLicenseProfileComponent,
          },
          {
            path: '',
            redirectTo: 'modelDescription',
            pathMatch: 'full',
          },
        ],
      },
      { path: 'catalogs', component: CatalogsComponent },
      { path: 'myModels', component: MyModelsComponent },
      { path: 'onBoardingModel', component: OnBoardingModelComponent },
      { path: 'designStudio', component: DesignStudioComponent },
      { path: 'siteAdmin', component: SiteAdminComponent },
      { path: 'manageLicense', component: ManageLicenseComponent },
      { path: 'publishRequest', component: PublishRequestComponent },
      { path: 'qAndA', component: QAndAComponent },
      {
        path: 'manageMyModel/:solutionId/:revisionId',
        component: ModelManagementComponent,
        children: [
          { path: 'shareWithTeam', component: ShareWithTeamPageComponent },
          {
            path: 'publisherAuthors',
            component: ManagePublisherAuthorsPageComponent,
          },
          {
            path: 'publishModel',
            component: PublishToMarketplacePageComponent,
          },
          { path: 'deleteModel', component: DeleteModelPageComponent },
          {
            path: '',
            redirectTo: 'shareWithTeam',
            pathMatch: 'full',
          },
        ],
      },
    ],
  },

  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

export default routeConfig;
