import { Directive, Input } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn,
} from '@angular/forms';

@Directive({
  selector: '[gpNameValidation]',
  standalone: true,
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: NameValidationDirective,
      multi: true,
    },
  ],
})
export class NameValidationDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    const viewValue = control.value;
    const isBlank = viewValue === '';
    const invalidChars = !isBlank && !/^[A-z0-9 ]+$/.test(viewValue);
    return invalidChars ? { invalidChars: true } : null;
  }
}
