import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: '[gpMatchingPassword]',
  standalone: true,
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: MatchingPasswordDirective,
      multi: true,
    },
  ],
})
export class MatchingPasswordDirective implements Validator {
  @Input() gpMatchingPassword!: string;
  validate(control: AbstractControl): { [key: string]: any } | null {
    const controlToCompare = control.parent?.get(this.gpMatchingPassword);
    if (controlToCompare && controlToCompare.value !== control.value) {
      return { notEqual: true };
    }

    return null;
  }
}
