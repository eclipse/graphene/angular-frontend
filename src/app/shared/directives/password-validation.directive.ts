import { Directive, Input } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';

@Directive({
  selector: '[gpPasswordValidation]',
  standalone: true,
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: PasswordValidationDirective,
      multi: true,
    },
  ],
})
export class PasswordValidationDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    const validations: ValidationErrors = {};

    const viewValue = control.value;
    const isBlank = viewValue === '';
    const invalidChars = !isBlank && !/^[A-z0-9 ]+$/.test(viewValue);

    var invalidLen = !isBlank && !invalidChars && viewValue.length < 8;

    var isWeak =
      !isBlank &&
      !invalidLen &&
      !/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z])(?=.*[^a-zA-Z0-9])/.test(
        viewValue,
      );
    var isLowerCaseLetter =
      !isBlank && !invalidLen && !/(?=.*[a-z])/.test(viewValue);
    var isUpperCaseLetter =
      !isBlank && !invalidLen && !/(?=.*[A-Z])/.test(viewValue);
    var isDigit = !isBlank && !invalidLen && !/(?=.*[0-9])/.test(viewValue);
    var isSpecialChar =
      !isBlank && !invalidLen && !/(?=.*[^a-zA-Z0-9])/.test(viewValue);
    if (invalidChars) {
      validations['invalidChars'] = true;
    }

    if (invalidLen) {
      validations['invalidLen'] = true;
    }

    if (isWeak) {
      validations['isWeak'] = true;
    }

    if (isLowerCaseLetter) {
      validations['isLowerCaseLetter'] = true;
    }

    if (isUpperCaseLetter) {
      validations['isUpperCaseLetter'] = true;
    }

    if (isDigit) {
      validations['isDigit'] = true;
    }

    if (isSpecialChar) {
      validations['isSpecialChar'] = true;
    }

    return Object.keys(validations).length > 0 ? validations : null;
  }
}
