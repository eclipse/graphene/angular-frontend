import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn,
} from '@angular/forms';

@Directive({
  selector: '[gpEmailIdValidation]',
  standalone: true,
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: EmailIDValidationDirective,
      multi: true,
    },
  ],
})
export class EmailIDValidationDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return emailIdValidator(control);
  }
}
export const emailIdValidator: ValidatorFn = (
  control: AbstractControl,
): ValidationErrors | null => {
  const viewValue = control.value;
  const isBlank = viewValue === '';
  const emailPattern = /^[^\s@]+@[^\s@]+\.[a-zA-Z]{2,}$/;
  const validPattern = !isBlank && emailPattern.test(viewValue);
  return validPattern ? null : { invalidPattern: true };
};
