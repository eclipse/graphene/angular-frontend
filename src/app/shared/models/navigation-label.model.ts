export interface NavigationLabelModel {
  label: string;
  disabled?: boolean;
}
