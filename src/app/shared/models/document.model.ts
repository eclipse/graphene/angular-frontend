export interface DocumentModel {
  created: string;
  modified: string;
  documentId: string;
  name: string;
  version: string;
  uri: string;
  size: number;
  userId: string;
}
