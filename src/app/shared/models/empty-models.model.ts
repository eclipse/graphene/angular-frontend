import { CatalogFilter } from './filter.model';
import { LicenseProfileModel } from './public-solution.model';

/* export const emptyModelDetails = (): ModelDetails => ({
  solution: {} as PublicSolutionDetailsModel,
  revision: {
    version: '',
    revisionId: '',
    onBoarded: '',
  },
  description: '',
  licenseProfile: {} as LicenseProfileModel,
  category: {} as CatalogFilter,
  toolkitType: {} as CatalogFilter,
  image: null,
  tags: [],
  documents: [],
  authors: [],
});
 */

export const emptyLicenseProfileModel: LicenseProfileModel = {
  $schema: '',
  companyName: '',
  contact: {
    name: '',
    URL: '',
    email: '',
  },
  copyright: {
    year: 2021,
    company: '',
    suffix: '',
  },
  intro: '',
  keyword: '',
  licenseName: '',
  rtuRequired: false,
  softwareType: '',
  additionalInfo: '',
};
