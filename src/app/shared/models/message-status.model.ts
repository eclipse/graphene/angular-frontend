export interface MessageStatusModel {
  taskId: string;
  stepResultId: string;
  trackingId: string;
  stepCode: string;
  solutionId: string;
  revisionId: string;
  artifactId: string;
  userId: string;
  name: string;
  statusCode: string;
  result: string;
  startDate: string;
  endDate: string;
}
