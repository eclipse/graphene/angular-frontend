export interface CreateRatingRequestPayload {
  rating: number;
  solutionId: string;
  textReview: string;
  userId: string;
  created?: string;
  modified?: string;
}

export interface UpdateSolutionRequestPayload {
  active: boolean;
  created: string;
  modelType: string;
  name: string;
  ownerId: string;
  solutionId: string;
  tookitType: string;
}
