export interface Catalog {
  name: string;
  accessTypeCode: string;
  catalogId?: string;
  created?: string;
  description: string;
  modified?: string;
  origin?: string;
  publisher?: string;
  selfPublish: boolean;
  url: string;
  solutionCount?: number;
  type: string;
}

export enum AccessTypeCode {
  Public = 'PB',
  Restricted = 'RS',
}
export enum CatalogType {
  LeaderBoard = 'LEADERBOARD',
  Standard = 'STANDARD',
}
