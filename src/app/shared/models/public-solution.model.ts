export interface PublicSolutionsRequestPayload {
  userId?: string;
  modelTypeCodes?: string[];
  active?: boolean;
  catalogIds?: string[];
  accessTypeCodes?: string[];
  nameKeyword?: string[];
  sortBy?: string;
  tags?: string[];
  published?: boolean;
  pageRequest?: {
    fieldToDirectionMap?: {
      [key: string]: string;
    };
    page?: number;
    size?: number;
  };
}

export interface Solution {
  name: string;
  ownerName: string;
  ownerId: string;
  ownerListForSol: { userId: string; firstName: string; lastName: string }[];
  active: boolean;
  sourceId: string;
  authors: AuthorPublisherModel[];
  mPeer: MPeer;
  modified: string;
  created: string;
  solutionRatingAvg: number;
  commentsCount: number;
  viewCount: number;
  downloadCount: number;
  selectFav: string;
  solutionId: string;
  solutionTagList: Tag[];
  latestRevisionId: string;
  modelTypeName: string;
  modelType: string;
  tookitType: string;
  tookitTypeName: string;
  catalogNames: string[];
  publisher: string;
}

export interface AuthorPublisherModel {
  name: string;
  contact: string;
}

export interface MPeer {
  name: string;
}

export interface Tag {
  tag: string;
}

export interface Revision {
  revisionId: string;
  version: string;
  onBoarded?: string;
}

export interface AllUserRating {
  created: string;
  modified: string;
  rating: number;
  solutionId: string;
  textReview: string;
  userId: string;
  userName: string;
}

export interface AverageRatings {
  downloadCount: number;
  featured: boolean;
  lastDownload: string;
  ratingAverageTenths: number;
  ratingCount: number;
  solutionId: string;
  viewCount: number;
}

export interface LicenseProfileModel {
  $schema: string;
  companyName: string;
  contact: {
    name: string;
    URL: string;
    email: string;
  };
  copyright: {
    year: number;
    company: string;
    suffix: string;
  };
  intro: string;
  keyword: string;
  licenseName: string;
  rtuRequired: boolean;
  softwareType: string;
  additionalInfo: string;
}

export interface ArtifactDownloadModel {
  artifactId: string;
  artifactType: string;
  artifactUri: string;
  created: string;
  description: string;
  mask: boolean;
  metadata: string;
  modified: string;
  name: string;
  ownerId: string;
  size: number;
  solutionId: string;
  validSolution: string;
  version: string;
}

export interface PublicSolutionDetailsRevisionModel {
  authors: AuthorPublisherModel[];
  created: string;
  metadata: string;
  modified: string;
  onboarded: string;
  origin: string;
  publisher: string;
  revisionId: string;
  solutionId: string;
  sourceId: string;
  userId: string;
  verifiedLicense: string;
  verifiedVulnerability: string;
  version: string;
}

export interface PublicSolution extends Solution {
  revisions: Revision[];
}

export interface PublicSolutionDetailsModel extends Solution {
  revisions: PublicSolutionDetailsRevisionModel[];
  catalogId?: string;
}
