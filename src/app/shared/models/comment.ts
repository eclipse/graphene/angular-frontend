export interface CommentModel {
  commentId?: string;
  created?: string;
  modified?: string;
  parentId?: string;
  stringDate?: string;
  text?: string;
  threadId: string;
  userId: string;
  url: string;
}

export interface ThreadModel {
  revisionId: string;
  solutionId: string;
  threadId: string;
  title: string;
}

export interface CommentReplyModel extends CommentModel {
  replies: CommentModel[];
}
