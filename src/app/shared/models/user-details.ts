export interface UserDetails {
  firstName: string;
  lastName?: string;
  emailId: string;
  userId?: string;
  username: string;
  password?: string;
  active?: boolean;
  lastLogin?: string;
  created?: string;
  modified?: string;
  picture?: string;
}
