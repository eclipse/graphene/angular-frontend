export interface Filter {
  name: string;
  selected: boolean;
}
export interface CatalogFilter {
  code: string;
  name: string;
}

export interface SortOption {
  name: string;
  value: string;
}

export interface FieldToDirectionMap {
  [key: string]: 'ASC' | 'DESC';
}

export enum Category {
  Classification = 'CL',
  Data_Sources = 'DS',
  Data_Transformer = 'DT',
  Prediction = 'PR',
  Regression = 'RG',
}

export enum ToolkitType {
  Data_Broker = 'BR',
  CC = 'CC',
  Collator = 'CO',
  Composite_Solution = 'CP',
  Data_Source = 'DA',
  Design_Studio = 'DS',
  H2O = 'H2',
  Initializer = 'IN',
  Java_Spark = 'JS',
  Model_Node = 'MN',
  ONAP = 'ON',
  Orchestrator = 'OR',
  ONNX = 'OX',
  Probe = 'PB',
  PFA = 'PF',
  R = 'RC',
  Shared_Folder = 'SF',
  Scikit_Learn = 'SK',
  Splitter = 'SP',
  Training_Client = 'TC',
  TensorFlow = 'TF',
}

// Mapping object for toolkit names to codes
export const toolkitTypeCodeMap: Record<string, ToolkitType> = {
  'Data Broker': ToolkitType.Data_Broker,
  'C/C++': ToolkitType.CC,
  Collator: ToolkitType.Collator,
  'Composite Solution': ToolkitType.Composite_Solution,
  'Data Source': ToolkitType.Data_Source,
  'Design Studio': ToolkitType.Design_Studio,
  H2O: ToolkitType.H2O,
  Initializer: ToolkitType.Initializer,
  'Java Spark': ToolkitType.Java_Spark,
  'Model Node': ToolkitType.Model_Node,
  ONAP: ToolkitType.ONAP,
  Orchestrator: ToolkitType.Orchestrator,
  ONNX: ToolkitType.ONNX,
  Probe: ToolkitType.Probe,
  PFA: ToolkitType.PFA,
  R: ToolkitType.R,
  'Shared Folder': ToolkitType.Shared_Folder,
  'Scikit Learn': ToolkitType.Scikit_Learn,
  Splitter: ToolkitType.Splitter,
  'Training Client': ToolkitType.Training_Client,
  TensorFlow: ToolkitType.TensorFlow,
};

// Mapping object for category names to codes
export const categoryCodeMap: Record<string, Category> = {
  Classification: Category.Classification,
  'Data Sources': Category.Data_Sources,
  'Data Transformer': Category.Data_Transformer,
  Prediction: Category.Prediction,
  Regression: Category.Regression,
};

export type CategoryCode = (typeof Category)[keyof typeof Category];
export type ToolkitTypeCode = (typeof ToolkitType)[keyof typeof ToolkitType];
