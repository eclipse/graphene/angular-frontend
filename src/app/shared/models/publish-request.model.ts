export interface PublishSolutionRequest {
  publishRequestId: number;
  solutionId: string;
  revisionId: string;
  requestUserId: string;
  requestUserName: string;
  approverId: string;
  requestStatusCode: string;
  comment: string;
  requestorName: string;
  solutionName: string;
  revisionName: string;
  revisionStatusCode: string;
  revisionStatusName: string;
  requestStatusName: string;
  creationDate: string;
  lastModifiedDate: string;
  catalogId: string;
  catalogName: string;
  accessType: string;
}
