import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicSolution } from '../../models';

@Component({
  selector: 'gp-favorite',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './favorite.component.html',
  styleUrl: './favorite.component.scss',
})
export class FavoriteComponent {
  @Input() loginUserID!: string;
  @Input() item!: PublicSolution;
  @Input() items!: PublicSolution[];
  @Input() isFavoriteSolution!: boolean;
  @Output() favoriteClicked = new EventEmitter<string>();

  onFavoriteClick(solutionId: string): void {
    this.favoriteClicked.emit(solutionId);
  }
}
