import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicSolution } from '../../models';

@Component({
  selector: 'gp-comment-count',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './comment-count.component.html',
  styleUrl: './comment-count.component.scss',
})
export class CommentCountComponent {
  @Input() item!: PublicSolution;
}
