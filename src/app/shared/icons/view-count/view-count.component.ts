import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicSolution } from '../../models';

@Component({
  selector: 'gp-view-count',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './view-count.component.html',
  styleUrl: './view-count.component.scss',
})
export class ViewCountComponent {
  @Input() item!: PublicSolution;
}
