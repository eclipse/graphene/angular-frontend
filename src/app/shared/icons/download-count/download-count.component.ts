import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicSolution } from '../../models';

@Component({
  selector: 'gp-download-count',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './download-count.component.html',
  styleUrl: './download-count.component.scss',
})
export class DownloadCountComponent {
  @Input() item!: PublicSolution;
}
