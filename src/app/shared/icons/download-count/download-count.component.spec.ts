import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadCountComponent } from './download-count.component';

describe('DownloadCountComponent', () => {
  let component: DownloadCountComponent;
  let fixture: ComponentFixture<DownloadCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DownloadCountComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DownloadCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
