import { Component, Inject, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { Alert, AlertType } from '../../models/alert.model';

@Component({
  selector: 'gp-alert',
  standalone: true,
  imports: [CommonModule, MatIconModule],
  templateUrl: './alert.component.html',
  styleUrl: './alert.component.scss',
})
export class AlertComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: Alert) {}

  ngOnInit() {}

  get getIcon() {
    switch (this.data.type) {
      case AlertType.Warning:
        return 'report_problem';
      case AlertType.Error:
        return 'error_outline';
      case AlertType.Success:
        return 'check_circle_outline';
      case AlertType.Info:
        return 'info_outline';
      default:
        return null;
    }
  }
}
