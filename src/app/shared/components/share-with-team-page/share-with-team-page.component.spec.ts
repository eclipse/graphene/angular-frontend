import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareWithTeamPageComponent } from './share-with-team-page.component';

describe('ShareWithTeamPageComponent', () => {
  let component: ShareWithTeamPageComponent;
  let fixture: ComponentFixture<ShareWithTeamPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShareWithTeamPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShareWithTeamPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
