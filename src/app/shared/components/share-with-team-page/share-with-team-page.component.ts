import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  inject,
} from '@angular/core';
import { AsyncPipe, CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import {
  MatAutocompleteModule,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { Observable, Subscription, map, startWith } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import {
  Alert,
  AlertType,
  PublicSolutionDetailsModel,
  Revision,
  UserDetails,
} from '../../models';
import { ActivatedRoute } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DeleteUserDialogConfirmationActionComponent } from '../delete-user-dialog-confirmation-action/delete-user-dialog-confirmation-action.component';
import { AlertService } from 'src/app/core/services/alert.service';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { SolutionIdComponent } from '../solution-id/solution-id.component';
import { TruncatePipe } from '../../pipes/truncate.pipe';

export interface State {
  flag: string;
  name: string;
  population: string;
}

@Component({
  selector: 'gp-share-with-team-page',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatDividerModule,
    AsyncPipe,
    SolutionIdComponent,
    TruncatePipe,
  ],
  templateUrl: './share-with-team-page.component.html',
  styleUrl: './share-with-team-page.component.scss',
})
export class ShareWithTeamPageComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  userCtrl = new FormControl('');
  filteredUsers: Observable<UserDetails[]>;

  users: UserDetails[] = [];
  allUsersList: UserDetails[] = [];

  allUserDetails: UserDetails[] = [];
  sharedWith: UserDetails[] = [];

  solutionId!: string;
  revisionId!: string;
  selectedRevision!: Revision;
  selectedRevisionSubscription!: Subscription;
  solution!: PublicSolutionDetailsModel;

  @ViewChild('userInput') userInput!: ElementRef<HTMLInputElement>;

  announcer = inject(LiveAnnouncer);

  constructor(
    private publicSolutionsService: PublicSolutionsService,
    private privateCatalogsService: PrivateCatalogsService,
    private sharedDataService: SharedDataService,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private alertService: AlertService,
  ) {
    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith(''), // Start with an empty string
      map((value) => {
        return this.transformValue(value);
      }),
      map((name) =>
        name ? this._filterUser(name) : this.allUsersList.slice(),
      ),
    );
  }

  transformValue(value: string | UserDetails | null): string {
    if (typeof value === 'string') {
      return value; // Directly return the string
    } else if (value) {
      return this.displayFn(value); // Convert UserDetails to string if not null
    }
    return ''; // Return empty string if value is null
  }

  displayFn(user: UserDetails): string {
    return user ? `${user.firstName} ${user.lastName}`.trim() : '';
  }

  ngOnInit(): void {
    this.activatedRoute.parent?.params.subscribe((params) => {
      this.solutionId = params['solutionId'];
      this.revisionId = params['revisionId'];
      this.getUsersChips();
      this.getShareWithTeam(this.solutionId);
      this.loadSolutionDetails(this.solutionId, this.revisionId);
      this.selectedRevisionSubscription =
        this.sharedDataService.selectedRevision$.subscribe(
          (revision: Revision) => {
            this.selectedRevision = revision;
          },
        );
    });
  }

  remove(user: UserDetails) {
    const index = this.users.indexOf(user);

    if (index >= 0) {
      this.users.splice(index, 1);

      this.announcer.announce(`Removed ${user}`);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.users.push(event.option.value);
    this.userInput.nativeElement.value = '';
    this.userCtrl.setValue(null);
  }

  private _filterUser(value: string): UserDetails[] {
    const filterValue = value.toLowerCase();

    return this.allUsersList.filter(
      (user) =>
        user.firstName.toLowerCase().includes(filterValue) ||
        user.lastName?.toLowerCase().includes(filterValue),
    );
  }

  getUsersChips(): void {
    const userActiveStatus = true;
    this.privateCatalogsService.getAllActiveUsers(userActiveStatus).subscribe({
      next: (users) => {
        if (this.sharedWith) {
          const sharedWithIds = new Set(
            this.sharedWith.map((user) => user.userId),
          );
          this.allUserDetails = users.filter((user) => {
            return (
              !sharedWithIds.has(user.userId) &&
              user.userId != this.solution.ownerId
            );
          });
        } else {
          this.allUserDetails = users.filter((user) => {
            return user.userId != this.solution.ownerId;
          });
        }

        this.allUsersList = this.allUserDetails;
      },
      error: (error) => {
        console.error('Error fetching users:', error);
      },
    });
  }

  getShareWithTeam(solutionId: string): void {
    this.privateCatalogsService.getShareWithTeam(solutionId).subscribe({
      next: (users) => {
        this.sharedWith = users;
        this.getUsersChips();
      },
      error: (error) => {
        console.error('Error fetching users:', error);
      },
    });
  }

  loadSolutionDetails(solutionId: string, revisionId: string) {
    this.publicSolutionsService
      .getSolutionDetails(solutionId, revisionId)
      .subscribe((res) => {
        this.solution = res;
      });
  }

  OnclickShareButton() {
    const selectedUsersIds: string[] = this.users.map(
      (user) => user.userId ?? '',
    );

    this.privateCatalogsService
      .insertMultipleShare(selectedUsersIds, this.solutionId)
      .subscribe((res) => {
        const alert: Alert = {
          message: '',
          type: AlertType.Success,
        };

        if (res.error_code === '100') {
          alert.message = 'Shared successfully. ';
        }

        this.alertService.notify(alert, 3000);
        this.getShareWithTeam(this.solutionId);
        this.users = [];
      });
  }

  removeUser(user: UserDetails) {
    const dialogRef: MatDialogRef<DeleteUserDialogConfirmationActionComponent> =
      this.dialog.open(DeleteUserDialogConfirmationActionComponent, {
        data: {
          dataKey: {
            title: 'Delete Shared Member',
            content: `Would you like to delete '${user.firstName} ${user.lastName}' from
      the shared list?`,
            alertMessage: 'Deleted successfully. ',
            action: () => this.deleteUser(user),
          },
        },
        autoFocus: false,
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.getShareWithTeam(this.solutionId);
    });
  }

  deleteUser(user: UserDetails): Observable<any> {
    return this.privateCatalogsService.deleteShareWithTeam(
      user.userId ?? '',
      this.solutionId,
    );
  }
}
