import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { AlertType } from '../../models';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'gp-delete-user-dialog-confirmation-action',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
  ],
  templateUrl: './delete-user-dialog-confirmation-action.component.html',
  styleUrl: './delete-user-dialog-confirmation-action.component.scss',
})
export class DeleteUserDialogConfirmationActionComponent implements OnInit {
  title!: string;
  content!: string;
  alertMessage!: string;

  constructor(
    public dialogRef: MatDialogRef<DeleteUserDialogConfirmationActionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alertService: AlertService,
  ) {}

  ngOnInit(): void {
    this.title = this.data.dataKey.title;
    this.content = this.data.dataKey.content;
    this.alertMessage = this.data.dataKey.alertMessage;
  }

  confirm() {
    this.data.dataKey.action().subscribe({
      next: (res: any) => {
        this.alertService.notify(
          { message: this.alertMessage, type: AlertType.Success },
          3000,
        );
        this.dialogRef.close(true);
      },
      error: (err: any) => {
        this.alertService.notify(
          { message: 'Operation failed', type: AlertType.Error },
          3000,
        );
        this.dialogRef.close(false);
      },
    });
  }
}
