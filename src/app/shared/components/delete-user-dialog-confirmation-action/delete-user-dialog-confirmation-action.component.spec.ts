import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteUserDialogConfirmationActionComponent } from './delete-user-dialog-confirmation-action.component';

describe('DeleteUserDialogConfirmationActionComponent', () => {
  let component: DeleteUserDialogConfirmationActionComponent;
  let fixture: ComponentFixture<DeleteUserDialogConfirmationActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DeleteUserDialogConfirmationActionComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DeleteUserDialogConfirmationActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
