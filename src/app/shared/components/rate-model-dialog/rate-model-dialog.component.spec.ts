import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateModelDialogComponent } from './rate-model-dialog.component';

describe('RateModelDialogComponent', () => {
  let component: RateModelDialogComponent;
  let fixture: ComponentFixture<RateModelDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RateModelDialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RateModelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
