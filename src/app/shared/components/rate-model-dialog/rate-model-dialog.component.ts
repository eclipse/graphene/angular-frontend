import {
  Component,
  Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatOptionModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { Alert, AlertType } from '../../models';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import {
  Observable,
  catchError,
  combineLatest,
  map,
  of,
  switchMap,
} from 'rxjs';

@Component({
  selector: 'gp-rate-model-dialog',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatOptionModule,
    MatTooltipModule,
  ],
  templateUrl: './rate-model-dialog.component.html',
  styleUrl: './rate-model-dialog.component.scss',
})
export class RateModelDialogComponent implements OnInit {
  review: string = '';
  rating: number = 0;
  starCount: number = 5;
  ratingArr: number[] = [];
  rating$: Observable<number>;

  constructor(
    public dialogRef: MatDialogRef<RateModelDialogComponent>,
    private privateCatalogsService: PrivateCatalogsService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alertService: AlertService,
    private sharedDataService: SharedDataService,
  ) {
    this.rating$ = combineLatest([
      of(this.data.dataKey.solutionId),
      of(this.data.dataKey.userId),
    ]).pipe(
      switchMap(([solutionId, userId]) => {
        if (!userId) {
          // Handle the case where userId is not available
          return of(null);
        }
        return this.privateCatalogsService.getSolutionRatings(
          solutionId,
          userId,
        );
      }),
      map((response) => response.response_body.rating),
      catchError((error) => {
        console.error('Error fetching ratings:', error);
        // Provide a default fallback value
        return of(0);
      }),
    );
  }
  ngOnInit(): void {
    this.sharedDataService.rating$.subscribe({
      next: (value) => {
        this.rating = value;
      },
    });

    for (let index = 0; index < this.starCount; index++) {
      this.ratingArr.push(index);
    }
  }

  submit(): void {
    this.dialogRef.close({ rating: this.rating, review: this.review });
    const dataObj = {
      rating: this.rating,
      solutionId: this.data.dataKey.solutionId,
      textReview: this.review,
      userId: this.data.dataKey.userId,
    };
    this.privateCatalogsService.createRatingSolution(dataObj).subscribe(
      (res) => {
        const alert: Alert = {
          message: '',
          type: AlertType.Success,
        };

        if (res.error_code === '100') {
          alert.message = "You've rated this model successfully. ";
        }

        this.alertService.notify(alert, 3000);
        this.dialogRef.close();
        this.sharedDataService.rating = dataObj.rating;
      },
      (err) => {},
    );
  }

  cancel(): void {
    this.dialogRef.close();
  }

  getRatingText(): string {
    switch (this.rating) {
      case 1:
        return 'Hated It';
      case 2:
        return 'Disliked It';
      case 3:
        return "It's OK";
      case 4:
        return 'Liked It';
      case 5:
        return 'Loved It';
      default:
        return '';
    }
  }

  showIcon(index: number) {
    if (this.rating >= index + 1) {
      return 'star';
    } else {
      return 'star_border';
    }
  }

  onClick(rating: number) {
    this.rating = rating;
    return false;
  }

  updateRating(newRating: number): void {
    this.rating = newRating;
  }
}
