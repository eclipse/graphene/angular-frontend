import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { Observable, Subscription, combineLatest, map } from 'rxjs';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { FormatBytesPipe } from '../../pipes/format-bytes.pipe';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoginComponent } from 'src/app/features/login/login.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ArtifactDownloadModel } from '../../models';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { apiConfig } from 'src/app/core/config';
import { JwtTokenService } from 'src/app/core/services/auth/jwt-token.service';

@Component({
  selector: 'gp-model-details-artifacts',
  standalone: true,
  imports: [CommonModule, MatTableModule, FormatBytesPipe, MatTooltipModule],
  templateUrl: './model-details-artifacts.component.html',
  styleUrl: './model-details-artifacts.component.scss',
})
export class ModelDetailsArtifactsComponent implements OnInit {
  solutionId: string = '';
  revisionId: string = '';
  versionId: string = '';
  versionIdSubscription: Subscription | undefined;
  artifactDownload!: ArtifactDownloadModel[];
  isUserIdAvailable$: Observable<boolean | undefined>;

  displayedColumns: string[] = [
    'name',
    'version',
    'modified on',
    'size',
    'action',
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private publicSolutionsService: PublicSolutionsService,
    private privateCatalogsService: PrivateCatalogsService,
    private sharedDataService: SharedDataService,
    private browserStorageService: BrowserStorageService,
    public dialog: MatDialog,
    private jwtTokenService: JwtTokenService,
  ) {
    this.isUserIdAvailable$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => !!details?.userId));
  }

  ngOnInit(): void {
    this.activatedRoute.parent?.paramMap.subscribe((paramMap) => {
      this.solutionId = paramMap.get('solutionId') || '';
      this.revisionId = paramMap.get('revisionId') || '';
    });

    // Get the initial version ID
    this.versionId = this.sharedDataService.versionId;

    // Subscribe to changes in versionId
    this.versionIdSubscription = this.sharedDataService.versionId$.subscribe(
      (versionId: string) => {
        this.versionId = versionId;
        this.loadArtifacts();
      },
    );

    // Load data initially
    this.loadArtifacts();
  }

  loadArtifacts() {
    this.publicSolutionsService
      .getArtifacts(this.solutionId, this.revisionId)
      .subscribe(
        (resp) => {
          this.artifactDownload = resp;
        },
        (error) => {},
      );
  }

  showAdvancedLogin(): void {
    const dialogRef: MatDialogRef<LoginComponent> =
      this.dialog.open(LoginComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }

  downloadArtifact(artifactId: string, artifactType: string): void {
    combineLatest([
      this.browserStorageService.getUserDetails(),
      this.jwtTokenService.getToken(),
    ]).subscribe({
      next: ([userDetails, jwtToken]) => {
        if (!userDetails || !jwtToken) {
          this.showAdvancedLogin();
          return;
        }

        const { userId } = userDetails;

        this.privateCatalogsService
          .performSVScan(this.solutionId, this.revisionId, 'download')
          .subscribe({
            next: (response) => {
              window.location.href = `${apiConfig.apiBackendURL}/api/downloads/${this.solutionId}?artifactId=${artifactId}&revisionId=${this.revisionId}&userId=${userId}&jwtToken=${jwtToken}`;
            },
            error: (error) => {
              console.error('Error during SV scan:', error);
            },
          });
      },
      error: (error) => {
        console.error('Error fetching user details or JWT token:', error);
      },
    });
  }
}
