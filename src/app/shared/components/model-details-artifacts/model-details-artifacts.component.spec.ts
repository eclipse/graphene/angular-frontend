import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailsArtifactsComponent } from './model-details-artifacts.component';

describe('ModelDetailsArtifactsComponent', () => {
  let component: ModelDetailsArtifactsComponent;
  let fixture: ComponentFixture<ModelDetailsArtifactsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModelDetailsArtifactsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModelDetailsArtifactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
