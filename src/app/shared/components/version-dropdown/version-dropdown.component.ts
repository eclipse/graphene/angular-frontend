import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { Revision } from '../../models';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'gp-version-dropdown',
  standalone: true,
  imports: [
    CommonModule,
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
  ],
  templateUrl: './version-dropdown.component.html',
  styleUrl: './version-dropdown.component.scss',
})
export class VersionDropdownComponent {
  @Input() selectedDefaultRevision!: Revision;
  @Input() revisionsList!: Revision[];
  @Output() revisionChange: EventEmitter<Revision> =
    new EventEmitter<Revision>();

  onChangeVersion(revision: Revision) {
    this.revisionChange.emit(revision);
  }
}
