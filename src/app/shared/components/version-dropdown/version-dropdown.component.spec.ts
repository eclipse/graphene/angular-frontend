import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionDropdownComponent } from './version-dropdown.component';

describe('VersionDropdownComponent', () => {
  let component: VersionDropdownComponent;
  let fixture: ComponentFixture<VersionDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [VersionDropdownComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VersionDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
