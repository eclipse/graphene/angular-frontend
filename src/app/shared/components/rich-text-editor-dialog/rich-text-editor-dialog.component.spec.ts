import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RichTextEditorDialogComponent } from './rich-text-editor-dialog.component';

describe('RichTextEditorDialogComponent', () => {
  let component: RichTextEditorDialogComponent;
  let fixture: ComponentFixture<RichTextEditorDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RichTextEditorDialogComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RichTextEditorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
