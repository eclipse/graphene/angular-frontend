import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { AlertService } from 'src/app/core/services/alert.service';
import { AlertType } from '../../models';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { QuillModule, QuillViewComponent } from 'ngx-quill';
import Quill from 'quill';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'gp-rich-text-editor-dialog',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    QuillModule,
    ReactiveFormsModule,
  ],
  templateUrl: './rich-text-editor-dialog.component.html',
  styleUrl: './rich-text-editor-dialog.component.scss',
})
export class RichTextEditorDialogComponent implements OnInit {
  @ViewChild('editorText') editorText!: QuillViewComponent;
  title!: string;
  content!: string;
  alertMessage!: string;
  private quillEditor!: Quill;
  private onChangeCallback!: (value: string) => void;
  private onTouchedCallback!: () => void;
  editorTextHtml!: any;
  form!: FormGroup;
  control!: string;
  descInnerHTML!: string;

  constructor(
    public dialogRef: MatDialogRef<RichTextEditorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
  ) {
    this.form = this.formBuilder.group({
      html: [null],
    });
  }

  ngOnInit(): void {
    this.title = this.data.dataKey.title;
    this.content = this.data.dataKey.content;
    this.alertMessage = this.data.dataKey.alertMessage;
    this.descInnerHTML = this.data.dataKey.descInnerHTML;
    if (this.descInnerHTML) {
      this.form.patchValue({ html: this.descInnerHTML });
    }
  }

  confirm() {
    this.data.dataKey.action(this.form.value.html).subscribe({
      next: (res: any) => {
        this.alertService.notify(
          { message: this.alertMessage, type: AlertType.Success },
          3000,
        );
        this.dialogRef.close(true);
      },
      error: (err: any) => {
        this.alertService.notify(
          { message: 'Operation failed', type: AlertType.Error },
          3000,
        );
        this.dialogRef.close(false);
      },
    });
  }

  onClickChange(event: any) {}
}
