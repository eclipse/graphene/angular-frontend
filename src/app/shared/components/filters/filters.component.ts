import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { Filter } from '../../models';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

@Component({
  selector: 'gp-filters',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatChipsModule,
    MatCheckboxModule,
    MatDividerModule,
    MatTooltipModule,
    ScrollingModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  templateUrl: './filters.component.html',
  styleUrl: './filters.component.scss',
})
export class FiltersComponent implements OnInit, OnChanges {
  @Input() tags!: Filter[];
  @Input() categories!: Filter[];
  @Output() selectedTagsChange = new EventEmitter<string[]>();
  @Output() selectedCategoriesChange = new EventEmitter<string[]>();
  @Output() searchValueChanged = new EventEmitter<string>();
  search: string = '';

  visibleTags: Filter[] = [];
  linkText: string = 'Show All';
  initialVisibleCount: number = 22;

  constructor() {}

  ngOnInit(): void {
    this.updateVisibleTags();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('tags' in changes) {
      this.updateVisibleTags();
    }
  }

  updateVisibleTags(): void {
    if (this.linkText === 'Show All') {
      this.visibleTags = this.tags.slice(0, this.initialVisibleCount);
    } else {
      this.visibleTags = [...this.tags];
    }
  }

  toggleLinkText(): void {
    this.linkText = this.linkText === 'Show Less' ? 'Show All' : 'Show Less';
    this.updateVisibleTags();
  }

  toggleTagSelection(tag: Filter): void {
    tag.selected = !tag.selected;
    const selectedTags = this.tags.filter((t) => t.selected).map((t) => t.name);
    this.selectedTagsChange.emit(selectedTags);
  }

  toggleCategorySelection(category: Filter): void {
    category.selected = !category.selected;
    const selectedCategories = this.categories
      .filter((c) => c.selected)
      .map((c) => c.name);
    this.selectedCategoriesChange.emit(selectedCategories);
  }

  onEnterKeyPressed(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const searchValue = (
        document.getElementById('searchInput') as HTMLInputElement
      )?.value;
      this.emitSearchValue(searchValue);
    }
  }

  private emitSearchValue(searchValue: string): void {
    this.searchValueChanged.emit(searchValue);
  }
}
