import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolutionIdComponent } from '../solution-id/solution-id.component';
import { NavigationLabelModel, PublicSolutionDetailsModel } from '../../models';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'gp-breadcrumb-navigation',
  standalone: true,
  imports: [CommonModule, SolutionIdComponent, MatTooltipModule],
  templateUrl: './breadcrumb-navigation.component.html',
  styleUrl: './breadcrumb-navigation.component.scss',
})
export class BreadcrumbNavigationComponent {
  @Input() firstNavigationLabel!: NavigationLabelModel;
  @Input() secondNavigationLabel!: NavigationLabelModel;
  @Input() solution!: PublicSolutionDetailsModel;

  @Output() firstNavigationClicked = new EventEmitter<void>();
  @Output() secondNavigationClicked = new EventEmitter<void>();

  onFirstNavigationLabelClick(): void {
    this.firstNavigationClicked.emit();
  }

  onSecondNavigationClick(): void {
    this.secondNavigationClicked.emit();
  }
}
