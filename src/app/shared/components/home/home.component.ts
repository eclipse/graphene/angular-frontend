import { Component, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { PublicSolutionsService } from '../../../core/services/public-solutions.service';
import { CardItemComponent } from '../card-item/card-item.component';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { PublicSolution } from '../../models';
import { Observable, Subscription, map, take } from 'rxjs';

@Component({
  selector: 'gp-home',
  standalone: true,
  imports: [
    CommonModule,
    MatGridListModule,
    MatCardModule,
    CardItemComponent,
    MatIconModule,
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent {
  itemsToShowInCarousel = 4;
  publicSolutions: PublicSolution[] = [];
  visibleStartIndex = 0;
  userId$: Observable<string | undefined>;
  private subscription = new Subscription();

  constructor(
    private publicSolutionsService: PublicSolutionsService,
    private router: Router,
    private browserStorageService: BrowserStorageService,
  ) {
    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
  }

  ngOnInit(): void {
    this.publicSolutionsService.getPublicSolutions().subscribe(
      (response) => {
        this.publicSolutions = response || [];
      },
      (error) => {},
    );
  }
  moveCarousel(direction: 'left' | 'right'): void {
    const increment = direction === 'left' ? -1 : 1;
    this.visibleStartIndex += increment;

    if (this.visibleStartIndex < 0) {
      this.visibleStartIndex = 0;
    } else if (
      this.visibleStartIndex + this.itemsToShowInCarousel >
      this.publicSolutions.length
    ) {
      this.visibleStartIndex =
        this.publicSolutions.length - this.itemsToShowInCarousel;
    }
  }

  onMarketPlaceClick() {
    this.subscription.add(
      this.userId$.pipe(take(1)).subscribe((userId) => {
        if (userId) {
          this.router.navigate(['/dashboard/marketplace']);
        } else {
          this.router.navigate(['/marketplace']);
        }
      }),
    );
  }
}
