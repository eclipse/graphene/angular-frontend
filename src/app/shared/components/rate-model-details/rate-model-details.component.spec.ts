import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateModelDetailsComponent } from './rate-model-details.component';

describe('RateModelDetailsComponent', () => {
  let component: RateModelDetailsComponent;
  let fixture: ComponentFixture<RateModelDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RateModelDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RateModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
