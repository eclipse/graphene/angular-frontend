import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTabsModule } from '@angular/material/tabs';
import {
  AllUserRating,
  AverageRatings,
  CommentModel,
  CommentReplyModel,
  PublicSolution,
} from '../../models';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { RateModelDialogComponent } from '../rate-model-dialog/rate-model-dialog.component';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'gp-rate-model-details',
  standalone: true,
  imports: [
    CommonModule,
    MatDividerModule,
    MatProgressBarModule,
    MatTabsModule,
    FormsModule,
  ],
  templateUrl: './rate-model-details.component.html',
  styleUrl: './rate-model-details.component.scss',
})
export class RateModelDetailsComponent {
  @Input() solutionId!: string;
  @Input() revisionId!: string;
  @Input() solution!: PublicSolution;
  @Input() averageRatings!: AverageRatings;
  @Input() totalRatingsCount: number = 0;
  @Input() ratingCounts: number[] = [0, 0, 0, 0, 0];
  @Input() allUserRatings: AllUserRating[] = [];
  @Input() totalCommentCount: number = 0;
  @Input() perRatingCounts: number[] = [0, 0, 0, 0, 0];
  @Input() userId!: string;
  @Input() solutionRating!: number;
  @Input() comments!: CommentReplyModel[];
  @Input() firstName!: string;
  @Input() lastName!: string;

  isEditComment: boolean = false;
  showReplyText: boolean = false;

  commentText!: string;
  editedCommentId!: string;

  @Output() updateRating = new EventEmitter();
  @Output() createComment = new EventEmitter<{
    comment: string;
    revisionId: string;
    userId: string;
    solutionId: string;
  }>();
  @Output() editComment = new EventEmitter<{
    solutionId: string;
    comment: CommentModel;
  }>();
  @Output() deleteComment = new EventEmitter<CommentModel>();
  @Output() replyToComment = new EventEmitter<{
    comment: CommentModel;
    revisionId: string;
  }>();

  replyText!: string;

  constructor(public dialog: MatDialog) {}

  openRateDialog() {
    const dialogRef: MatDialogRef<RateModelDialogComponent> = this.dialog.open(
      RateModelDialogComponent,
      {
        data: {
          dataKey: {
            solutionId: this.solutionId,
            userId: this.userId,
          },
        },
        autoFocus: false,
      },
    );

    dialogRef.afterClosed().subscribe((result) => {
      this.updateRating.emit();
    });
  }

  postComments() {
    this.showReplyText = false;
    this.createComment.emit({
      comment: this.commentText,
      revisionId: this.revisionId,
      userId: this.userId,
      solutionId: this.solutionId,
    });

    this.commentText = '';
  }

  onClickEditComment(comment: CommentModel) {
    this.editedCommentId = comment.commentId ?? '';
    this.isEditComment = !this.isEditComment;
  }

  onClickShowReply() {
    this.showReplyText = true;
  }

  onClickPostComment() {
    this.showReplyText = false;
  }

  OnClickPostReply(comment: CommentModel) {
    this.showReplyText = false;
    this.replyText = '';
    this.replyToComment.emit({
      comment: {
        text: this.replyText,
        parentId: comment.commentId,
        threadId: comment.threadId,
        userId: comment.userId,
        url: this.solutionId,
      } as CommentModel,
      revisionId: this.revisionId,
    });
  }

  onClickEditPostComment(comment: CommentModel) {
    this.isEditComment = false;
    this.editComment.emit({ solutionId: this.solutionId, comment });
  }

  OnClickCancelReply() {
    this.showReplyText = false;
  }

  onClickCancelEditComment() {
    this.isEditComment = false;
  }

  onClickDeleteComment(comment: CommentModel) {
    this.deleteComment.emit(comment);
  }
}
