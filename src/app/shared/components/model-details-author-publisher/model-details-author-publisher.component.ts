import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { MatTableModule } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import {
  AuthorPublisherModel,
  PublicSolutionDetailsModel,
  PublicSolutionDetailsRevisionModel,
} from '../../models';

@Component({
  selector: 'gp-model-details-author-publisher',
  standalone: true,
  imports: [CommonModule, MatTableModule],
  templateUrl: './model-details-author-publisher.component.html',
  styleUrl: './model-details-author-publisher.component.scss',
})
export class ModelDetailsAuthorPublisherComponent implements OnInit {
  solutionId!: string;
  revisionId!: string;
  solution!: PublicSolutionDetailsModel;
  authorList!: AuthorPublisherModel[];
  authorListSubscription: Subscription | undefined;

  publisherList!: AuthorPublisherModel[];
  displayedColumns: string[] = ['name', 'contact information'];

  get dataSource(): AuthorPublisherModel[] {
    return this.authorList.concat(this.publisherList);
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private publicSolutionsService: PublicSolutionsService,
    private sharedDataService: SharedDataService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.parent?.paramMap.subscribe((paramMap) => {
      this.solutionId = paramMap.get('solutionId') || '';
      this.revisionId = paramMap.get('revisionId') || '';
      this.loadModelAuthors();
    });

    this.loadModelAuthors();

    this.publicSolutionsService
      .getSolutionDetails(this.solutionId, this.revisionId)
      .subscribe(
        (res) => {
          this.solution = res;
          if (this.solution) {
            this.populatePublisherList();
          }
        },
        (err) => {
          console.error('Error fetching solution details:', err);
        },
      );
  }

  populatePublisherList() {
    if (this.solution && this.solution.revisions) {
      this.publisherList = this.solution.revisions
        .filter(
          (revision: PublicSolutionDetailsRevisionModel) =>
            revision.publisher !== null,
        )
        .map((revision: PublicSolutionDetailsRevisionModel) => ({
          name: revision.publisher,
          contact: '',
        }));
      this.dataSource;
    }
  }

  loadModelAuthors() {
    this.authorListSubscription = this.sharedDataService.authorList$.subscribe(
      (authorList: AuthorPublisherModel[]) => {
        this.authorList = authorList;
      },
    );
  }
}
