import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailsAuthorPublisherComponent } from './model-details-author-publisher.component';

describe('ModelDetailsAuthorPublisherComponent', () => {
  let component: ModelDetailsAuthorPublisherComponent;
  let fixture: ComponentFixture<ModelDetailsAuthorPublisherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModelDetailsAuthorPublisherComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModelDetailsAuthorPublisherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
