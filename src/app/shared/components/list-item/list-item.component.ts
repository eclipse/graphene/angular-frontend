import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedItemBaseComponent } from '../../common/shared-item-base/shared-item-base.component';
import { CatalogNameComponent } from '../catalog-name/catalog-name.component';
import { PublisherComponent } from '../publisher/publisher.component';
import { StarsComponent } from '../stars/stars.component';
import { TagsListComponent } from '../tags-list/tags-list.component';
import { CommentCountComponent } from '../../icons/comment-count/comment-count.component';
import { ViewCountComponent } from '../../icons/view-count/view-count.component';
import { DownloadCountComponent } from '../../icons/download-count/download-count.component';
import { FavoriteComponent } from '../../icons/favorite/favorite.component';

@Component({
  selector: 'gp-list-item',
  standalone: true,
  imports: [
    CommonModule,
    MatMenuModule,
    MatTooltipModule,
    CatalogNameComponent,
    PublisherComponent,
    StarsComponent,
    TagsListComponent,
    CommentCountComponent,
    ViewCountComponent,
    DownloadCountComponent,
    FavoriteComponent,
  ],
  templateUrl: './list-item.component.html',
  styleUrl: './list-item.component.scss',
})
export class ListItemComponent extends SharedItemBaseComponent {}
