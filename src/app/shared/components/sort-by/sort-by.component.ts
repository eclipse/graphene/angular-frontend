import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { SortOption } from '../../models';

@Component({
  selector: 'gp-sort-by',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
  ],
  templateUrl: './sort-by.component.html',
  styleUrl: './sort-by.component.scss',
})
export class SortByComponent {
  @Output() sortBySelected = new EventEmitter<string>();
  @Input() sortByOptions: SortOption[] = [];
  @Input() selectedSortBy: string = '';

  updateSortBy(): void {
    this.sortBySelected.emit(this.selectedSortBy);
  }
}
