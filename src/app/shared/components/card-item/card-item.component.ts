import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { SharedItemBaseComponent } from '../../common/shared-item-base/shared-item-base.component';
import { CatalogNameComponent } from '../catalog-name/catalog-name.component';
import { PublisherComponent } from '../publisher/publisher.component';
import { StarsComponent } from '../stars/stars.component';
import { TagsListComponent } from '../tags-list/tags-list.component';
import { CommentCountComponent } from '../../icons/comment-count/comment-count.component';
import { ViewCountComponent } from '../../icons/view-count/view-count.component';
import { DownloadCountComponent } from '../../icons/download-count/download-count.component';
import { FavoriteComponent } from '../../icons/favorite/favorite.component';
import { Router } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';

@Component({
  selector: 'gp-card-item',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatTooltipModule,
    MatMenuModule,
    CatalogNameComponent,
    PublisherComponent,
    StarsComponent,
    TagsListComponent,
    CommentCountComponent,
    ViewCountComponent,
    DownloadCountComponent,
    FavoriteComponent,
  ],
  templateUrl: './card-item.component.html',
  styleUrl: './card-item.component.scss',
})
export class CardItemComponent extends SharedItemBaseComponent {
  constructor(
    router: Router,
    publicSolutionsService: PublicSolutionsService,
    privateCatalogsService: PrivateCatalogsService,
    browserStorageService: BrowserStorageService,
  ) {
    super(
      publicSolutionsService,
      privateCatalogsService,
      router,
      browserStorageService,
    );
  }
}
