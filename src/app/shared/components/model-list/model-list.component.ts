import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicSolution } from '../../models';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { CardItemComponent } from '../card-item/card-item.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { ListItemComponent } from '../list-item/list-item.component';
import { MatButtonModule } from '@angular/material/button';
import { HeadlineComponent } from '../headline/headline.component';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'gp-model-list',
  standalone: true,
  imports: [
    CommonModule,
    CardItemComponent,
    MatPaginatorModule,
    MatGridListModule,
    ListItemComponent,
    MatButtonModule,
    HeadlineComponent,
    MatIconModule,
  ],
  templateUrl: './model-list.component.html',
  styleUrl: './model-list.component.scss',
})
export class ModelListComponent implements OnInit {
  @Input() solutions: PublicSolution[] = [];
  @Input() totalItems: number = 0;
  @Input() isLoading: boolean = false;
  @Input() showEntirePage!: boolean;
  @Input() headlineTitle!: string;
  @Input() viewTile: boolean = true;
  @Input() calculateStartIndex!: number;
  @Input() calculateEndIndex!: number;
  @Input() pageSize!: number;
  @Input() pageIndex!: number;
  @Input() pageSizeOptions!: number[];
  @Input() modelType!: 'published' | 'unpublished' | 'both';
  @Input() favoriteSolutionsMap: { [key: string]: boolean } = {};

  @Output() pageChange = new EventEmitter<PageEvent>();
  @Output() updateFavorite = new EventEmitter<string>();
  @Output() backEvent = new EventEmitter();

  constructor() {}
  ngOnInit(): void {}

  onPageEvent(event: PageEvent): void {
    this.pageChange.emit(event);
  }

  onUpdateFavoriteEvent(solutionId: string) {
    this.updateFavorite.emit(solutionId);
  }

  onClickSeeBackEvent() {
    this.backEvent.emit();
  }
}
