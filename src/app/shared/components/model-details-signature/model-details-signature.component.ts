import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gp-model-details-signature',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './model-details-signature.component.html',
  styleUrls: ['./model-details-signature.component.scss'],
})
export class ModelDetailsSignatureComponent implements OnInit, OnDestroy {
  modelSignature: string = '';
  solutionId!: string;
  revisionId!: string;
  versionId: string = '';
  versionIdSubscription: Subscription | undefined;

  constructor(
    private activatedRoute: ActivatedRoute,
    private publicSolutionsService: PublicSolutionsService,
    private sharedDataService: SharedDataService,
  ) {}

  ngOnInit() {
    this.activatedRoute.parent?.paramMap.subscribe((paramMap) => {
      this.solutionId = paramMap.get('solutionId') || '';
      this.revisionId = paramMap.get('revisionId') || '';
    });

    // Get the initial version ID
    this.versionId = this.sharedDataService.versionId;

    // Subscribe to changes in versionId
    this.versionIdSubscription = this.sharedDataService.versionId$.subscribe(
      (versionId: string) => {
        this.versionId = versionId;
      },
    );

    // Load data initially
    this.loadData();
  }

  ngOnDestroy() {
    // Unsubscribe from versionIdSubscription
    if (this.versionIdSubscription) {
      this.versionIdSubscription.unsubscribe();
    }
  }

  loadData() {
    // Check if both solutionId and versionId are available
    if (this.solutionId && this.versionId) {
      this.publicSolutionsService
        .getModelSignature(this.solutionId, this.versionId)
        .subscribe(
          (res) => {
            this.modelSignature = res;
          },
          (err) => {
            console.error('Error:', err);
          },
        );
    }
  }
}
