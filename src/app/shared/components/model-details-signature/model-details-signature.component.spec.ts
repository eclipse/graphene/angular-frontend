import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailsSignatureComponent } from './model-details-signature.component';

describe('ModelDetailsSignatureComponent', () => {
  let component: ModelDetailsSignatureComponent;
  let fixture: ComponentFixture<ModelDetailsSignatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModelDetailsSignatureComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModelDetailsSignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
