import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailsDescriptionComponent } from './model-details-description.component';

describe('ModelDetailsDescriptionComponent', () => {
  let component: ModelDetailsDescriptionComponent;
  let fixture: ComponentFixture<ModelDetailsDescriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModelDetailsDescriptionComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModelDetailsDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
