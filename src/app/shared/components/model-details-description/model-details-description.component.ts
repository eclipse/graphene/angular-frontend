import { Component, ElementRef, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { Subscription } from 'rxjs';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { ArtifactDownloadModel, Revision } from '../../models';

@Component({
  selector: 'gp-model-details-description',
  standalone: true,
  imports: [
    CommonModule,
    MatTabsModule,
    MatDividerModule,
    MatProgressBarModule,
    MatIconModule,
  ],
  templateUrl: './model-details-description.component.html',
  styleUrl: './model-details-description.component.scss',
})
export class ModelDetailsDescriptionComponent {
  solutionId: string = '';
  revisionId: string = '';
  selectedCatalogId = '';
  selectedRevision: Revision = {
    version: '',
    revisionId: '',
  };
  selectedRevisionSubscription: Subscription | undefined;
  selectedCatalogIdSubscription: Subscription | undefined;
  imageToShow: any;
  artifactDownload: ArtifactDownloadModel[] = [];
  dockerUrlOfModel: string = '';
  imageToShowSubscription: Subscription | undefined;
  description = '';
  solutionCompanyDesc = '';
  checkFlag = true;
  ratedescriptioncheck = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private publicSolutionsService: PublicSolutionsService,
    private sharedDataService: SharedDataService,
  ) {}

  ngOnInit() {
    this.activatedRoute.parent?.paramMap.subscribe((paramMap) => {
      this.solutionId = paramMap.get('solutionId') || '';
      this.revisionId = paramMap.get('revisionId') || '';
      this.loadSolutionDescription();
    });

    this.selectedCatalogId = this.sharedDataService.selectedCatalogId;

    this.selectedCatalogIdSubscription =
      this.sharedDataService.selectedCatalogId$.subscribe(
        (selectedCatalogId: string) => {
          this.selectedCatalogId = selectedCatalogId;
        },
      );
    this.selectedRevisionSubscription =
      this.sharedDataService.selectedRevision$.subscribe(
        (revision: Revision) => {
          this.selectedRevision.revisionId = revision.revisionId;
          this.loadSolutionDescription();
        },
      );
    this.imageToShowSubscription =
      this.sharedDataService.imageToShow$.subscribe((imageToShow: any) => {
        this.imageToShow = imageToShow;
      });

    this.loadArtifacts();
  }

  processArtifacts(responseBody: ArtifactDownloadModel[]): void {
    const dockerArtifacts = responseBody.filter(
      (artifact) => artifact.artifactType === 'DI',
    );
    const validDockerArtifactsLength = [2, 3];

    if (
      dockerArtifacts.length &&
      validDockerArtifactsLength.includes(this.artifactDownload.length)
    ) {
      this.dockerUrlOfModel = dockerArtifacts[0].artifactUri;
    }
  }

  loadArtifacts() {
    this.publicSolutionsService
      .getArtifacts(this.solutionId, this.revisionId)
      .subscribe(
        (resp) => {
          this.artifactDownload = resp;
          this.processArtifacts(this.artifactDownload);
        },
        (error) => {},
      );
  }

  loadSolutionDescription() {
    if (this.selectedCatalogId && this.revisionId) {
      this.publicSolutionsService
        .getSolutionDescription(
          this.selectedRevision.revisionId,
          this.selectedCatalogId,
        )
        .subscribe(
          (resp) => {
            this.description = resp.response_body.description;
            this.processDescription(this.description);
          },
          (error) => {
            this.description = '';
            this.ratedescriptioncheck = 0;
          },
        );
    }
  }

  private processDescription(description: string): void {
    // Removing HTML tags
    const cleanedDescription = description.replace(/<[^>]+>/gm, '');

    // Checking length
    const descriptionLength = cleanedDescription.length;
    this.ratedescriptioncheck = descriptionLength;

    if (descriptionLength < 500) {
      this.checkFlag = true;
    } else {
      this.checkFlag = false;
    }
  }
}
