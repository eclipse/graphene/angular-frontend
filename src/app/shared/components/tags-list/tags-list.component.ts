import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { PublicSolution, Tag } from '../../models';

@Component({
  selector: 'gp-tags-list',
  standalone: true,
  imports: [CommonModule, MatMenuModule],
  templateUrl: './tags-list.component.html',
  styleUrl: './tags-list.component.scss',
})
export class TagsListComponent {
  @Input() item!: PublicSolution;
  @Input() slnID!: string;
  @Input() Tag!: boolean;
}
