import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailsLicenseProfileComponent } from './model-details-license-profile.component';

describe('ModelDetailsLicenseProfileComponent', () => {
  let component: ModelDetailsLicenseProfileComponent;
  let fixture: ComponentFixture<ModelDetailsLicenseProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModelDetailsLicenseProfileComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModelDetailsLicenseProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
