import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription, map } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CreateEditLicenseProfileComponent } from '../create-edit-license-profile/create-edit-license-profile.component';
import { UploadLicenseProfileComponent } from '../upload-license-profile/upload-license-profile.component';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import {
  emptyLicenseProfileModel,
  LicenseProfileModel,
  Revision,
} from '../../models';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';

@Component({
  selector: 'gp-model-details-license-profile',
  standalone: true,
  imports: [CommonModule, MatButtonModule, CreateEditLicenseProfileComponent],
  templateUrl: './model-details-license-profile.component.html',
  styleUrl: './model-details-license-profile.component.scss',
})
export class ModelDetailsLicenseProfileComponent implements OnInit {
  @Input() isExistingLicenseProfile: boolean = true;
  modelLicense: LicenseProfileModel | null = null;
  isLicenseFound = false;
  isLoadingLicense = true;
  isLicenseJson = false;
  modelLicenseError = '';
  solutionId!: string;
  revisionId!: string;
  revisionSubscription: Subscription | undefined;
  isUserIdAvailable$: Observable<boolean | undefined>;
  userId$: Observable<string | undefined>;
  userId!: string;
  selectedRevision!: Revision;

  private subscription: Subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private publicSolutionsService: PublicSolutionsService,
    private privateCatalogsService: PrivateCatalogsService,
    protected sharedDataService: SharedDataService,
    public dialog: MatDialog,
    private browserStorageService: BrowserStorageService,
    private cd: ChangeDetectorRef,
  ) {
    this.isUserIdAvailable$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => !!details?.userId));
    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
  }

  ngOnInit() {
    this.subscription.add(
      this.sharedDataService.licenseProfile$.subscribe((profile) => {
        this.modelLicense = profile;
      }),
    );
    this.subscription.add(
      this.browserStorageService.getUserDetails().subscribe((details) => {
        this.userId = details?.userId ?? '';
      }),
    );
    this.activatedRoute.parent?.paramMap.subscribe((paramMap) => {
      this.solutionId = paramMap.get('solutionId') || '';
      this.revisionId = paramMap.get('revisionId') || '';
    });

    // Get the initial version ID
    this.selectedRevision = this.sharedDataService.selectedRevision;

    // Subscribe to changes in versionId
    this.revisionSubscription =
      this.sharedDataService.selectedRevision$.subscribe(
        (revision: Revision) => {
          this.selectedRevision = revision;
          this.loadData();
        },
      );

    // Load data initially
    this.loadData();
  }

  ngOnDestroy() {
    // Unsubscribe from versionIdSubscription
    if (this.revisionSubscription) {
      this.revisionSubscription.unsubscribe();
    }
  }

  loadData() {
    // Check if both solutionId and versionId are available
    if (this.solutionId && this.selectedRevision.version) {
      this.publicSolutionsService
        .getLicenseFile(this.solutionId, this.selectedRevision.version)
        .subscribe(
          (res) => {
            if (res) {
              this.modelLicense = res;
              if (this.modelLicense) {
                this.isLoadingLicense = false;
                this.isLicenseFound = true;
              } else {
                this.modelLicenseError = 'No license found';
                this.isLoadingLicense = false;
              }
            } else {
              // Handle the case where res is undefined or null
              this.isLoadingLicense = false;
              this.modelLicenseError = 'No license found';
              console.error('Error: Response is undefined or null');
              this.modelLicense = emptyLicenseProfileModel;
            }
          },
          (err) => {
            this.isLoadingLicense = false;
            this.modelLicenseError = 'No license found';
            console.error('Error:', err);
          },
        );
    }
  }

  onClickUpdate() {
    const dialogRef: MatDialogRef<CreateEditLicenseProfileComponent> =
      this.dialog.open(CreateEditLicenseProfileComponent, {
        data: {
          dataKey: {
            modelLicense: this.modelLicense,
            solutionId: this.solutionId,
            revisionId: this.revisionId,
            isEditMode: true,
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      // This will be executed when the dialog is closed
      // Reload data to fetch the updated license profile
      this.loadData();
    });
  }

  uploadExistingLicenseFile(file: File): Observable<any> {
    return this.privateCatalogsService.uploadExistingModelLicenseProfile(
      this.userId,
      this.solutionId,
      this.revisionId,
      this.selectedRevision.version,
      file,
    );
  }
  uploadNewLicenseFile(file: File) {
    return this.privateCatalogsService.uploadNewModelLicenseProfile(
      this.userId,
      file,
    );
  }

  onClickUpload() {
    const dialogRef: MatDialogRef<UploadLicenseProfileComponent> =
      this.dialog.open(UploadLicenseProfileComponent, {
        data: {
          dataKey: {
            isEditMode: false,
            title: 'Upload License file',
            errorMessage:
              'Please update the license profile to correct the following validation errors:',
            supportedFileText:
              'Maximum file size: 1mb | Supported file type: .json',
            action: (file: File) =>
              this.isExistingLicenseProfile
                ? this.uploadExistingLicenseFile(file)
                : this.uploadNewLicenseFile(file),
            isLicenseProfile: true,
            isProcessEvent: true,
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      // This will be executed when the dialog is closed
      // Reload data to fetch the updated license profile
      this.loadData();
    });
  }

  onClickCreate() {
    const dialogRef: MatDialogRef<CreateEditLicenseProfileComponent> =
      this.dialog.open(CreateEditLicenseProfileComponent, {
        data: {
          dataKey: {
            isEditMode: false,
            solutionId: this.solutionId,
            revisionId: this.revisionId,
          },
        },
      });
    dialogRef.afterClosed().subscribe((result) => {
      // This will be executed when the dialog is closed
      // Reload data to fetch the updated license profile
      this.loadData();
    });
  }
}
