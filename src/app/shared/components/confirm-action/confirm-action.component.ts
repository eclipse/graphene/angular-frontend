import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'gp-confirm-action',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
  ],
  templateUrl: './confirm-action.component.html',
  styleUrl: './confirm-action.component.scss',
})
export class ConfirmActionComponent implements OnInit {
  title!: string;
  content!: string;
  secondAction!: string;

  constructor(
    public dialogRef: MatDialogRef<ConfirmActionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(): void {
    this.title = this.data.dataKey.title;
    this.content = this.data.dataKey.content;
    this.secondAction = this.data.dataKey.secondAction;
  }

  onClickSecondAction() {
    this.data.dataKey.action().subscribe({
      next: (res: any) => {
        this.dialogRef.close(true);
      },
      error: (err: any) => {
        this.dialogRef.close(false);
      },
    });
  }
}
