import {
  Component,
  ElementRef,
  inject,
  OnInit,
  signal,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import {
  Alert,
  AlertType,
  Catalog,
  CatalogFilter,
  DocumentModel,
  LicenseProfileModel,
  PublicSolutionDetailsModel,
  PublicSolutionDetailsRevisionModel,
  PublishSolutionRequest,
  Revision,
  Tag,
  ToolkitTypeCode,
  toolkitTypeCodeMap,
} from '../../models';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FiltersService } from 'src/app/core/services/filters.service';
import { QuillModule } from 'ngx-quill';
import { UploadLicenseProfileComponent } from '../upload-license-profile/upload-license-profile.component';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AlertService } from 'src/app/core/services/alert.service';
import { ModelDetailsLicenseProfileComponent } from '../model-details-license-profile/model-details-license-profile.component';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { RichTextEditorDialogComponent } from '../rich-text-editor-dialog/rich-text-editor-dialog.component';
import {
  BehaviorSubject,
  catchError,
  combineLatest,
  map,
  Observable,
  of,
  startWith,
  Subject,
  Subscription,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs';
import { UpdateModelNameDialogComponent } from '../update-model-name-dialog/update-model-name-dialog.component';
import { UpdateSolutionRequestPayload } from '../../models/request-payloads';
import {
  MatAutocompleteModule,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { CreateEditLicenseProfileComponent } from '../create-edit-license-profile/create-edit-license-profile.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
interface RouteParams {
  solutionId: string;
  revisionId: string;
}

interface ModelData {
  solutionId: string;
  revisionId: string;
  solution?: PublicSolutionDetailsModel | null;
  picture?: any | null;
  description?: any | null;
  licenseProfile?: LicenseProfileModel | LicenseProfileModel[] | null;
  category?: CatalogFilter | null;
  toolkitType?: CatalogFilter | null;
  image?: any | null;
  tags?: string[];
  documents?: any[];
}

@Component({
  selector: 'gp-publish-to-marketplace-page',
  standalone: true,
  imports: [
    CommonModule,
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    QuillModule,
    ModelDetailsLicenseProfileComponent,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    MatDividerModule,
  ],
  templateUrl: './publish-to-marketplace-page.component.html',
  styleUrl: './publish-to-marketplace-page.component.scss',
})
export class PublishToMarketplacePageComponent implements OnInit {
  solutionId!: string;
  revisionId!: string;
  catalogs!: Catalog[];
  selectedCatalog!: Catalog;
  selectedToolkitType!: CatalogFilter;
  selectedCategory!: CatalogFilter;
  publishToMarketPlaceForm!: FormGroup;
  solution!: PublicSolutionDetailsModel;
  categories: CatalogFilter[] = [];
  toolkitTypes: CatalogFilter[] = [];
  tagCtrl = new FormControl('');
  @ViewChild('tagInput') tagInput!: ElementRef<HTMLInputElement>;
  filteredTags = new BehaviorSubject<string[]>([]);

  imageFile!: { content: File; name: string };
  documentFile!: { content: File; name: string };
  licenseProfile!: { content: File; name: string };
  tagsItems = signal<Tag[]>([]);
  documents = signal<DocumentModel[]>([]);
  allTags = signal<string[]>([]);

  descInnerHTML!: string;

  blured = false;
  focused = false;

  data$: Observable<ModelData>;

  addEditLicenseProfile: boolean = false;

  announcer = inject(LiveAnnouncer);

  defaultSolutionData = {};
  imageToShow: any;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  private subscription: Subscription = new Subscription();
  userId!: string;
  revisionsList!: Revision[];
  selectedDefaultRevision!: Revision;
  licenseName!: string;
  statusStepperProgress = {
    documentation: false,
    requestApproval: false,
    published: false,
  };
  publishRequest!: PublishSolutionRequest;
  isSubmitted = false;
  enableSubmit = false;
  private onDestroy = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private privateCatalogsService: PrivateCatalogsService,
    private formBuilder: FormBuilder,
    private filtersService: FiltersService,
    private sharedDataService: SharedDataService,
    public dialog: MatDialog,
    private alertService: AlertService,
    private browserStorageService: BrowserStorageService,
    private publicSolutionsService: PublicSolutionsService,
  ) {
    this.buildForm();
    this.data$ =
      this.activatedRoute.parent?.params.pipe(
        switchMap(({ solutionId, revisionId }) => {
          return this.publicSolutionsService
            .getSolutionDetails(solutionId, revisionId)
            .pipe(
              tap((res) => {
                this.revisionsList = this.getRevisionsList(res);

                this.selectedDefaultRevision = this.revisionsList.filter(
                  (rv) => rv.revisionId === this.revisionId,
                )[0];

                this.setRevisionInService(this.selectedDefaultRevision);
              }),
              switchMap((solution) => {
                const documentStream =
                  this.selectedCatalog && this.selectedCatalog.catalogId
                    ? this.publicSolutionsService
                        .getSolutionDocuments(
                          solutionId,
                          revisionId,
                          this.selectedCatalog.catalogId,
                        )
                        .pipe(catchError(() => of([])))
                    : of([]);
                const descriptionStream =
                  this.selectedCatalog && this.selectedCatalog.catalogId
                    ? this.publicSolutionsService
                        .getSolutionDescription(
                          revisionId,
                          this.selectedCatalog.catalogId,
                        )
                        .pipe(catchError(() => of('')))
                    : of('');
                return combineLatest({
                  solutionId: of(solutionId),
                  revisionId: of(revisionId),
                  solution: of(solution || null),
                  picture: this.publicSolutionsService
                    .getPictureOfSolution(solutionId)
                    .pipe(catchError(() => of(null))),
                  licenseProfile: this.publicSolutionsService
                    .getLicenseFile(
                      solutionId,
                      this.selectedDefaultRevision.version,
                    )
                    .pipe(catchError(() => of(null))),
                  tags: this.privateCatalogsService.getAllTag(),
                  documents: documentStream,
                  description: descriptionStream,
                });
              }),
              tap((data) => {
                this.processSideEffects(data);
              }),
              catchError((error) => {
                console.error('Error in combined data stream:', error);
                return of({} as ModelData);
              }),
            );
        }),
        catchError((error) => {
          console.error('Error fetching parameters:', error);
          return of({} as ModelData);
        }),
      ) || of({} as ModelData);
  }

  buildForm() {
    this.publishToMarketPlaceForm = this.formBuilder.group({
      catalog: [null, [Validators.required]],
      name: [
        '',
        [
          Validators.required,
          Validators.pattern('^(?=.*[a-z])[a-z0-9]+(?:[-.][a-z0-9]+)*$'),
        ],
        [],
      ],
      description: ['', [Validators.required]],
      category: [null, [Validators.required]],
      toolkitType: [null, Validators.required],
      licenseProfile: [null, [Validators.required]],
      documents: [[]],
      tags: [[], [Validators.required]],
      image: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.activatedRoute.parent?.params.subscribe((params) => {
      this.solutionId = params['solutionId'];
      this.revisionId = params['revisionId'];
      this.privateCatalogsService
        .getAuthors(this.solutionId, this.revisionId)
        .subscribe({
          next: (res) => {
            if (res.length === 0) {
              const alert: Alert = {
                message:
                  'You cannot publish the model without entering the author name. Please add author name in the "Manage Publisher/Authors" page to publish it.',
                type: AlertType.Error,
              };
              this.alertService.notify(alert);
              this.alertService.notify;
            }
          },
          error: (error) => {
            console.error('Error fetching users:', error);
          },
        });
      this.publicSolutionsService
        .getSolutionDetails(this.solutionId, this.revisionId)
        .subscribe({
          next: (res) => {
            if (res) {
              this.solution = res;
              this.publishToMarketPlaceForm.patchValue({
                name: res.name,
              });
            }
          },
          error: (error) => {},
        });
    });

    this.privateCatalogsService.getAllAvailableCatalogs().subscribe({
      next: (res) => {
        if (res) this.catalogs = res;
      },
      error: (error) => {},
    });

    this.filtersService.getModelTypes().subscribe(
      (res: CatalogFilter[]) => {
        this.categories = res;
      },
      (err: any) => {},
    );

    this.filtersService.getToolkitTypes().subscribe(
      (res: CatalogFilter[]) => {
        this.toolkitTypes = res;
      },
      (err: any) => {},
    );

    this.tagCtrl.valueChanges
      .pipe(
        startWith(''),
        map((inputValue) =>
          inputValue
            ? this.filterTags(inputValue, this.allTags())
            : this.allTags(),
        ),
      )
      .subscribe((filteredTags) => {
        this.filteredTags.next(filteredTags);
      });

    this.subscription.add(
      this.browserStorageService.getUserDetails().subscribe((details) => {
        this.userId = details?.userId ?? '';
      }),
    );
    this.publishToMarketPlaceForm.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.checkFormValidity();
      });
  }

  checkFormValidity() {
    const {
      name,
      catalog,
      description,
      category,
      toolkitType,
      licenseProfile,
      tags,
      image,
    } = this.publishToMarketPlaceForm.value;

    this.enableSubmit =
      name &&
      catalog &&
      description &&
      category &&
      toolkitType &&
      licenseProfile &&
      tags &&
      image;
  }

  private filterTags(inputValue: string, tags: string[]): string[] {
    return tags.filter((tag) =>
      tag.toLowerCase().includes(inputValue.toLowerCase()),
    );
  }

  updateAllTagsSignal(value: string, tags: string[]): string[] {
    const updatedTags = tags.filter(
      (tag) => tag.toLowerCase() !== value.toLocaleLowerCase(),
    );
    this.allTags.update((tags: string[]) => [...updatedTags]);
    return updatedTags;
  }

  onChangeCatalog(event: MatSelectChange) {
    this.selectedCatalog = event.value;

    if (this.selectedCatalog.catalogId !== '') {
      this.publicSolutionsService
        .getSolutionDescription(
          this.revisionId,
          this.selectedCatalog.catalogId ?? '',
        )
        .subscribe({
          next: (res) => {
            this.publishToMarketPlaceForm.patchValue({
              description: res.response_body.description,
            });
          },
          error: (error) => {
            console.error('Failed to fetch description', error);
            // Handle the error, e.g., set a default value or show an error message
          },
        });

      this.publicSolutionsService
        .getSolutionDocuments(
          this.solutionId,
          this.revisionId,
          this.selectedCatalog.catalogId ?? '',
        )
        .subscribe({
          next: (res) => {
            this.publishToMarketPlaceForm.patchValue({
              documents: res,
            });

            this.documents.update((documents: DocumentModel[]) => [
              ...documents,
              ...res,
            ]);
          },
          error: (error) => {
            console.error('Failed to fetch documents', error);
          },
        });
      this.privateCatalogsService
        .searchPublishRequestWithCatalogIds(
          this.revisionId,
          this.selectedCatalog.catalogId ?? '',
        )
        .subscribe({
          next: (res) => {
            this.publishRequest = res;
            if (res) {
              if (res.requestStatusCode === 'PE') {
                this.statusStepperProgress.documentation = true;
                this.statusStepperProgress.requestApproval = true;
              }
              if (res.requestStatusCode === 'AP') {
                this.statusStepperProgress.documentation = true;
                this.statusStepperProgress.published = true;
              }
              if (res.requestStatusCode === 'WD') {
                this.statusStepperProgress.requestApproval = false;
                this.statusStepperProgress.published = false;
              }
            }
          },
          error: (error) => {
            console.error('Failed to fetch publish request', error);
            // Handle the error, e.g., set a default value or show an error message
          },
        });
    }
  }

  submit() {
    this.privateCatalogsService
      .publishSolution(
        this.solutionId,
        this.selectedCatalog.accessTypeCode,
        this.userId,
        this.revisionId,
        this.selectedCatalog.catalogId ?? '',
      )
      .subscribe({
        next: (res) => {
          this.loadPublishRequest();
        },
        error: () => {},
      });
  }

  created(event: any) {}

  onaAddEditImage() {}
  onAddEditDocuments(event: MatChipInputEvent) {}
  onAddEditLicenseProfile() {}

  getFilenameExtension(filename: string): string {
    const parts = filename.split('.');
    return parts[parts.length - 1];
  }

  selectImageFile(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      this.imageFile.content = file;
      this.imageFile.name = file.name;
      this.publishToMarketPlaceForm.patchValue({ image: file });
      const extensionFile = this.getFilenameExtension(this.imageFile.name);
    }
  }

  selectDocumentFile(event: any, file: { content: File; name: string }) {
    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      this.documentFile.content = file;
      this.documentFile.name = file.name;
      this.publishToMarketPlaceForm.patchValue({ document: file });
      const extensionFile = this.getFilenameExtension(this.documentFile.name);
    }
  }

  selectLicenseProfileFile(event: any, file: { content: File; name: string }) {
    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      this.licenseProfile.content = file;
      this.licenseProfile.name = file.name;
      this.publishToMarketPlaceForm.patchValue({ licenseProfile: file });
      const extensionFile = this.getFilenameExtension(this.licenseProfile.name);
    }
  }
  onClickUpdateImageSolution(file: File) {
    return this.privateCatalogsService.setSolutionPicture(
      this.solutionId,
      file,
    );
  }

  onClickUploadImageFile() {
    const dialogRef: MatDialogRef<UploadLicenseProfileComponent> =
      this.dialog.open(UploadLicenseProfileComponent, {
        data: {
          dataKey: {
            title: 'Upload image model',
            isEditMode: false,
            isCheckExtension: false,
            action: (file: File) => this.onClickUpdateImageSolution(file),
            isLicenseProfile: false,
            isProcessEvent: false,
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.updateSolutionImage();
    });
  }
  updateSolutionImage() {
    this.publicSolutionsService
      .getPictureOfSolution(this.solutionId)
      .subscribe({
        next: (res) => {
          this.createImageFromBlob(res);
        },
        error: (error) => {},
      });
  }

  onClickUpdateSolutionDocument(file: File) {
    return this.privateCatalogsService.updateSolutionFiles(
      this.solutionId,
      this.revisionId,
      this.selectedCatalog.catalogId ?? '',
      file,
    );
  }

  onClickUploadDocumentFile() {
    const dialogRef: MatDialogRef<UploadLicenseProfileComponent> =
      this.dialog.open(UploadLicenseProfileComponent, {
        data: {
          dataKey: {
            title: 'Upload document model',
            isEditMode: false,
            isCheckExtension: false,
            action: (file: File) => this.onClickUpdateSolutionDocument(file),
            isLicenseProfile: false,
            isProcessEvent: false,
            accept: 'application/pdf',
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.updateDocuments();
    });
  }

  updateDocuments() {
    this.publicSolutionsService
      .getSolutionDocuments(
        this.solutionId,
        this.revisionId,
        this.selectedCatalog.catalogId ?? '',
      )
      .subscribe({
        next: (res) => {
          this.documents.update((dcs) => [...res]);
        },
        error: (error) => {},
      });
  }

  onLicenseProfileClick() {
    this.addEditLicenseProfile = true;
  }

  removeTag(tag: Tag) {
    this.privateCatalogsService.deleteTag(tag.tag, this.solutionId).subscribe({
      next: (res) => {
        const alert: Alert = {
          message: 'Tag deleted successfully',
          type: AlertType.Success,
        };
        this.alertService.notify(alert, 5000);
        this.alertService.notify;
        this.updateTags();
      },
      error: () => {},
    });
  }

  updateTags() {
    this.publicSolutionsService
      .getSolutionDetails(this.solutionId, this.revisionId)
      .subscribe({
        next: (res) => {
          const tagsList = res?.solutionTagList;
          this.tagsItems.update((tags: Tag[]) => [...tagsList]);
          this.publishToMarketPlaceForm.patchValue({
            tags: res?.solutionTagList,
          });
        },
        error: () => {},
      });
  }

  removeDocument(document: DocumentModel) {
    this.privateCatalogsService
      .deleteSolutionsFiles(
        this.solutionId,
        this.revisionId,
        this.selectedCatalog.catalogId ?? '',
        document.documentId,
      )
      .subscribe({
        next: (res) => {
          const alert: Alert = {
            message: 'Document deleted successfully',
            type: AlertType.Success,
          };
          this.alertService.notify(alert, 5000);
          this.alertService.notify;
          this.updateDocuments();
        },
        error: () => {},
      });
  }

  addTag(event: MatChipInputEvent): void {
    if (!(this.allTags().indexOf(event.value) > -1)) {
      this.privateCatalogsService.createTags(event.value).subscribe({
        next: (res) => {
          const alert: Alert = {
            message: 'Tag created successfully',
            type: AlertType.Success,
          };
          this.alertService.notify(alert, 5000);
          this.alertService.notify;

          this.privateCatalogsService.getAllTag().subscribe({
            next: (res) => {
              this.allTags.update((tags: string[]) => [...res]);
              this.filteredTags.next(this.allTags());
            },
            error: () => {},
          });
        },
        error: () => {},
      });
    }
    this.privateCatalogsService.addTag(event.value, this.solutionId).subscribe({
      next: (res) => {
        if (this.allTags().indexOf(event.value) > -1) {
          const alert: Alert = {
            message: 'Tag added successfully',
            type: AlertType.Success,
          };
          this.alertService.notify(alert, 5000);
          this.alertService.notify;
          this.updateTags();
          this.filteredTags.next(
            this.updateAllTagsSignal(event.value, this.allTags()),
          );
        }
        this.updateTags();
      },
      error: () => {},
    });

    // Clear the input value
    event.chipInput!.clear();
  }

  onChangeInput(event: any) {
    // if()
    //event.target.value.trim().toLowerCase()
  }

  extractDescription(html: string): string {
    return html.replace(/<[^>]+>/gm, '');
  }

  onAddEditDescription(htmlVal: string) {
    this.publishToMarketPlaceForm.patchValue({ description: htmlVal });
    this.privateCatalogsService
      .updateCompanyDescription(
        htmlVal,
        this.solutionId,
        this.revisionId,
        this.selectedCatalog.catalogId ?? '',
      )
      .subscribe({
        next: (res) => {
          this.publishToMarketPlaceForm.patchValue({
            description: res.response_body.description,
          });
          error: (error: any) => {};
        },
      });
  }
  addEditDescription() {
    const dialogRef: MatDialogRef<RichTextEditorDialogComponent> =
      this.dialog.open(RichTextEditorDialogComponent, {
        data: {
          dataKey: {
            title: 'Add model description',
            isEditMode: false,
            isCheckExtension: false,
            descInnerHTML: this.publishToMarketPlaceForm.value.description,
            action: (htmlVal: string) => {
              this.onAddEditDescription(htmlVal);
            },
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  private processSideEffects(data: ModelData): void {
    this.revisionId = data.revisionId;
    this.solutionId = data.solutionId;

    if (data.picture) {
      this.createImageFromBlob(data.picture);
    }

    if (data.solution?.modelTypeName && data.solution.modelType) {
      this.publishToMarketPlaceForm.get('category')?.setValue({
        name: data.solution?.modelTypeName,
        code: data.solution.modelType,
      });
    }

    if (data.solution?.tookitType && data.solution.tookitTypeName)
      this.publishToMarketPlaceForm.controls['toolkitType'].patchValue({
        name: data.solution.tookitTypeName,
        code: data.solution.tookitType,
      });

    if (data.documents) {
      this.publishToMarketPlaceForm.patchValue({ documents: data.documents });
    }

    if (
      data.solution &&
      data.solution?.solutionTagList &&
      data.solution.solutionTagList.length >= 1
    ) {
      const tagsList = data.solution?.solutionTagList;
      this.tagsItems.update((tags: Tag[]) => [...tagsList]);
      this.publishToMarketPlaceForm.patchValue({
        tags: data.solution?.solutionTagList,
      });
    }
    if (data.description) {
      const desc = this.extractDescription(
        data.description.response_body.description,
      );
      this.publishToMarketPlaceForm.patchValue({
        description: desc,
      });
    }
    if (data.solution && data.solution.catalogId) {
      const solutionCatalog = this.catalogs.filter(
        (c) => c.catalogId === data.solution?.catalogId,
      )[0];
      this.publishToMarketPlaceForm.patchValue({
        catalog: solutionCatalog,
      });
    }
    if (data.tags && data.tags.length > 0) {
      const updatedTags = data.tags ?? [];
      this.allTags.update((tags: string[]) => [...updatedTags]);
    }

    if (data.licenseProfile) {
      this.licenseName =
        'license-' + this.selectedDefaultRevision.version + '.json';
      this.publishToMarketPlaceForm.patchValue({
        licenseProfile: data.licenseProfile,
      });
    }
  }

  setRevisionInService(revision: Revision): void {
    this.sharedDataService.selectedRevision = revision;
  }

  editModelName(name: string) {
    const updatedSolution: UpdateSolutionRequestPayload = {
      ...this.solution,
      name,
    };
    this.privateCatalogsService.updateSolution(updatedSolution).subscribe({
      next: (res) => {},
      error: (error) => {},
    });
  }

  onClickEditModelName() {
    const version = this.solution.revisions.find(
      (rv) => rv.revisionId === this.revisionId,
    )?.version;
    const dialogRef: MatDialogRef<UpdateModelNameDialogComponent> =
      this.dialog.open(UpdateModelNameDialogComponent, {
        data: {
          dataKey: {
            modelData: this.solution,
            version,
            action: (name: string) => this.editModelName(name),
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  unpublishModel() {}

  compareObjects(o1: CatalogFilter, o2: CatalogFilter): boolean {
    return o1.name === o2.name && o1.code === o2.code;
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.privateCatalogsService
      .addTag(event.option.value, this.solutionId)
      .subscribe({
        next: (res) => {
          const alert: Alert = {
            message: 'Tag added successfully',
            type: AlertType.Success,
          };
          this.alertService.notify(alert, 5000);
          this.alertService.notify;
          this.updateTags();
          event.option.deselect();
          this.tagCtrl.setValue(null);
          this.tagInput.nativeElement.value = '';
          this.filteredTags.next(
            this.updateAllTagsSignal(event.option.value, this.allTags()),
          );
        },
        error: () => {},
      });
  }

  uploadNewLicenseFile(file: File) {
    return this.privateCatalogsService.uploadNewModelLicenseProfile(
      this.userId,
      file,
    );
  }

  uploadExistingLicenseFile(file: File): Observable<any> {
    return this.privateCatalogsService.uploadExistingModelLicenseProfile(
      this.userId,
      this.solutionId,
      this.selectedDefaultRevision.revisionId,
      this.selectedDefaultRevision.version,
      file,
    );
  }

  onClickUploadLicenseProfile() {
    const dialogRef: MatDialogRef<UploadLicenseProfileComponent> =
      this.dialog.open(UploadLicenseProfileComponent, {
        data: {
          dataKey: {
            isEditMode: false,
            title: 'Upload License file',
            errorMessage:
              'Please update the license profile to correct the following validation errors:',
            supportedFileText:
              'Maximum file size: 1mb | Supported file type: .json',
            action: (file: File) => this.uploadExistingLicenseFile(file),
            isLicenseProfile: true,
            isProcessEvent: true,
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      // This will be executed when the dialog is closed
      // Reload data to fetch the updated license profile
    });
  }

  getRevisionsList(solution: PublicSolutionDetailsModel): Revision[] {
    return solution.revisions.map(
      (revision: PublicSolutionDetailsRevisionModel) => ({
        version: revision.version,
        revisionId: revision.revisionId,
        onBoarded: revision.onboarded,
      }),
    );
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        this.imageToShow = reader.result;
        this.publishToMarketPlaceForm.patchValue({
          image: reader.result,
        });
      },
      false,
    );

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  onClickUpdateLicenseProfile() {
    const dialogRef: MatDialogRef<CreateEditLicenseProfileComponent> =
      this.dialog.open(CreateEditLicenseProfileComponent, {
        data: {
          dataKey: {
            modelLicense: null,
            solutionId: this.solutionId,
            revisionId: this.revisionId,
            isEditMode: true,
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      // This will be executed when the dialog is closed
      // Reload data to fetch the updated license profile
    });
  }

  onClickCreateLicenseProfile() {
    const dialogRef: MatDialogRef<CreateEditLicenseProfileComponent> =
      this.dialog.open(CreateEditLicenseProfileComponent, {
        data: {
          dataKey: {
            isEditMode: false,
            solutionId: this.solutionId,
            revisionId: this.revisionId,
          },
        },
      });
    dialogRef.afterClosed().subscribe((result) => {
      // This will be executed when the dialog is closed
      // Reload data to fetch the updated license profile
    });
  }

  onClickWithdrawRequest() {
    this.privateCatalogsService
      .withdrawPublishRequest(this.publishRequest.publishRequestId)
      .subscribe({
        next: (res) => {
          this.publishRequest = res;
          if (res.requestStatusCode === 'PE') {
            this.statusStepperProgress.documentation = true;
            this.statusStepperProgress.requestApproval = true;
          }
          if (res.requestStatusCode === 'AP') {
            this.statusStepperProgress.documentation = true;
            this.statusStepperProgress.published = true;
          }
          if (res.requestStatusCode === 'WD') {
            this.statusStepperProgress.requestApproval = false;
            this.statusStepperProgress.published = false;
          }
        },
        error: (error) => {},
      });
  }

  loadPublishRequest() {
    this.publicSolutionsService
      .getSolutionDocuments(
        this.solutionId,
        this.revisionId,
        this.selectedCatalog.catalogId ?? '',
      )
      .subscribe({
        next: (res) => {
          this.publishToMarketPlaceForm.patchValue({
            documents: res,
          });

          this.documents.update((documents: DocumentModel[]) => [
            ...documents,
            ...res,
          ]);
        },
        error: (error) => {
          console.error('Failed to fetch documents', error);
        },
      });
    this.privateCatalogsService
      .searchPublishRequestWithCatalogIds(
        this.revisionId,
        this.selectedCatalog.catalogId ?? '',
      )
      .subscribe({
        next: (res) => {
          res = this.publishRequest;
          if (res.requestStatusCode === 'PE') {
            this.statusStepperProgress.documentation = true;
            this.statusStepperProgress.requestApproval = true;
          }
          if (res.requestStatusCode === 'AP') {
            this.statusStepperProgress.documentation = true;
            this.statusStepperProgress.published = true;
          }
          if (res.requestStatusCode === 'WD') {
            this.statusStepperProgress.requestApproval = false;
            this.statusStepperProgress.published = false;
          }
        },
        error: (error) => {
          console.error('Failed to fetch description', error);
          // Handle the error, e.g., set a default value or show an error message
        },
      });
  }
}
