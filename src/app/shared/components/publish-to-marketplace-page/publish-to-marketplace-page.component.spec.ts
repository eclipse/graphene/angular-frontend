import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishToMarketplacePageComponent } from './publish-to-marketplace-page.component';

describe('PublishToMarketplacePageComponent', () => {
  let component: PublishToMarketplacePageComponent;
  let fixture: ComponentFixture<PublishToMarketplacePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PublishToMarketplacePageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PublishToMarketplacePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
