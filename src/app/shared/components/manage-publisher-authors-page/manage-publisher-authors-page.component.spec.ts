import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePublisherAuthorsPageComponent } from './manage-publisher-authors-page.component';

describe('ManagePublisherAuthorsPageComponent', () => {
  let component: ManagePublisherAuthorsPageComponent;
  let fixture: ComponentFixture<ManagePublisherAuthorsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ManagePublisherAuthorsPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ManagePublisherAuthorsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
