import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  FormsModule,
  ReactiveFormsModule,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ActivatedRoute, Router } from '@angular/router';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { Alert, AlertType, AuthorPublisherModel } from '../../models';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DeleteUserDialogConfirmationActionComponent } from '../delete-user-dialog-confirmation-action/delete-user-dialog-confirmation-action.component';
import { Observable, map, of } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert.service';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';

@Component({
  selector: 'gp-manage-publisher-authors-page',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDividerModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatIconModule,
  ],
  templateUrl: './manage-publisher-authors-page.component.html',
  styleUrl: './manage-publisher-authors-page.component.scss',
})
export class ManagePublisherAuthorsPageComponent implements OnInit {
  solutionId!: string;
  revisionId!: string;
  authorsList!: AuthorPublisherModel[];
  publisherName!: string;
  authorForm!: FormGroup;
  publisherForm!: FormGroup;
  editPublisher: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private privateCatalogsService: PrivateCatalogsService,
    public dialog: MatDialog,
    private alertService: AlertService,
    private sharedDataService: SharedDataService,
  ) {
    this.authorForm = this.formBuilder.group({
      name: ['', Validators.required],
      contact: ['', Validators.required],
    });

    this.publisherForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.pattern('^[A-Z|a-z|0-9 ]*$'),
          // this.notSameAsPublisher(this.publisherName),
        ],
        // [],
      ],
    });
  }

  ngOnInit(): void {
    this.activatedRoute.parent?.params.subscribe((params) => {
      this.solutionId = params['solutionId'];
      this.revisionId = params['revisionId'];
      this.loadAuthorList(this.solutionId, this.revisionId);
      this.loadPublisher(this.solutionId, this.revisionId);
    });
  }

  loadAuthorList(solutionId: string, revisionId: string) {
    this.privateCatalogsService.getAuthors(solutionId, revisionId).subscribe({
      next: (res) => {
        this.authorsList = res;
        this.updateAuthorsService(res);
      },
      error: (error) => {
        console.error('Error fetching users:', error);
      },
    });
  }

  loadPublisher(solutionId: string, revisionId: string) {
    this.privateCatalogsService.getPublisher(solutionId, revisionId).subscribe({
      next: (res) => {
        if (res) {
          this.publisherName = res;
          this.publisherNameControlValue = res;
          this.updateValidators();
        }
      },
      error: (error) => {
        console.error('Error fetching users:', error);
      },
    });
  }

  updatePublisherData(solutionId: string, revisionId: string) {
    this.privateCatalogsService.getPublisher(solutionId, revisionId).subscribe({
      next: (res) => {
        this.publisherName = res;
        this.publisherNameControlValue = res;
      },
      error: (error) => {
        console.error('Error fetching users:', error);
      },
    });
  }

  onAddAuthorClick(formDirective: FormGroupDirective) {
    const author = this.authorForm.value;

    this.privateCatalogsService
      .addAuthor(this.solutionId, this.revisionId, author)
      .subscribe({
        next: (res) => {
          this.authorsList = res;
          const alert: Alert = {
            message: '',
            type: AlertType.Success,
          };

          alert.message = 'Author added successfully. ';
          this.alertService.notify(alert, 3000);
          this.authorForm.reset();
          formDirective.resetForm();
          this.updateAuthorsService(res);
        },
        error: (error) => {
          const alert: Alert = {
            message: '',
            type: AlertType.Error,
          };

          alert.message =
            'Error while adding Author :  ' + error.error.response_detail;
          this.alertService.notify(alert, 3000);
        },
      });
  }

  onRemoveAuthorClick(author: AuthorPublisherModel) {
    const dialogRef: MatDialogRef<DeleteUserDialogConfirmationActionComponent> =
      this.dialog.open(DeleteUserDialogConfirmationActionComponent, {
        data: {
          dataKey: {
            title: 'Delete Author',
            content: `Would you like to delete author  '${author.name}' `,
            alertMessage: 'Author deleted successfully. ',

            action: () => this.removeAuthor(author),
          },
        },
        autoFocus: false,
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.loadAuthorList(this.solutionId, this.revisionId);
    });
  }

  OnEditPublisherClick() {
    if (this.publisherNameControl)
      this.changeAsyncValidationRules(
        [this.notSameAsPublisher()],
        this.publisherNameControl,
      );
    if (this.publisherForm.valid)
      this.privateCatalogsService
        .updatePublisher(
          this.solutionId,
          this.revisionId,
          this.publisherForm.value.name,
        )
        .subscribe({
          next: (res) => {
            const alert: Alert = {
              message: '',
              type: AlertType.Success,
            };

            alert.message = 'Publisher updated successfully. ';
            this.alertService.notify(alert, 3000);

            this.updatePublisherData(this.solutionId, this.revisionId);
            if (this.publisherNameControl) {
              this.clearPublisherNameValidation(this.publisherNameControl);
              this.changeSyncValidationRules(
                [
                  Validators.required,
                  Validators.minLength(2),
                  Validators.pattern('^[A-Za-z0-9 ]+$'),
                ],
                this.publisherNameControl,
              );
            }
          },
          error: (error) => {},
        });
  }

  removeAuthor(author: AuthorPublisherModel): Observable<any> {
    return this.privateCatalogsService.removeAuthor(
      this.solutionId,
      this.revisionId,
      author ?? null,
    );
  }

  onClickEditPublisher() {
    this.editPublisher = true;
  }

  onCancelClick() {
    this.editPublisher = false;
    this.publisherForm.setValue({ name: this.publisherName });
  }

  notSameAsPublisher(): AsyncValidatorFn {
    return (
      control: AbstractControl,
    ): Observable<{ [key: string]: any } | null> => {
      return of(control.value).pipe(
        map((value) => {
          const isSame =
            value.trim().toLowerCase() ===
            this.publisherName.trim().toLowerCase();
          return isSame ? { sameAsPublisher: true } : null;
        }),
      );
    };
  }

  get publisherNameControl() {
    return this.publisherForm.get('name');
  }

  get publisherNameControlValue() {
    return this.publisherNameControl?.value;
  }

  set publisherNameControlValue(value) {
    this.publisherNameControl?.setValue(value, { emitEvent: false });
  }

  updateValidators(): void {
    const nameControl = this.publisherNameControl;
    if (nameControl) {
      nameControl.setValidators([
        Validators.required,
        Validators.minLength(2),
        Validators.pattern('^[A-Za-z0-9 ]+$'),
      ]);
      nameControl.setAsyncValidators(this.notSameAsPublisher());
      nameControl.updateValueAndValidity({ emitEvent: false });
    }
  }

  changeSyncValidationRules(
    validators: ValidatorFn | ValidatorFn[] | null,
    control: AbstractControl<any, any>,
  ): void {
    control.setValidators(validators);
  }

  changeAsyncValidationRules(
    validators: AsyncValidatorFn | AsyncValidatorFn[] | null,
    control: AbstractControl<any, any>,
  ): void {
    control.setAsyncValidators(validators);
    //  control.addAsyncValidators(validators);
    control.updateValueAndValidity();
  }

  clearPublisherNameValidation(control: AbstractControl<any, any>) {
    control.clearAsyncValidators();
  }

  updateAuthorsService(authors: AuthorPublisherModel[]) {
    // Get the current value directly
    let authorsList = this.sharedDataService.authorListValue;

    if (!authorsList) {
      authorsList = [];
    }

    this.sharedDataService.authorList = authors;
  }
}
