import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PublicSolution } from '../../models';

@Component({
  selector: 'gp-catalog-name',
  standalone: true,
  imports: [CommonModule, MatTooltipModule],
  templateUrl: './catalog-name.component.html',
  styleUrl: './catalog-name.component.scss',
})
export class CatalogNameComponent {
  @Input() item!: PublicSolution;
}
