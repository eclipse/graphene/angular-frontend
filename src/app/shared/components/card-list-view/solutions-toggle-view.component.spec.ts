import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolutionsToggleViewComponent } from './solutions-toggle-view.component';

describe('SolutionsToggleViewComponent', () => {
  let component: SolutionsToggleViewComponent;
  let fixture: ComponentFixture<SolutionsToggleViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SolutionsToggleViewComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SolutionsToggleViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
