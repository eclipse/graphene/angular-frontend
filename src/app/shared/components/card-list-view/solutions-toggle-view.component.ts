import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'gp-solutions-toggle-view',
  standalone: true,
  imports: [CommonModule, MatIconModule],
  templateUrl: './solutions-toggle-view.component.html',
  styleUrl: './solutions-toggle-view.component.scss',
})
export class SolutionsToggleViewComponent {
  @Input() viewTile!: boolean;
  @Output() viewTileChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  toggleView(isGridView: boolean): void {
    this.viewTile = isGridView;
    this.viewTileChange.emit(this.viewTile);
  }
}
