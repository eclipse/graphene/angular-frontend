import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailsDocumentsComponent } from './model-details-documents.component';

describe('ModelDetailsDocumentsComponent', () => {
  let component: ModelDetailsDocumentsComponent;
  let fixture: ComponentFixture<ModelDetailsDocumentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModelDetailsDocumentsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModelDetailsDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
