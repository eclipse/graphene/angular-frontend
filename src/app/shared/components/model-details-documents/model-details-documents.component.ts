import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { MatTableModule } from '@angular/material/table';

@Component({
  selector: 'gp-model-details-documents',
  standalone: true,
  imports: [CommonModule, MatTableModule],
  templateUrl: './model-details-documents.component.html',
  styleUrl: './model-details-documents.component.scss',
})
export class ModelDetailsDocumentsComponent implements OnInit, OnDestroy {
  solutionId!: string;
  revisionId!: string;
  selectedCatalogId: string = '';
  selectedCatalogIdSubscription: Subscription | undefined;
  supportingDocs!: any[];
  documents!: any[];

  displayedColumns: string[] = ['name', 'version', 'modified on', 'action'];

  constructor(
    private activatedRoute: ActivatedRoute,
    private publicSolutionsService: PublicSolutionsService,
    private sharedDataService: SharedDataService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.parent?.paramMap.subscribe((paramMap) => {
      this.solutionId = paramMap.get('solutionId') || '';
      this.revisionId = paramMap.get('revisionId') || '';
    });

    // Get the initial version ID
    this.selectedCatalogId = this.sharedDataService.selectedCatalogId;

    // Subscribe to changes in versionId
    this.selectedCatalogIdSubscription =
      this.sharedDataService.selectedCatalogId$.subscribe(
        (selectedCatalogId: string) => {
          this.selectedCatalogId = selectedCatalogId;
          this.loadData();
        },
      );

    this.loadData();
  }
  ngOnDestroy(): void {
    // Unsubscribe from versionIdSubscription
    if (this.selectedCatalogIdSubscription) {
      this.selectedCatalogIdSubscription.unsubscribe();
    }
  }

  loadData() {
    // Check if both solutionId and versionId are available
    if (this.solutionId && this.selectedCatalogId) {
      this.publicSolutionsService
        .getSolutionDocuments(
          this.solutionId,
          this.revisionId,
          this.selectedCatalogId,
        )
        .subscribe(
          (res) => {
            this.supportingDocs = res;
            this.transformDocuments();
          },
          (err) => {
            console.error('Error:', err);
          },
        );
    }
  }

  transformDocuments() {
    this.documents = this.supportingDocs.map((doc: any) => ({
      name: doc.name,
      ext: doc.name.split('.').pop(),
      documentId: doc.documentId,
    }));
  }
}
