import { Component, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'gp-expanded-search',
  standalone: true,
  imports: [CommonModule, FormsModule, MatIconModule, MatButtonModule],
  templateUrl: './expanded-search.component.html',
  styleUrl: './expanded-search.component.scss',
})
export class ExpandedSearchComponent {
  searchActive: boolean = false;
  searchTerm: string = '';

  toggleSearch() {
    this.searchActive = !this.searchActive;
    if (!this.searchActive) {
      // Clear the search term when hiding the input
      this.searchTerm = '';
    }
  }
}
