import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteModelPageComponent } from './delete-model-page.component';

describe('DeleteModelPageComponent', () => {
  let component: DeleteModelPageComponent;
  let fixture: ComponentFixture<DeleteModelPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DeleteModelPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DeleteModelPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
