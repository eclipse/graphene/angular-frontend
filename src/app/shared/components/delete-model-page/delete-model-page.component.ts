import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DeleteUserDialogConfirmationActionComponent } from '../delete-user-dialog-confirmation-action/delete-user-dialog-confirmation-action.component';
import { PublicSolutionDetailsModel } from '../../models';
import { ActivatedRoute, Router } from '@angular/router';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'gp-delete-model-page',
  standalone: true,
  imports: [CommonModule, MatButtonModule],
  templateUrl: './delete-model-page.component.html',
  styleUrl: './delete-model-page.component.scss',
})
export class DeleteModelPageComponent implements OnInit {
  solution!: PublicSolutionDetailsModel;
  solutionId!: string;
  revisionId!: string;
  model!: any;

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private publicSolutionsService: PublicSolutionsService,
    private privateCatalogsService: PrivateCatalogsService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.parent?.params.subscribe((params) => {
      this.solutionId = params['solutionId'];
      this.revisionId = params['revisionId'];
      this.loadSolutionDetails(this.solutionId, this.revisionId);
    });
  }

  deleteModel(): Observable<any> {
    return this.privateCatalogsService.deleteSolution(
      this.solution,
      this.revisionId,
    );
  }

  onDeleteModelClick() {
    const dialogRef: MatDialogRef<DeleteUserDialogConfirmationActionComponent> =
      this.dialog.open(DeleteUserDialogConfirmationActionComponent, {
        data: {
          dataKey: {
            title: `Delete model ${this.solution.name}`,
            content: `Are you sure that you want to delete this model and all of its revisions, public or private?' ${this.solution.name}`,
            alertMessage: 'Model deleted successfully. ',

            action: () => this.deleteModel(),
          },
        },
        autoFocus: false,
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate(['/dashboard/myModels']);
    });
  }

  loadSolutionDetails(solutionId: string, revisionId: string) {
    this.publicSolutionsService
      .getSolutionDetails(solutionId, revisionId)
      .subscribe((res) => {
        this.solution = res;
      });
  }
}
