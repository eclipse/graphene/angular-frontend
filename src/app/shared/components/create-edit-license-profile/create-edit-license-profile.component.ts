import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { JwtTokenService } from 'src/app/core/services/auth/jwt-token.service';
import {
  Observable,
  Subscription,
  catchError,
  combineLatest,
  map,
  switchMap,
  take,
  throwError,
} from 'rxjs';
import { AlertService } from 'src/app/core/services/alert.service';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { Alert, AlertType, LicenseProfileModel } from '../../models';
import { HttpEventType } from '@angular/common/http';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';

@Component({
  selector: 'gp-create-edit-license-profile',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    MatToolbarModule,
    MatDialogModule,
    MatIconModule,
  ],
  templateUrl: './create-edit-license-profile.component.html',
  styleUrl: './create-edit-license-profile.component.scss',
})
export class CreateEditLicenseProfileComponent implements OnInit {
  isEditMode!: boolean;
  modelLicense!: LicenseProfileModel;
  form!: FormGroup;
  routeSub!: Subscription;
  userId$: Observable<string | undefined>;
  solutionId: string = '';
  revisionId: string = '';
  versionId: string = '';
  versionIdSubscription: Subscription | undefined;
  enableSubmit = false;

  constructor(
    private formBuilder: FormBuilder,
    private sharedDataService: SharedDataService,
    private alertService: AlertService,
    private privateCatalogsService: PrivateCatalogsService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CreateEditLicenseProfileComponent>,
    private browserStorageService: BrowserStorageService,
  ) {
    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
  }

  private setupFormValueChangesSubscription() {
    combineLatest([
      this.controls['keyword'].valueChanges,
      this.controls['licenseName'].valueChanges,
      this.controls['year'].valueChanges,
      this.controls['company'].valueChanges,
      this.controls['suffix'].valueChanges,
      this.controls['contactName'].valueChanges,
      this.controls['url'].valueChanges,
      this.controls['email'].valueChanges,
    ]).subscribe(
      ([
        keyword,
        licenseName,
        year,
        company,
        suffix,
        contactName,
        url,
        email,
      ]) => {
        this.enableSubmit =
          keyword &&
          licenseName &&
          year &&
          company &&
          suffix &&
          contactName &&
          url &&
          email;
      },
    );
  }

  ngOnInit() {
    this.modelLicense = this.data.dataKey.modelLicense;
    this.isEditMode = this.data.dataKey.isEditMode;
    this.solutionId = this.data.dataKey.solutionId;
    this.revisionId = this.data.dataKey.revisionId;

    this.initializeForm();
    this.setupFormValueChangesSubscription();
    if (this.modelLicense) this.updateFormValues();

    // Get the initial version ID
    this.versionId = this.sharedDataService.versionId;
    // Subscribe to changes in versionId
    this.versionIdSubscription = this.sharedDataService.versionId$.subscribe(
      (versionId: string) => {
        this.versionId = versionId;
      },
    );
  }

  ngOnDestroy() {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    // Unsubscribe from versionIdSubscription
    if (this.versionIdSubscription) {
      this.versionIdSubscription.unsubscribe();
    }
  }

  get controls() {
    return this.form ? this.form.controls : {};
  }

  private initializeForm() {
    this.form = this.formBuilder.group({
      keyword: ['', Validators.required],
      licenseName: ['', Validators.required],
      introduction: [''],
      softwareArtifact: [''],
      companyName: [''],
      year: ['', Validators.required],
      company: ['', Validators.required],
      suffix: ['', Validators.required],
      contactName: ['', Validators.required],
      url: ['', Validators.required],
      email: ['', Validators.required],
      additionalInformation: [''],
    });
  }

  private updateFormValues() {
    this.form.patchValue({
      keyword: this.modelLicense?.keyword,
      licenseName: this.modelLicense?.licenseName,
      softwareArtifact: this.modelLicense?.softwareType,
      companyName: this.modelLicense?.companyName,
      year: this.modelLicense?.copyright.year,
      company: this.modelLicense?.copyright.company,
      suffix: this.modelLicense?.copyright.suffix,
      contactName: this.modelLicense?.contact.name,
      url: this.modelLicense?.contact.URL,
      email: this.modelLicense?.contact.email,
      introduction: this.modelLicense?.intro,
      additionalInformation: this.modelLicense.additionalInfo,
    });
  }

  submit(): void {
    if (
      this.solutionId &&
      this.revisionId &&
      this.versionId &&
      this.form.valid
    ) {
      const licenseProfile = {
        $schema:
          'https://raw.githubusercontent.com/acumos/license-manager/master/license-manager-client-library/src/main/resources/schema/1.0.0/license-profile.json',
        keyword: this.form.value.keyword,
        licenseName: this.form.value.licenseName,
        intro: this.form.value.introduction,
        copyright: {
          year: this.form.value.year,
          company: this.form.value.company,
          suffix: this.form.value.suffix,
        },
        softwareType: this.form.value.softwareArtifact,
        companyName: this.form.value.companyName,
        contact: {
          name: this.form.value.contactName,
          URL: this.form.value.url,
          email: this.form.value.email,
        },
        additionalInfo: this.form.value.additionalInformation,
        rtuRequired: false,
      };

      // Using the userId$ observable directly in the submission logic
      this.userId$
        .pipe(
          take(1), // Ensures the subscription gets only one value (the latest) and completes
          switchMap((userId) => {
            if (!userId) {
              throw new Error('User ID is unavailable');
            }
            return this.privateCatalogsService.createUpdateLicenseProfile(
              userId,
              this.solutionId,
              this.revisionId,
              this.versionId,
              licenseProfile,
            );
          }),
          catchError((error) => {
            console.error('Error updating license profile:', error);
            return throwError(
              () => new Error('Failed to update license profile'),
            );
          }),
        )
        .subscribe({
          next: (event) => {
            if (event.type === HttpEventType.Response) {
              this.handleUploadSuccess(event.body);
              this.sharedDataService.licenseProfile = licenseProfile;
              this.dialogRef.close();
            }
          },
          error: (error) => this.handleUploadError(error),
        });
    }
  }

  private handleUploadSuccess(response: any) {
    const alert: Alert = {
      message: 'License uploaded successfully',
      type: AlertType.Success,
    };
    this.alertService.notify(alert, 5000);
  }

  private handleUploadError(error: any) {
    // Error handling logic here
    const messageError = error.error.response_detail
      ? error.error.response_detail
      : error.error;
    // Additional error handling
  }
}
