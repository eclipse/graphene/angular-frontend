import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditLicenseProfileComponent } from './create-edit-license-profile.component';

describe('CreateEditLicenseProfileComponent', () => {
  let component: CreateEditLicenseProfileComponent;
  let fixture: ComponentFixture<CreateEditLicenseProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateEditLicenseProfileComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CreateEditLicenseProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
