import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateModelNameDialogComponent } from './update-model-name-dialog.component';

describe('UpdateModelNameDialogComponent', () => {
  let component: UpdateModelNameDialogComponent;
  let fixture: ComponentFixture<UpdateModelNameDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UpdateModelNameDialogComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateModelNameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
