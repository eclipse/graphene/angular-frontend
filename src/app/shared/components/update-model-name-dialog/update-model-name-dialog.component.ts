import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AlertService } from 'src/app/core/services/alert.service';
import { AlertType } from '../../models';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
  selector: 'gp-update-model-name-dialog',
  standalone: true,
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
  ],
  templateUrl: './update-model-name-dialog.component.html',
  styleUrl: './update-model-name-dialog.component.scss',
})
export class UpdateModelNameDialogComponent implements OnInit {
  title!: string;
  content!: string;
  alertMessage!: string;
  updateModelNameForm!: FormGroup;
  message: string = '';

  constructor(
    public dialogRef: MatDialogRef<UpdateModelNameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
  ) {
    this.updateModelNameForm = this.formBuilder.group({
      modelName: ['', [Validators.required]],
      modelVersion: [{ value: '', disabled: true }],
      modelId: [{ value: '', disabled: true }],
    });
  }

  ngOnInit(): void {
    this.title = this.data.dataKey.title;
    this.content = this.data.dataKey.content;
    this.alertMessage = this.data.dataKey.alertMessage;
    this.updateModelNameForm.patchValue({
      modelName: this.data.dataKey.modelData.name,
    });
    this.updateModelNameForm.patchValue({
      modelVersion: this.data.dataKey.version,
    });
    this.updateModelNameForm.patchValue({
      modelId: this.data.dataKey.modelData.solutionId,
    });
  }

  update() {
    if (
      this.data.dataKey.modelData.name !==
      this.updateModelNameForm.controls['modelName'].value
    )
      this.data.dataKey
        .action(this.updateModelNameForm.controls['modelName'].value)
        .subscribe({
          next: (res: any) => {
            this.alertService.notify(
              { message: this.alertMessage, type: AlertType.Success },
              3000,
            );
            this.dialogRef.close(true);
          },
          error: (err: any) => {
            this.alertService.notify(
              { message: 'Operation failed', type: AlertType.Error },
              3000,
            );
            this.dialogRef.close(false);
          },
        });
    else {
      this.message = 'Provide new value';
    }
  }
}
