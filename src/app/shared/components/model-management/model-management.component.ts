import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import {
  PublicSolutionDetailsModel,
  PublicSolutionDetailsRevisionModel,
  Revision,
  UserDetails,
} from '../../models';
import {
  BehaviorSubject,
  Observable,
  Subscription,
  map,
  of,
  switchMap,
  tap,
} from 'rxjs';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { BreadcrumbNavigationComponent } from '../breadcrumb-navigation/breadcrumb-navigation.component';
import { VersionDropdownComponent } from '../version-dropdown/version-dropdown.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { TruncatePipe } from '../../pipes/truncate.pipe';

@Component({
  selector: 'gp-model-management',
  standalone: true,
  imports: [
    CommonModule,
    MatSidenavModule,
    MatTabsModule,
    MatIconModule,
    RouterModule,
    BreadcrumbNavigationComponent,
    VersionDropdownComponent,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    TruncatePipe,
  ],
  templateUrl: './model-management.component.html',
  styleUrl: './model-management.component.scss',
})
export class ModelManagementComponent implements OnInit {
  solutionId!: string;
  revisionId!: string;
  selectedRevision!: Revision;
  selectedRevisionSubscription!: Subscription;
  sharedWith: UserDetails[] = [];
  solution!: PublicSolutionDetailsModel;
  revisionsList!: Revision[];
  solution$!: Observable<PublicSolutionDetailsModel>;
  revisions$!: Observable<Revision[]>;
  sharedWith$!: Observable<UserDetails[]>;
  selectedRevision$: BehaviorSubject<Revision | null> =
    new BehaviorSubject<Revision | null>(null);

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sharedDataService: SharedDataService,
    private publicSolutionsService: PublicSolutionsService,
    private privateCatalogsService: PrivateCatalogsService,
  ) {}
  ngOnInit(): void {
    this.solution$ = this.activatedRoute.params.pipe(
      switchMap((params) => {
        this.solutionId = params['solutionId'];
        this.revisionId = params['revisionId'];
        this.sharedWith$ = this.privateCatalogsService.getShareWithTeam(
          this.solutionId,
        );
        return this.publicSolutionsService.getSolutionDetails(
          this.solutionId,
          this.revisionId,
        );
      }),
      tap((solution) => {
        this.revisions$ = this.getRevisionsAsObservable(solution);
        this.revisions$ = this.getRevisionsAsObservable(solution);
        this.updateSelectedRevision(this.revisionId, solution);
        this.setRevisionInService(solution.revisions[0]);
      }),
    );
  }

  private updateSelectedRevision(
    revisionId: string,
    solution: PublicSolutionDetailsModel,
  ): void {
    const revision = solution.revisions.find(
      (rev) => rev.revisionId === revisionId,
    );
    if (revision) {
      this.selectedRevision$.next({
        revisionId: revision.revisionId,
        version: revision.version,
        onBoarded: revision.onboarded,
      });
    }
  }

  isActive(modelRoute: string): boolean {
    return this.router.routerState.snapshot.url === modelRoute;
  }

  loadData(solutionId: string, revisionId: string) {
    this.publicSolutionsService
      .getSolutionDetails(solutionId, revisionId)
      .subscribe((res) => {
        const revisionsList = this.getRevisionsList(res);
        this.selectedRevision = revisionsList[0];
        this.setRevisionInService(this.selectedRevision);
      });
  }

  getRevisionsList(solution: PublicSolutionDetailsModel): Revision[] {
    return solution.revisions.map(
      (revision: PublicSolutionDetailsRevisionModel) => ({
        version: revision.version,
        revisionId: revision.revisionId,
        onBoarded: revision.onboarded,
      }),
    );
  }

  getShareWithTeam(solutionId: string): void {
    this.privateCatalogsService.getShareWithTeam(solutionId).subscribe({
      next: (users) => {
        this.sharedWith = users;
      },
      error: (error) => {
        console.error('Error fetching users:', error);
      },
    });
  }
  onHomeClick() {}

  onManageMyModelClick() {}

  private getRevisionsAsObservable(
    solution: PublicSolutionDetailsModel,
  ): Observable<Revision[]> {
    return new Observable<Revision[]>((subscriber) => {
      const revisions = solution.revisions.map((rev) => ({
        version: rev.version,
        revisionId: rev.revisionId,
        onBoarded: rev.onboarded,
      }));
      subscriber.next(revisions);
      subscriber.complete();
    });
  }

  onChangeVersion(revision: Revision): void {
    this.selectedRevision$.next(revision);
    this.setRevisionInService(revision);
    this.setVersionIdInService(revision.version);
  }

  setRevisionInService(revision: Revision): void {
    this.sharedDataService.selectedRevision = revision;
  }

  setVersionIdInService(version: string): void {
    this.sharedDataService.versionId = version;
  }
}
