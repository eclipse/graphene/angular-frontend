import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PublicSolution } from '../../models';

@Component({
  selector: 'gp-publisher',
  standalone: true,
  imports: [CommonModule, MatTooltipModule],
  templateUrl: './publisher.component.html',
  styleUrl: './publisher.component.scss',
})
export class PublisherComponent {
  @Input() item!: PublicSolution;
}
