import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadLicenseProfileComponent } from './upload-license-profile.component';

describe('UploadLicenseProfileComponent', () => {
  let component: UploadLicenseProfileComponent;
  let fixture: ComponentFixture<UploadLicenseProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UploadLicenseProfileComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UploadLicenseProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
