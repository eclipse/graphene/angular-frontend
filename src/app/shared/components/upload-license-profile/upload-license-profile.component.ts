import {
  Component,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
import { DndDirective } from '../../directives/dnd.directive';
import { AlertService } from 'src/app/core/services/alert.service';
import { Alert, AlertType, LicenseProfileModel } from '../../models';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import {
  Observable,
  Subject,
  Subscription,
  catchError,
  combineLatest,
  filter,
  finalize,
  map,
  of,
  switchMap,
  takeUntil,
  tap,
  throwError,
} from 'rxjs';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';

@Component({
  selector: 'gp-upload-license-profile',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressBarModule,
    MatIconModule,
    FormsModule,
    DndDirective,
  ],
  templateUrl: './upload-license-profile.component.html',
  styleUrl: './upload-license-profile.component.scss',
})
export class UploadLicenseProfileComponent implements OnInit, OnDestroy {
  @ViewChild('fileDropRef') fileDropRef!: ElementRef<HTMLInputElement>;
  routeSub!: Subscription;
  file: File | null = null;
  filename: string | null = null;
  progressBar = 0;
  modelUploadError: boolean = false;
  versionId: string = '';
  versionIdSubscription: Subscription | undefined;
  modelLicense!: LicenseProfileModel;
  isLicenseFound: boolean = false;
  changeLicense: boolean = false;
  progressBarValue = 0;
  modelUploadErrorMsg: string[] = [];
  fileSize = 0;
  userId$: Observable<string | undefined>;
  licenseProfile!: LicenseProfileModel;
  private onDestroy = new Subject<void>();
  alertSuccessMessage!: string;
  alertErrorMessage!: string;
  alertType!: AlertType;
  isLicenseProfile!: boolean;
  isCheckExtension!: boolean;
  expectedExtension!: string;
  errorMessage!: string;
  supportedFileText!: string;
  title!: string;
  isProcessEvent!: boolean;
  accept!: string;

  constructor(
    private alertService: AlertService,
    private browserStorageService: BrowserStorageService,
    private sharedDataService: SharedDataService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<UploadLicenseProfileComponent>,
  ) {
    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
  }
  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();

    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.versionIdSubscription) {
      this.versionIdSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.isLicenseProfile = this.data.dataKey.isLicenseProfile;
    this.isCheckExtension = this.data.dataKey.isCheckExtension;
    this.expectedExtension = this.data.dataKey.expectedExtension;
    this.errorMessage = this.data.dataKey.errorMessage;
    this.supportedFileText = this.data.dataKey.supportedFileText;
    this.title = this.data.dataKey.title;
    this.isProcessEvent = this.data.dataKey.isProcessEvent;
    this.accept = this.data.dataKey.accept;

    // Get the initial version ID
    this.versionId = this.sharedDataService.versionId;

    // Subscribe to changes in versionId
    this.versionIdSubscription = this.sharedDataService.versionId$.subscribe(
      (versionId: string) => {
        this.versionId = versionId;
      },
    );
  }
  uploadFile(file: File) {
    if (this.isCheckExtension) {
      const fileExtension = this.getFilenameExtension(file.name);
      if (fileExtension !== this.expectedExtension) {
        this.handleUploadError(this.errorMessage);

        return;
      }
    }

    this.data.dataKey.action(file).subscribe({
      next: (event: any) => {
        console.log({ event });
        if (this.isProcessEvent) this.processEvent(event);
        else {
          this.handleUploadSuccess(event);
        }
      },
      error: (err: any) => {
        this.handleUploadError(err);
      },
    });
  }

  processEvent(event: any): void {
    if (event && event.type === HttpEventType.UploadProgress && event.total) {
      this.progressBarValue = Math.round((100 * event.loaded) / event.total);
    } else if (event && event.type === HttpEventType.Response) {
      this.handleUploadSuccess(event.body);
      if (this.isLicenseProfile)
        this.sharedDataService.licenseProfile = this.licenseProfile;
    }
  }

  private handleUploadSuccess(response: any) {
    const alert: Alert = {
      message: 'File uploaded successfully',
      type: AlertType.Success,
    };
    this.alertService.notify(alert, 5000);
    this.dialogRef.close(true);
  }

  private handleUploadError(error: any) {
    this.modelUploadError = true;
    if (error.error) {
      const messageError = error.error.response_detail ?? error.error;
      this.modelUploadErrorMsg = [messageError];
    } else {
      this.modelUploadErrorMsg = [error];
    }
  }

  // Function to reset progress
  resetProgress() {
    this.progressBar = 0;
  }

  onFileDropped(file: File) {
    this.file = file;
    this.filename = file.name;
  }

  readFile(file: File) {
    const reader = new FileReader();
    if (this.isLicenseProfile)
      reader.onload = () => {
        try {
          this.licenseProfile = JSON.parse(reader.result as string);
        } catch (e) {}
      };
    reader.onerror = () => {
      console.error('Error reading the file:', reader.error);
    };
    reader.readAsText(file);
  }

  handleFileInput(event: any) {
    event.preventDefault();
    let file: File | null = null;
    if (event instanceof File) {
      file = event;
    } else {
      const element = event.target as HTMLInputElement;
      file = element.files?.[0] || null;
    }
    if (file) {
      this.file = file;
      this.readFile(this.file);
      if (this.file.size / 1024 / 1024 < 1) {
        this.fileSize = Math.ceil(file.size / 1024);
      }
      this.uploadFile(this.file);
    }
  }

  deleteFile() {
    this.file = null;
    this.modelUploadErrorMsg = [];
    this.modelUploadError = false;
  }

  getFilenameExtension(filename: string): string {
    // Split the filename by dot (.) and get the last element of the array
    const parts = filename.split('.');
    return parts[parts.length - 1];
  }

  idCheckExtensionValid(
    fileExtension: string,
    expectedExtension: string,
  ): boolean {
    return fileExtension === expectedExtension;
  }
}
