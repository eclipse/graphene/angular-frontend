import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'gp-solution-id',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './solution-id.component.html',
  styleUrl: './solution-id.component.scss',
})
export class SolutionIdComponent {
  @Input() solutionId!: string;
}
