import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolutionIdComponent } from './solution-id.component';

describe('SolutionIdComponent', () => {
  let component: SolutionIdComponent;
  let fixture: ComponentFixture<SolutionIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SolutionIdComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SolutionIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
