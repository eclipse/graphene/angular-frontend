import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedItemBaseComponent } from './shared-item-base.component';

describe('SharedItemBaseComponent', () => {
  let component: SharedItemBaseComponent;
  let fixture: ComponentFixture<SharedItemBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedItemBaseComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SharedItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
