import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { Router } from '@angular/router';
import { PublicSolution } from '../../models';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gp-shared-item-base',
  standalone: true,
  imports: [CommonModule],
  template: '',
  styles: [],
})
export abstract class SharedItemBaseComponent {
  @Input() item!: PublicSolution;
  @Input() isFavoriteSolution!: boolean;
  @Input() items!: PublicSolution[];
  @Input() isMarketPlacePage = false;
  @Input() viewTile!: boolean;
  @Input() isPublishedSolution!: boolean;
  imageToShow: any;

  isImageLoading = true;
  Tag: boolean = false;
  slnID!: string;
  selected: any[] = [];
  tagFilter: any[] = [];
  siteConfigTag: any[] = [];

  private destroy$: Subscription = new Subscription();
  userId!: string;

  @Output() favoriteClicked = new EventEmitter<string>();

  onFavoriteClick(solutionId: string): void {
    this.favoriteClicked.emit(solutionId);
  }

  constructor(
    protected publicSolutionsService: PublicSolutionsService,
    protected privateCatalogsService: PrivateCatalogsService,
    protected router: Router,
    private browserStorageService: BrowserStorageService,
  ) {}

  ngOnInit(): void {
    this.destroy$.add(
      this.browserStorageService.getUserDetails().subscribe((details) => {
        this.userId = details?.userId ?? '';
        this.initAfterUserId();
      }),
    );
  }

  private initAfterUserId(): void {
    if (this.item) {
      this.loadItemImage();
    }
  }

  private loadItemImage(): void {
    this.publicSolutionsService
      .getPictureOfSolution(this.item.solutionId)
      .subscribe(
        (data) => {
          this.createImageFromBlob(data);
          this.isImageLoading = false;
        },
        (error) => {
          this.isImageLoading = false;
          console.error('Error loading image', error);
        },
      );
  }

  createImageFromBlob(image: Blob): void {
    let reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        this.imageToShow = reader.result;
      },
      false,
    );

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  getStarWidth(avgRatingValue: number | null): { width: string } {
    if (avgRatingValue !== null) {
      const starPercentage = (avgRatingValue / 5) * 100;
      const starPercentageRounded = Math.round(starPercentage / 10) * 10;
      return { width: `${starPercentageRounded}%` };
    } else {
      return { width: '0%' };
    }
  }

  goToModelDetails(): void {
    if (this.item && this.item.solutionId) {
      const solutionId = this.item.solutionId;
      const revisionId = this.item.latestRevisionId;
      const path = this.userId
        ? '/dashboard/marketSolutions/model-details'
        : '/marketSolutions/model-details';
      this.router.navigate([path, solutionId, revisionId]);
    }
  }
}
