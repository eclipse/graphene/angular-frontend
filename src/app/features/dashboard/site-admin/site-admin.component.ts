import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'gp-site-admin',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './site-admin.component.html',
  styleUrl: './site-admin.component.scss'
})
export class SiteAdminComponent {

}
