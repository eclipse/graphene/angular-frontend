import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishRequestComponent } from './publish-request.component';

describe('PublishRequestComponent', () => {
  let component: PublishRequestComponent;
  let fixture: ComponentFixture<PublishRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PublishRequestComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PublishRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
