import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'gp-publish-request',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './publish-request.component.html',
  styleUrl: './publish-request.component.scss'
})
export class PublishRequestComponent {

}
