import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule } from '@angular/material/sidenav';
import { Router, RouterModule } from '@angular/router';
import { FiltersComponent } from 'src/app/shared/components/filters/filters.component';
import { CardItemComponent } from 'src/app/shared/components/card-item/card-item.component';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatGridListModule } from '@angular/material/grid-list';
import { ListItemComponent } from 'src/app/shared/components/list-item/list-item.component';
import { MatButtonModule } from '@angular/material/button';
import { HeadlineComponent } from 'src/app/shared/components/headline/headline.component';
import { SolutionsToggleViewComponent } from 'src/app/shared/components/card-list-view/solutions-toggle-view.component';
import { SortByComponent } from 'src/app/shared/components/sort-by/sort-by.component';
import { map, Observable, Subscription } from 'rxjs';
import {
  CatalogFilter,
  CategoryCode,
  categoryCodeMap,
  FieldToDirectionMap,
  Filter,
  PublicSolution,
  PublicSolutionsRequestPayload,
  SortOption,
  Tag,
} from 'src/app/shared/models';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { FiltersService } from 'src/app/core/services/filters.service';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { environment } from 'src/environments/environment';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { ModelListComponent } from 'src/app/shared/components/model-list/model-list.component';
import { BreadcrumbNavigationComponent } from 'src/app/shared/components/breadcrumb-navigation/breadcrumb-navigation.component';

export type ModelType = 'published' | 'unpublished' | 'both';

@Component({
  selector: 'gp-my-models',
  standalone: true,
  imports: [
    CommonModule,
    MatSidenavModule,
    RouterModule,
    FiltersComponent,
    CardItemComponent,
    MatPaginatorModule,
    MatGridListModule,
    SortByComponent,
    SolutionsToggleViewComponent,
    ListItemComponent,
    MatButtonModule,
    HeadlineComponent,
    MatChipsModule,
    MatIconModule,
    ModelListComponent,
    BreadcrumbNavigationComponent,
  ],
  templateUrl: './my-models.component.html',
  styleUrl: './my-models.component.scss',
})
export class MyModelsComponent implements OnInit {
  userId$: Observable<string | undefined>;
  private destroy$: Subscription = new Subscription();
  loginUserId!: string;
  publicSolutions!: PublicSolution[];
  favoriteSolutions!: PublicSolution[];
  pageSizeOptions = [25, 50, 100];
  pageSize = 25;
  pageIndex = 0;
  unpublishedTotalItems = 0;
  publishedTotalItems = 0;
  tags: Filter[] = [];
  categories: Filter[] = [];
  solutionPictureUrl = '';
  catalogIds: string[] = [];
  selectedTags: string[] = [];
  selectedCategories: string[] = [];
  searchKeyWord: string[] = [];
  sortByOptions: SortOption[] = [
    { name: 'Most Liked', value: 'ML' },
    { name: 'Fewest Liked', value: 'FL' },
    { name: 'Most Downloaded', value: 'MD' },
    { name: 'Fewest Downloaded', value: 'FD' },
    { name: 'Highest Reach', value: 'HR' },
    { name: 'Lowest Reach', value: 'LR' },
    { name: 'Most Recent', value: 'MR' },
    { name: 'Older', value: 'OLD' },
    { name: 'Name', value: 'name' },
    { name: 'Created Date', value: 'created' },
  ];
  sortByFilter: string = '';
  fieldToDirectionMap: FieldToDirectionMap = { viewCount: 'DESC' };
  viewTile: boolean = true;
  modelType: ModelType = 'both';

  private isFirstLoad = true;
  favoriteSolutionsMap: { [key: string]: boolean } = {};
  isUnpublishedSolutionsLoading: boolean = false;
  isPublishedSolutionsLoading: boolean = false;
  isOnMyFavoriteSelected = false;
  selectedPublishedSolutions!: PublicSolution[];
  allPublishedSolutions!: PublicSolution[];
  selectedUnpublishedSolutions!: PublicSolution[];
  allUnpublishedSolutions!: PublicSolution[];
  pageSelection: string[] = [];
  showEntirePage = false;

  constructor(
    private privateCatalogsService: PrivateCatalogsService,
    private filtersService: FiltersService,
    private router: Router,
    private browserStorageService: BrowserStorageService,
  ) {
    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
  }

  loadPublicSolutions() {
    let requestPayload: PublicSolutionsRequestPayload = {
      userId: this.loginUserId,

      modelTypeCodes: this.selectedCategories,
      tags: this.selectedTags,
      nameKeyword: this.searchKeyWord,
      sortBy: this.sortByFilter,
      pageRequest: {
        fieldToDirectionMap: this.fieldToDirectionMap,
        page: this.pageIndex,
        size: this.pageSize,
      },
    };
    if (this.modelType === 'both') {
      this.isUnpublishedSolutionsLoading = true;
      this.isPublishedSolutionsLoading = true;

      requestPayload.published = false;

      this.privateCatalogsService
        .fetchPublishedAndUnpublishedSolutions(requestPayload)
        .subscribe((publicSolutions) => {
          this.allUnpublishedSolutions = publicSolutions.publicSolutions ?? [];
          this.unpublishedTotalItems = publicSolutions?.totalElements ?? 0;

          this.unpublishedTotalItems > 4
            ? (this.selectedUnpublishedSolutions = this.slicedModels(
                4,
                this.allUnpublishedSolutions,
              ))
            : (this.selectedUnpublishedSolutions =
                this.allUnpublishedSolutions);

          if (this.isFirstLoad)
            this.tags = this.extractTagsFromPublicSolutions(
              this.selectedPublishedSolutions,
            );
          this.isUnpublishedSolutionsLoading = false;
        });
      requestPayload.published = true;
      this.privateCatalogsService
        .fetchPublishedAndUnpublishedSolutions(requestPayload)
        .subscribe((publicSolutions) => {
          this.allPublishedSolutions = publicSolutions.publicSolutions ?? [];
          this.publishedTotalItems = publicSolutions?.totalElements ?? 0;

          this.publishedTotalItems > 4
            ? (this.selectedPublishedSolutions = this.slicedModels(
                4,
                this.allPublishedSolutions,
              ))
            : (this.selectedPublishedSolutions = this.allPublishedSolutions);

          if (this.isFirstLoad)
            this.tags = [
              ...this.tags,
              ...this.extractTagsFromPublicSolutions(
                this.selectedPublishedSolutions,
              ),
            ];
          this.isPublishedSolutionsLoading = false;
        });
    } else if (this.modelType === 'unpublished') {
      this.isUnpublishedSolutionsLoading = true;
      requestPayload.published = false;

      this.privateCatalogsService
        .fetchPublishedAndUnpublishedSolutions(requestPayload)
        .subscribe((publicSolutions) => {
          this.allUnpublishedSolutions = publicSolutions.publicSolutions ?? [];
          this.unpublishedTotalItems = publicSolutions?.totalElements ?? 0;

          this.selectedUnpublishedSolutions = this.allUnpublishedSolutions;

          if (this.isFirstLoad)
            this.tags = this.extractTagsFromPublicSolutions(
              this.selectedUnpublishedSolutions,
            );
          this.isUnpublishedSolutionsLoading = false;
        });
    } else if (this.modelType === 'published') {
      this.isPublishedSolutionsLoading = true;
      requestPayload.published = true;
      this.privateCatalogsService
        .fetchPublishedAndUnpublishedSolutions(requestPayload)
        .subscribe((publicSolutions) => {
          this.allPublishedSolutions = publicSolutions.publicSolutions ?? [];
          this.publishedTotalItems = publicSolutions?.totalElements ?? 0;

          this.selectedPublishedSolutions = this.allPublishedSolutions;

          if (this.isFirstLoad)
            this.tags = this.extractTagsFromPublicSolutions(
              this.selectedPublishedSolutions,
            );
          this.isPublishedSolutionsLoading = false;
        });
    }
  }

  updateFavorite(solutionId: string): void {
    const dataObj = {
      request_body: {
        solutionId: solutionId,
        userId: this.loginUserId,
      },
      request_from: 'string',
      request_id: 'string',
    };

    if (!this.favoriteSolutionsMap[solutionId]) {
      this.privateCatalogsService.createFavorite(dataObj).subscribe(
        (response) => {
          this.favoriteSolutionsMap[solutionId] =
            !this.favoriteSolutionsMap[solutionId];

          // Handle response if needed
        },
        (error) => {
          // Handle error if needed
        },
      );
    } else {
      this.privateCatalogsService.deleteFavorite(dataObj).subscribe(
        (response) => {
          // Handle response if needed
          this.favoriteSolutionsMap[solutionId] =
            !this.favoriteSolutionsMap[solutionId];
        },
        (error) => {
          // Handle error if needed
        },
      );
    }
  }

  ngOnInit(): void {
    this.destroy$.add(
      this.browserStorageService.getUserDetails().subscribe((details) => {
        this.loginUserId = details?.userId ?? '';
      }),
    );
    this.sortByFilter = environment.ui_system_config.marketplace_sort_by;
    this.loadPublicSolutions();
    this.subscribeToUserId();
    this.filtersService.getModelTypes().subscribe(
      (res: CatalogFilter[]) => {
        this.categories = res.map((catalog: CatalogFilter) => ({
          name: catalog.name,
          selected: false,
        }));
      },
      (err: any) => {},
    );
  }

  onPageChange(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.loadPublicSolutions();
  }

  calculateStartIndex(pageIndex: number, pageSize: number): number {
    return pageIndex * pageSize + 1;
  }

  calculateEndIndex(
    totalItems: number,
    pageIndex: number,
    pageSize: number,
  ): number {
    const endIndex = (pageIndex + 1) * pageSize;
    return endIndex > totalItems ? totalItems : endIndex;
  }

  private extractTagsFromPublicSolutions(
    solutions: PublicSolution[],
  ): Filter[] {
    let tags: Filter[] = [];

    solutions.forEach((solution) => {
      if (solution.solutionTagList && solution.solutionTagList.length > 0) {
        solution.solutionTagList.forEach((tagObj: Tag) => {
          if (tagObj.tag) {
            const tag: Filter = {
              name: tagObj.tag.toLowerCase(),
              selected: false,
            };
            tags.push(tag);
          }
        });
      }
    });
    // Remove duplicates by name
    const uniqueTags: Filter[] = Array.from(
      new Map(tags.map((tag) => [tag.name, tag])).values(),
    );

    // Sort tags alphabetically by name
    uniqueTags.sort((a, b) => a.name.localeCompare(b.name));
    return uniqueTags;
  }

  handleSelectedTagsChange(selectedTags: string[]): void {
    this.isFirstLoad = false;
    this.selectedTags = selectedTags;
    this.loadPublicSolutions();
  }

  handleSelectedCategoriesChange(selectedCategories: string[]): void {
    this.isFirstLoad = false;
    // Convert selected categories to their codes
    const selectedCategoryCodes: CategoryCode[] =
      this.convertCategoriesToCodes(selectedCategories);

    // Update the selectedCategories property
    this.selectedCategories = selectedCategoryCodes;
    this.loadPublicSolutions();
  }

  convertCategoriesToCodes(selectedCategories: string[]): CategoryCode[] {
    return selectedCategories.map(
      (categoryName) => categoryCodeMap[categoryName] as CategoryCode,
    );
  }

  onSearchValueChanged(searchValue: string): void {
    this.isFirstLoad = false;
    this.searchKeyWord.push(searchValue);
    this.loadPublicSolutions();
  }

  getFieldToDirectionMap(sortBy: string): FieldToDirectionMap {
    switch (sortBy) {
      case 'FD':
        return { downloadCount: 'ASC' };
      case 'MD':
        return { downloadCount: 'DESC' };
      case 'FL':
        return { ratingAverageTenths: 'ASC' };
      case 'ML':
        return { ratingAverageTenths: 'DESC' };
      case 'HR':
        return { viewCount: 'DESC' };
      case 'LR':
        return { viewCount: 'ASC' };
      case 'MR':
        return { modified: 'DESC' };
      case 'OLD':
        return { modified: 'ASC' };
      case 'name':
        return { name: 'ASC' };
      case 'created':
        return { created: 'DESC' };
      default:
        throw new Error(`Invalid sortBy value: ${sortBy}`);
    }
  }

  updateFieldToDirectionMap(sortByFilter: string): void {
    this.fieldToDirectionMap = this.getFieldToDirectionMap(sortByFilter);
    this.isFirstLoad = false;
    this.loadPublicSolutions();
  }

  onViewTileChange(value: boolean): void {
    this.viewTile = value;
  }

  onHomeClick() {
    this.userId$.subscribe((userId) => {
      if (userId) {
        this.router.navigate(['/dashboard/home']);
      } else {
        this.router.navigate(['/home']);
      }
    });
  }

  loadFavoriteCatalogs(userId: string) {
    this.privateCatalogsService.getFavoriteSolutions(userId).subscribe(
      (solutions) => {
        this.favoriteSolutions = solutions;
        this.favoriteSolutionsMap = {};
        solutions.forEach((solution) => {
          this.favoriteSolutionsMap[solution.solutionId] = true;
        });
        // Handle your solutions here
      },
      (error) => console.error('Error loading favorite solutions:', error),
    );
  }

  subscribeToUserId() {
    this.userId$.subscribe((userId) => {
      if (userId) {
        this.loadFavoriteCatalogs(userId);
      }
    });
  }

  isFavoriteSolution(solutionId: string): boolean {
    return !!this.favoriteSolutionsMap[solutionId];
  }

  onAllCatalogsClick() {
    this.isOnMyFavoriteSelected = false;
    this.loadPublicSolutions();
  }
  onMyFavoriteCatalogsClick() {
    this.isOnMyFavoriteSelected = true;
    this.isPublishedSolutionsLoading = true;
    this.isUnpublishedSolutionsLoading = true;
    this.privateCatalogsService
      .getFavoriteSolutions(this.loginUserId)
      .subscribe(
        (solutions) => {
          this.favoriteSolutions = solutions;
          this.publicSolutions = solutions;
          // Update the favorite solutions map for quick lookup
          this.favoriteSolutionsMap = {};
          solutions.forEach((solution) => {
            this.favoriteSolutionsMap[solution.solutionId] = true;
          });
          // Handle your solutions here
          this.isUnpublishedSolutionsLoading = false;
          this.isPublishedSolutionsLoading;
        },
        (error) => console.error('Error loading favorite solutions:', error),
      );
  }

  slicedModels(val: number, models: PublicSolution[]): PublicSolution[] {
    return models.slice(0, val);
  }

  onClickSeeAll(type: ModelType) {
    this.modelType = type;
    this.showEntirePage = true;
    this.loadPublicSolutions();
  }
  onClickBack(type: ModelType) {
    this.modelType = type;
    this.showEntirePage = false;
    this.loadPublicSolutions();
  }
}
