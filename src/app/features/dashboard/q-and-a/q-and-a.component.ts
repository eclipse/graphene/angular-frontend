import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'gp-q-and-a',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './q-and-a.component.html',
  styleUrl: './q-and-a.component.scss'
})
export class QAndAComponent {

}
