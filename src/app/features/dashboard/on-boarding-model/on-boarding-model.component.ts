import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadlineComponent } from 'src/app/shared/components/headline/headline.component';
import { BreadcrumbNavigationComponent } from 'src/app/shared/components/breadcrumb-navigation/breadcrumb-navigation.component';
import { MatDividerModule } from '@angular/material/divider';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {
  MatCheckboxChange,
  MatCheckboxModule,
} from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { UploadLicenseProfileComponent } from 'src/app/shared/components/upload-license-profile/upload-license-profile.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CreateEditLicenseProfileComponent } from 'src/app/shared/components/create-edit-license-profile/create-edit-license-profile.component';
import { MatRadioModule } from '@angular/material/radio';
import { ModelDetailsLicenseProfileComponent } from 'src/app/shared/components/model-details-license-profile/model-details-license-profile.component';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import {
  Alert,
  AlertType,
  LicenseProfileModel,
  PublicSolution,
} from 'src/app/shared/models';
import { HttpEventType } from '@angular/common/http';
import { AlertService } from 'src/app/core/services/alert.service';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import {
  Observable,
  Subject,
  Subscription,
  catchError,
  first,
  map,
  of,
  switchMap,
  takeUntil,
  timer,
} from 'rxjs';
import { MatListModule } from '@angular/material/list';
import { Router } from '@angular/router';
import { ConfirmActionComponent } from 'src/app/shared/components/confirm-action/confirm-action.component';
import { MessageStatusModel } from 'src/app/shared/models/message-status.model';

@Component({
  selector: 'gp-on-boarding-model',
  standalone: true,
  imports: [
    CommonModule,
    HeadlineComponent,
    BreadcrumbNavigationComponent,
    MatDividerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    UploadLicenseProfileComponent,
    MatRadioModule,
    ModelDetailsLicenseProfileComponent,
    MatListModule,
  ],
  templateUrl: './on-boarding-model.component.html',
  styleUrl: './on-boarding-model.component.scss',
})
export class OnBoardingModelComponent implements OnInit {
  @ViewChild('fileDropRef') fileDropRef!: ElementRef<HTMLInputElement>;
  currentFile?: File;
  fileSize = 0;
  message = '';
  fileName = 'No chosen file';
  onboardingModelForm!: FormGroup;
  modelUploadError: boolean = false;
  modelUploadErrorMsg: string[] = [];
  userId$: Observable<string | undefined>;
  solutionTrackId!: string;
  enableSubmit: boolean = false;
  private onDestroy = new Subject<void>();
  private subscription: Subscription = new Subscription();
  modelLicense: LicenseProfileModel | null = null;
  userId!: string;
  messageStatuses!: MessageStatusModel[];
  modelNameExists!: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private sharedDataService: SharedDataService,
    public dialog: MatDialog,
    private alertService: AlertService,
    private privateCatalogsService: PrivateCatalogsService,
    private browserStorageService: BrowserStorageService,
    private router: Router,
  ) {
    this.onboardingModelForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.pattern('^(?=.*[a-z])[a-z0-9]+(?:[-.][a-z0-9]+)*$'),
        ],
        [this.validateNameNotTaken.bind(this)],
      ],
      dockerURI: ['', [Validators.required]],
      protobufFile: [null, [Validators.required]],
      addLicenseProfile: [false],
      licenseProfile: [null],
    });

    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
  }
  ngOnInit(): void {
    this.subscription.add(
      this.sharedDataService.licenseProfile$.subscribe((licenseProfile) => {
        this.modelLicense = licenseProfile;
        this.onboardingModelForm.patchValue({
          licenseProfile: licenseProfile,
        });
        /**
         * this.cd.detectChanges();  // Manually trigger change detection
         */
      }),
    );

    this.onboardingModelForm.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.checkFormValidity();
      });

    this.subscription.add(
      this.browserStorageService.getUserDetails().subscribe((details) => {
        this.userId = details?.userId ?? '';
      }),
    );
  }

  checkFormValidity() {
    const { name, dockerURI, addLicenseProfile } =
      this.onboardingModelForm.value;
    if (!addLicenseProfile) {
      this.enableSubmit =
        name &&
        dockerURI &&
        this.onboardingModelForm.controls['dockerURI'].status === 'VALID' &&
        this.currentFile;
    } else {
      this.enableSubmit =
        name && dockerURI && this.currentFile && this.modelLicense;
    }
  }

  onHomeClick() {}

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  selectFile(event: any): void {
    this.message = '';

    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      this.currentFile = file;
      this.fileName = this.currentFile.name;
      this.onboardingModelForm.patchValue({ protobufFile: file });
      const extensionFile = this.getFilenameExtension(this.fileName);
      if (extensionFile !== 'proto') {
        this.message = '.proto file is required.';
      } else if (this.currentFile && extensionFile === 'proto') {
        this.privateCatalogsService
          .uploadProtoBufFile(this.currentFile)
          .subscribe({
            next: (event) => this.processEvent(event),
            error: (error) => {}, // To catch any errors not caught by catchError
            complete: () => {},
          });
      }
    } /*  else {
      this.fileName = 'No chosen file';
    } */
  }

  uploadFile() {
    event?.preventDefault();
    this.fileDropRef.nativeElement.click();
    if (this.currentFile) {
      this.privateCatalogsService
        .uploadProtoBufFile(this.currentFile)
        .subscribe({
          next: (event) => this.processEvent(event),
          error: (error) => {}, // To catch any errors not caught by catchError
          complete: () => {},
        });
    }
  }

  getFilenameExtension(filename: string): string {
    // Split the filename by dot (.) and get the last element of the array
    const parts = filename.split('.');
    return parts[parts.length - 1];
  }

  onClickUpload(event: MatCheckboxChange) {}

  onCreateLicenseProfile() {
    const dialogRef: MatDialogRef<CreateEditLicenseProfileComponent> =
      this.dialog.open(CreateEditLicenseProfileComponent, {
        data: {
          dataKey: {
            isEditMode: false,
            solutionId: '',
            revisionId: '',
          },
        },
      });
    dialogRef.afterClosed().subscribe((result) => {});
  }

  onClickUploadLicenseProfile() {
    const dialogRef: MatDialogRef<UploadLicenseProfileComponent> =
      this.dialog.open(UploadLicenseProfileComponent);
  }

  resetData() {
    this.onboardingModelForm.reset();
    this.sharedDataService.licenseProfile = null;
    this.currentFile = undefined;
    this.fileName = 'No chosen file';
  }

  processEvent(event: any): void {
    if (event && event.type === HttpEventType.Response) {
      this.handleUploadSuccess(event.body);
    }
  }

  private handleUploadSuccess(response: any) {
    const alert: Alert = {
      message: 'File uploaded successfully',
      type: AlertType.Success,
    };
    this.alertService.notify(alert, 5000);
  }

  async onClickUploadProtoBufFile() {
    const dialogRef: MatDialogRef<UploadLicenseProfileComponent> =
      this.dialog.open(UploadLicenseProfileComponent, {
        data: {
          dataKey: {
            title: 'Upload Protobuf File',
            isEditMode: false,
            isCheckExtension: true,
            expectedExtension: 'proto',
            errorMessage: '.proto file is required.',
            supportedFileText: 'Supported files type: .proto',
            action: (file: File) => {
              this.currentFile = file;
              this.fileName = file.name;
              return this.privateCatalogsService.uploadProtoBufFile(file);
            },
            isLicenseProfile: false,
            isProcessEvent: false,
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      // This will be executed when the dialog is closed
      // Reload data to fetch the updated license profile
      this.checkFormValidity();
    });
  }

  viewModel(
    resp: MessageStatusModel[],
    dialogRef: MatDialogRef<ConfirmActionComponent>,
  ) {
    if (this.modelNameExists) {
      const solution = resp[0];
      const solutionId = solution.solutionId;
      const revisionId = solution.revisionId;
      const path = '/dashboard/marketSolutions/model-details';
      this.router.navigate([path, solutionId, revisionId]);
    } else this.router.navigate(['/dashboard/myModels']);

    dialogRef.close();
  }

  validateNameNotTaken(
    control: AbstractControl,
  ): Observable<ValidationErrors | null> {
    if (!control.value) {
      return of(null); // No value to check
    }

    return this.privateCatalogsService
      .searchSolutionsByName(control.value)
      .pipe(
        map((res) => {
          // Check if the name is already taken by verifying the response format and content
          const isNameTaken =
            res.totalElements > 0 &&
            res.publicSolutions.some((pb) => pb.active);
          this.modelNameExists = isNameTaken;
          return isNameTaken ? { nameTaken: true } : null;
        }),
        catchError((error) => {
          console.error('Error checking name availability', error);
          // Optionally return an error or a user-friendly message or simply null
          return of({ nameCheckFailed: true });
        }),
        first(), // Ensure the observable completes after the first response
      );
  }

  onClickBoardModel() {
    if (this.enableSubmit) {
      this.privateCatalogsService
        .addCatalog(
          this.userId,
          this.onboardingModelForm.value.name,
          this.onboardingModelForm.value.dockerURI,
        )
        .subscribe({
          next: (res) => {
            this.solutionTrackId = res;
            const alert: Alert = {
              message:
                'On-boarding process has started and it will take a few seconds to reflect the change in status.',
              type: AlertType.Info,
            };
            this.alertService.notify(alert, 5000);
            this.privateCatalogsService
              .getMessagingStatus(this.userId, this.solutionTrackId)
              .subscribe({
                next: (resp) => {
                  this.messageStatuses = resp;
                  if (resp) {
                    const dialogRef: MatDialogRef<ConfirmActionComponent> =
                      this.dialog.open(ConfirmActionComponent, {
                        data: {
                          dataKey: {
                            title: 'View model',
                            content: this.modelNameExists
                              ? 'Model successfully onboarded an incremented version is  created within the existing model'
                              : 'Model successfully onboarded',
                            secondAction: 'View model',
                            action: () => this.viewModel(resp, dialogRef),
                          },
                        },
                        autoFocus: false,
                      });

                    dialogRef.afterClosed().subscribe((result) => {
                      this.resetData();
                    });
                  }
                },
                error: (error) => {},
              });
          },
          error: (error) => {},
        });
    }
  }
}
