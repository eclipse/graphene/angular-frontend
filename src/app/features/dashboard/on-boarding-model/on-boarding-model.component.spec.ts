import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnBoardingModelComponent } from './on-boarding-model.component';

describe('OnBoardingModelComponent', () => {
  let component: OnBoardingModelComponent;
  let fixture: ComponentFixture<OnBoardingModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OnBoardingModelComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OnBoardingModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
