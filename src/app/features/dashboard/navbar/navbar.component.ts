import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, signal } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialog } from '@angular/material/dialog';
import { LocalLoginComponent } from '../../login/local-login/local-login.component';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Router } from '@angular/router';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { Observable, map } from 'rxjs';
@Component({
  selector: 'gp-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
  ],
})
export class NavbarComponent {
  @Input() collapsed = signal(false);
  firstName$: Observable<string | undefined>;

  constructor(
    private authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
    private browserStorageService: BrowserStorageService,
  ) {
    this.firstName$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.firstName));
  }

  openDialog() {
    this.dialog.open(LocalLoginComponent);
  }

  logout() {
    this.browserStorageService.removeUserDetail();
    this.browserStorageService.removeAuthToken();
    this.authService.logout();
    this.router.navigate(['/']);
  }
}
