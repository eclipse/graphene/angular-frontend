import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'gp-manage-license',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './manage-license.component.html',
  styleUrl: './manage-license.component.scss'
})
export class ManageLicenseComponent {

}
