import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, signal } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'gp-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatListModule,
    MatSidenavModule,
    MatTabsModule,
    MatIconModule,
  ],
})
export class SidebarComponent implements OnInit {
  sideNavCollapsed = signal(false);
  @Input() set collapsed(val: boolean) {
    this.sideNavCollapsed.set(val);
  }

  constructor(private router: Router) {}
  ngOnInit(): void {
    this.isMarketPlaceActive();
  }

  isActive(modelRoute: string): boolean {
    const currentUrl = this.router.routerState.snapshot.url;
    return currentUrl === modelRoute;
  }

  isMarketPlaceActive(): boolean {
    const currentUrl = this.router.routerState.snapshot.url;
    const marketSolutionsPath = '/dashboard/marketSolutions/model-details/';
    return currentUrl.includes(marketSolutionsPath);
  }

  onQAndAClick() {
    window.open(
      'https://gitlab.eclipse.org/eclipse/graphene/tutorials',
      '_blank',
    );
  }
}
