import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'gp-design-studio',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './design-studio.component.html',
  styleUrl: './design-studio.component.scss'
})
export class DesignStudioComponent {

}
