import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbNavigationComponent } from 'src/app/shared/components/breadcrumb-navigation/breadcrumb-navigation.component';
import { map, Observable } from 'rxjs';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { Router } from '@angular/router';
import { HeadlineComponent } from 'src/app/shared/components/headline/headline.component';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import {
  MatPaginator,
  MatPaginatorIntl,
  MatPaginatorModule,
} from '@angular/material/paginator';
import { Catalog } from 'src/app/shared/models';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddEditCatalogDialogComponent } from './add-edit-catalog-dialog/add-edit-catalog-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { DeleteUserDialogConfirmationActionComponent } from 'src/app/shared/components/delete-user-dialog-confirmation-action/delete-user-dialog-confirmation-action.component';
import { MatInputModule } from '@angular/material/input';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';

@Component({
  selector: 'gp-catalogs',
  standalone: true,
  imports: [
    CommonModule,
    BreadcrumbNavigationComponent,
    HeadlineComponent,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSortModule,
    MatFormFieldModule,
  ],
  templateUrl: './catalogs.component.html',
  styleUrl: './catalogs.component.scss',
})
export class CatalogsComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  userId$: Observable<string | undefined>;
  displayedColumns: string[] = [
    'name',
    'type',
    'publisher',
    'selfPublish',
    'accessTypeCode',
    'solutionCount',
    'created',
    'action',
  ];
  catalogs!: Catalog[];
  pageSizeOptions = [10, 25, 50, 100];
  siteInstanceName!: string;
  selectedCatalog!: Catalog;
  dataSource = new MatTableDataSource<Catalog>([]);

  constructor(
    private privateCatalogsService: PrivateCatalogsService,
    private router: Router,
    private browserStorageService: BrowserStorageService,
    private paginatorInt: MatPaginatorIntl,
    public dialog: MatDialog,
  ) {
    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
    paginatorInt.itemsPerPageLabel = 'Showing';
  }
  ngOnInit(): void {
    this.loadData();
    this.getInstanceName();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  onHomeClick() {
    this.userId$.subscribe((userId) => {
      if (userId) {
        this.router.navigate(['/dashboard/home']);
      } else {
        this.router.navigate(['/home']);
      }
    });
  }

  loadData() {
    this.privateCatalogsService.loadTotalCatalog().subscribe({
      next: (res) => {
        this.privateCatalogsService.loadCatalogs(0, res).subscribe({
          next: (res) => {
            this.catalogs = res.response_body.content;
            this.dataSource.data = res.response_body.content;
          },
          error: (error) => {},
        });
      },
      error: (error) => {},
    });
  }
  getInstanceName() {
    this.privateCatalogsService.getSiteConfig().subscribe({
      next: (res) => {
        JSON.parse(res.response_body.configValue).fields.forEach(
          (field: any) => {
            if (field.name === 'siteInstanceName') {
              this.siteInstanceName = field.data;
            }
          },
        );
      },
      error: (error) => {},
    });
  }

  addNewCatalog(newCatalog: Catalog) {
    const catalog: Catalog = {
      ...newCatalog,
      publisher: this.siteInstanceName,
    };

    return this.privateCatalogsService.createCatalog(catalog);
  }
  onClickAddNewCatalog() {
    const dialogRef: MatDialogRef<AddEditCatalogDialogComponent> =
      this.dialog.open(AddEditCatalogDialogComponent, {
        data: {
          dataKey: {
            action: (catalog: Catalog) => this.addNewCatalog(catalog),
            actionType: 'create',
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.loadData();
    });
  }

  editCatalog(editedCatalog: Catalog) {
    const catalog: Catalog = {
      ...editedCatalog,
      publisher: this.siteInstanceName,
      catalogId: this.selectedCatalog.catalogId,
    };

    return this.privateCatalogsService.updateCatalog(catalog);
  }

  onClickEditCatalog(catalog: Catalog) {
    this.selectedCatalog = catalog;
    const dialogRef: MatDialogRef<AddEditCatalogDialogComponent> =
      this.dialog.open(AddEditCatalogDialogComponent, {
        data: {
          dataKey: {
            action: (catalog: Catalog) => this.editCatalog(catalog),
            actionType: 'edit',
            modelData: this.selectedCatalog,
          },
        },
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.loadData();
    });
  }

  deleteCatalog(catalog: Catalog) {
    return this.privateCatalogsService.deleteCatalog(catalog);
  }

  onClickDeleteCatalog(catalog: Catalog) {
    this.selectedCatalog = catalog;
    const dialogRef: MatDialogRef<DeleteUserDialogConfirmationActionComponent> =
      this.dialog.open(DeleteUserDialogConfirmationActionComponent, {
        data: {
          dataKey: {
            title: `Delete Confirmation ${catalog.name}`,
            content: `Are you sure that you wish to delete' ${catalog.name}`,
            alertMessage: 'Catalog deleted successfully. ',

            action: () => this.deleteCatalog(catalog),
          },
        },
        autoFocus: false,
      });

    dialogRef.afterClosed().subscribe((result) => {
      this.loadData();
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
