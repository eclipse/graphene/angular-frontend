import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCatalogDialogComponent } from './add-edit-catalog-dialog.component';

describe('AddEditCatalogDialogComponent', () => {
  let component: AddEditCatalogDialogComponent;
  let fixture: ComponentFixture<AddEditCatalogDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AddEditCatalogDialogComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEditCatalogDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
