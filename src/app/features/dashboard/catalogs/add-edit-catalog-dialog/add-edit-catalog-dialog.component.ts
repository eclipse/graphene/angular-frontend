import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AlertService } from 'src/app/core/services/alert.service';
import {
  AccessTypeCode,
  AlertType,
  CatalogType,
  SortOption,
} from 'src/app/shared/models';

@Component({
  selector: 'gp-add-edit-catalog-dialog',
  standalone: true,
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatCheckboxModule,
    MatSelectModule,
  ],
  templateUrl: './add-edit-catalog-dialog.component.html',
  styleUrl: './add-edit-catalog-dialog.component.scss',
})
export class AddEditCatalogDialogComponent implements OnInit {
  title!: string;
  alertMessage!: string;
  addEditDialogForm!: FormGroup;
  message: string = '';
  catalogTypes = Object.keys(CatalogType).map((key) => ({
    name: key,
    value: CatalogType[key as keyof typeof CatalogType],
  }));
  accessTypeCodes = Object.keys(AccessTypeCode).map((key) => ({
    name: key,
    value: AccessTypeCode[key as keyof typeof AccessTypeCode],
  }));

  constructor(
    public dialogRef: MatDialogRef<AddEditCatalogDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
  ) {
    this.addEditDialogForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      accessTypeCode: ['', [Validators.required]],
      type: ['', [Validators.required]],
      description: ['', [Validators.required]],
      selfPublish: [false, [Validators.required]],
    });
  }

  ngOnInit(): void {
    console.log('modelData', this.data.dataKey.modelData);
    this.title = this.data.dataKey.title;
    this.alertMessage = this.data.dataKey.alertMessage;
    if (this.data.dataKey.actionType === 'edit') {
      this.addEditDialogForm.patchValue({
        name: this.data.dataKey.modelData.name,
      });
      this.addEditDialogForm.patchValue({
        accessTypeCode: this.data.dataKey.modelData.accessTypeCode,
      });
      this.addEditDialogForm.patchValue({
        type: this.data.dataKey.modelData.type,
      });
      this.addEditDialogForm.patchValue({
        description: this.data.dataKey.modelData.description,
      });
      this.addEditDialogForm.patchValue({
        selfPublish: this.data.dataKey.modelData.selfPublish,
      });
      console.log('form after update', this.addEditDialogForm.value);
    }
  }

  addEditCatalog() {
    if (this.addEditDialogForm.valid)
      this.data.dataKey.action(this.addEditDialogForm.value).subscribe({
        next: (res: any) => {
          this.alertService.notify(
            {
              message:
                this.data.dataKey.actionType === 'create'
                  ? 'Catalog created successfully'
                  : 'Catalog updated successfully',
              type: AlertType.Success,
            },
            3000,
          );
          this.dialogRef.close(true);
        },
        error: (err: any) => {
          this.alertService.notify(
            { message: 'Operation failed', type: AlertType.Error },
            3000,
          );
          this.dialogRef.close(false);
        },
      });
  }

  get controls() {
    return this.addEditDialogForm.controls;
  }
}
