import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import {
  Observable,
  Subscription,
  catchError,
  combineLatest,
  map,
  of,
  switchMap,
  tap,
} from 'rxjs';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { LoginComponent } from '../login/login.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ViewCountComponent } from 'src/app/shared/icons/view-count/view-count.component';
import { DownloadCountComponent } from 'src/app/shared/icons/download-count/download-count.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FooterComponent } from 'src/app/shared/components/footer/footer.component';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedDataService } from 'src/app/core/services/shared-data/shared-data.service';
import { RateModelDetailsComponent } from 'src/app/shared/components/rate-model-details/rate-model-details.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import {
  AllUserRating,
  AuthorPublisherModel,
  AverageRatings,
  Catalog,
  CommentModel,
  CommentReplyModel,
  PublicSolution,
  PublicSolutionDetailsModel,
  PublicSolutionDetailsRevisionModel,
  Revision,
  Tag,
} from 'src/app/shared/models';
import { environment } from 'src/environments/environment';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtTokenService } from 'src/app/core/services/auth/jwt-token.service';
import { apiConfig } from 'src/app/core/config';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { SolutionIdComponent } from 'src/app/shared/components/solution-id/solution-id.component';
import { BreadcrumbNavigationComponent } from 'src/app/shared/components/breadcrumb-navigation/breadcrumb-navigation.component';
import { VersionDropdownComponent } from 'src/app/shared/components/version-dropdown/version-dropdown.component';

interface SolutionData {
  solutionId: string;
  revisionId: string;
  solution: PublicSolutionDetailsModel;
  catalogs: Catalog[];
  picture?: any;
  averageRatings?: AverageRatings;
  allRatings?: AllUserRating[];
  comments: any;
  authors: AuthorPublisherModel[];
  relatedSolutions?: PublicSolution[];
}

@Component({
  selector: 'gp-model-details',
  standalone: true,
  imports: [
    CommonModule,
    MatTooltipModule,
    MatMenuModule,
    MatIconModule,
    ViewCountComponent,
    DownloadCountComponent,
    MatSidenavModule,
    RouterModule,
    FooterComponent,
    MatTabsModule,
    RateModelDetailsComponent,
    MatDividerModule,
    MatChipsModule,
    MatListModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    SolutionIdComponent,
    BreadcrumbNavigationComponent,
    VersionDropdownComponent,
  ],
  templateUrl: './model-details.component.html',
  styleUrl: './model-details.component.scss',
  providers: [{ provide: 'Window', useValue: window }],
})
export class ModelDetailsComponent implements OnInit {
  exportTo!: string;
  solutionId!: string;
  revisionId!: string;
  solution!: PublicSolutionDetailsModel;
  imageToShow: any;
  isImageLoading = true;
  selectedDefaultCatalog!: Catalog;
  revisionsList!: Revision[];
  selectedDefaultRevision!: Revision;
  averageRatings!: AverageRatings;
  allRatings: AllUserRating[] = [];
  totalRatingsCount: number = 0;
  ratingCounts: number[] = [0, 0, 0, 0, 0];
  perRatingCounts: number[] = [0, 0, 0, 0, 0];
  solution$!: Observable<any>;
  deployMenu!: {
    title: string;
    icon_url: string;
    local_service_path: string;
    produces_download: boolean;
  }[];

  userId$: Observable<string | undefined> = this.browserStorageService
    .getUserDetails()
    .pipe(map((details) => details?.userId));
  isLoggedIn$: Observable<boolean>;
  userId!: string;
  data$: Observable<SolutionData>;

  defaultSolutionData: SolutionData = {
    solution: {} as PublicSolutionDetailsModel,
    catalogs: [],
    picture: null,
    averageRatings: {} as AverageRatings,
    allRatings: [],
    comments: { response_body: { content: [] } },
    authors: [],
    relatedSolutions: [],
    solutionId: '',
    revisionId: '',
  };

  rating$!: Observable<number>;
  comments$!: Observable<CommentModel[]>;
  comments!: CommentReplyModel[];
  firstName: string = '';
  lastName: string = '';
  editModel: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private publicSolutionsService: PublicSolutionsService,
    private privateCatalogsService: PrivateCatalogsService,
    private browserStorageService: BrowserStorageService,
    public dialog: MatDialog,
    private sharedDataService: SharedDataService,
    @Inject('Window') private window: Window,
    private jwtTokenService: JwtTokenService,
  ) {
    this.isLoggedIn$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => !!details?.userId));

    this.data$ = this.activatedRoute.params.pipe(
      map((params) => ({
        solutionId: params['solutionId'] as string,
        revisionId: params['revisionId'] as string,
      })),
      switchMap(({ solutionId, revisionId }) =>
        this.publicSolutionsService
          .getSolutionDetails(solutionId, revisionId)
          .pipe(
            switchMap((solution) =>
              combineLatest({
                solutionId: of(solutionId),
                revisionId: of(revisionId),
                solution: of(solution),
                catalogs:
                  this.publicSolutionsService.getCatalogsOfSolution(solutionId),
                picture: this.publicSolutionsService
                  .getPictureOfSolution(solutionId)
                  .pipe(catchError(() => of(null))),
                averageRatings: this.publicSolutionsService
                  .getAverageRatings(solutionId)
                  .pipe(catchError(() => of({} as AverageRatings))),
                allRatings: this.publicSolutionsService
                  .getAllRatings(solutionId)
                  .pipe(catchError(() => of([]))),
                comments: this.publicSolutionsService
                  .getComment(solutionId, revisionId)
                  .pipe(
                    catchError(() => of({ response_body: { content: [] } })),
                  ),
                authors: this.publicSolutionsService
                  .getModelAuthors(solutionId, revisionId)
                  .pipe(catchError(() => of([]))),
                relatedSolutions: this.publicSolutionsService
                  .relatedSolutions(solution)
                  .pipe(catchError(() => of([]))),
              }),
            ),
            tap((data) => {
              this.processSideEffects(data);
            }),
            map((data) => data as SolutionData),
            catchError((error) => {
              console.error('Error loading data:', error);
              return of(this.defaultSolutionData);
            }),
          ),
      ),
    );
  }

  private initializeDataStreams() {
    this.rating$ = combineLatest([
      this.activatedRoute.params,
      this.userId$,
    ]).pipe(
      switchMap(([params, userId]) => {
        if (!userId) {
          // Handle the case where userId is not available
          return of(null);
        }
        this.userId = userId;
        this.solutionId = params['solutionId'];
        this.publicSolutionsService
          .getSolutionDetails(params['solutionId'], params['revisionId'])
          .subscribe((res) => {
            this.disableEdit(userId, res);
          });
        return this.privateCatalogsService.getSolutionRatings(
          this.solutionId,
          userId,
        );
      }),
      map((response) => response.response_body.rating),
      tap((rating) => {
        if (rating !== null && rating !== undefined) {
          // Update the shared service
          this.sharedDataService.rating = rating;
        }
      }),
      catchError((error) => {
        console.error('Error fetching ratings:', error);
        return of(0); // Return a default value or handle the error appropriately
      }),
    );
  }

  ngOnInit() {
    this.browserStorageService.getUserDetails().subscribe((res) => {
      this.firstName = res?.firstName ?? '';
      this.lastName = res?.lastName ?? '';
    });
    this.initializeDataStreams();
    this.deployMenu = environment.ui_system_config.deploy_menu;
    this.activatedRoute.params.subscribe((params) => {
      this.solutionId = params['solutionId'];
      this.revisionId = params['revisionId'];
      this.updateRating();
      this.getComments(this.solutionId, this.revisionId);
    });
  }

  getRevisionsList(solution: PublicSolutionDetailsModel): Revision[] {
    return solution.revisions.map(
      (revision: PublicSolutionDetailsRevisionModel) => ({
        version: revision.version,
        revisionId: revision.revisionId,
        onBoarded: revision.onboarded,
      }),
    );
  }

  setSelectedCatalogIdInService(): void {
    const selectedCatalogId = this.selectedDefaultCatalog.catalogId;
    this.sharedDataService.selectedCatalogId = selectedCatalogId ?? '';
  }

  setVersionIdInService(): void {
    const versionId = this.selectedDefaultRevision.version;
    this.sharedDataService.versionId = versionId;
  }

  setRevisionInService(revision: Revision): void {
    this.sharedDataService.selectedRevision = revision;
  }

  seSolutionInService(solution: PublicSolution): void {
    this.sharedDataService.solution = solution;
  }

  setImageToShowInService(): void {
    const imageToShow = this.imageToShow;
    this.sharedDataService.imageToShow = imageToShow;
  }

  setAuthorListInService(authorList: AuthorPublisherModel[]): void {
    this.sharedDataService.authorList = authorList;
  }

  updateStarWidth(averageRatings: AverageRatings) {
    if (averageRatings) {
      const starPercentage = (averageRatings.ratingAverageTenths / 5) * 100;
      const starPercentageRounded = Math.round(starPercentage / 10) * 10;
      const starsInner = document.querySelector('.stars-inner') as HTMLElement;
      if (starsInner) {
        starsInner.style.width = starPercentageRounded + '%';
      }
    }
  }

  calculateRatings(allUserRatings: AllUserRating[]) {
    this.ratingCounts = [0, 0, 0, 0, 0];

    allUserRatings.forEach((rating) => {
      if (rating.rating >= 1 && rating.rating <= 5) {
        this.ratingCounts[5 - rating.rating]++;
      }
    });

    this.totalRatingsCount = this.ratingCounts.reduce(
      (total, count) => total + count,
      0,
    );

    if (this.totalRatingsCount > 0) {
      this.perRatingCounts = this.ratingCounts.map(
        (count) => (count / this.totalRatingsCount) * 100,
      );
    } else {
      this.perRatingCounts = [0, 0, 0, 0, 0];
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        this.imageToShow = reader.result;
        this.setImageToShowInService();
      },
      false,
    );

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  onHomeClick(): void {
    this.isLoggedIn$.subscribe((isLoggedIn) => {
      const destination = isLoggedIn ? '/dashboard/home' : '/home';
      this.router.navigate([destination]);
    });
  }

  onMarketPlaceClick(): void {
    this.isLoggedIn$.subscribe((isLoggedIn) => {
      const destination = isLoggedIn
        ? '/dashboard/marketplace'
        : '/marketplace';
      this.router.navigate([destination]);
    });
  }

  onChangeVersion(revision: Revision) {
    this.selectedDefaultRevision = revision;
    this.setVersionIdInService();
    this.setRevisionInService(revision);
    // later load details version should be added
  }

  openSignInDialog() {
    const dialogRef: MatDialogRef<LoginComponent> =
      this.dialog.open(LoginComponent);

    dialogRef.afterClosed().subscribe((result) => {
      // handle dialog close
    });
  }

  scrollToComment(view: string) {
    const elementId = view === 'edit' ? 'editComment' : 'discussionTab';
    const element = document.getElementById(elementId);
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
      if (view === 'edit') {
        element.focus();
      }
    }
  }

  isActive(modelRoute: string): boolean {
    return this.router.routerState.snapshot.url === modelRoute;
  }

  removeCurrentSolution(
    relatedSolutions: PublicSolution[],
    currentSolution: PublicSolution,
  ) {
    relatedSolutions.filter(
      (solution) => solution.solutionId !== currentSolution.solutionId,
    );
  }

  //logic should be added later
  changeSelectedCatalogId(catalogId: string, catalogName: string) {}

  onClickShowAll() {
    this.router.navigate(['/marketplace']);
  }

  goToRelatedSolutions(solutionId: string, revisionId: string): void {
    this.router.navigate([
      '/marketSolutions/model-details',
      solutionId,
      revisionId,
    ]);
  }

  private processSideEffects(data: SolutionData): void {
    this.revisionsList = this.getRevisionsList(data.solution);
    this.selectedDefaultRevision = this.revisionsList.filter(
      (rv) => rv.revisionId === this.revisionId,
    )[0];
    this.setRevisionInService(this.selectedDefaultRevision);

    if (data.catalogs.length > 0) {
      this.selectedDefaultCatalog = data.catalogs[0];
      this.setSelectedCatalogIdInService();
    }
    this.setVersionIdInService();
    this.createImageFromBlob(data.picture);
    this.updateStarWidth(data.averageRatings || ({} as AverageRatings));
    this.calculateRatings(data.allRatings || []);
    this.setAuthorListInService(data.authors);
    this.removeCurrentSolution(data.relatedSolutions || [], data.solution);
  }

  triggerDeployment(menuEntry: {
    title: string;
    icon_url: string;
    local_service_path: string;
    produces_download: boolean;
  }) {
    this.jwtTokenService
      .getToken()
      .pipe(
        switchMap((token) => {
          if (!token) {
            return of(null);
          }
          const url = `${apiConfig.apiBackendURL}${menuEntry.local_service_path}${this.solutionId}/${this.revisionId}?jwtToken=${token}`;
          if (menuEntry.produces_download) {
            window.location.href = url;
          } else {
            window.open(url, '_blank');
          }
          return of(null);
        }),
      )
      .subscribe();
  }

  getImagePath(title: string): string {
    return title === 'Local Kubernetes'
      ? '../../../assets/images/kubernetes-local.svg'
      : '../../../assets/images/kinrw-pg.svg';
  }

  updateRating() {
    combineLatest([
      this.publicSolutionsService.getAverageRatings(this.solutionId),
      this.publicSolutionsService.getAllRatings(this.solutionId),
    ]).subscribe(
      ([averageRatings, allRatings]) => {
        this.averageRatings = averageRatings;
        this.allRatings = allRatings;
        this.totalRatingsCount = allRatings.length;

        this.updateStarWidth(averageRatings);
        this.calculateRatings(allRatings);
      },
      (error) => {
        console.error('Failed to fetch ratings: ', error);
      },
    );
  }

  createComment(event: {
    comment: string;
    revisionId: string;
    userId: string;
    solutionId: string;
  }) {
    this.privateCatalogsService
      .createThread(event.solutionId, event.revisionId)
      .subscribe((res1) => {
        const commentObj: CommentModel = {
          text: event.comment,
          threadId: res1.threadId,
          url: event.solutionId,
          userId: event.userId,
        };
        this.privateCatalogsService
          .createComment(commentObj)
          .subscribe((res2) => {
            this.getComments(event.solutionId, event.revisionId);
          });
      });
  }
  createOrReplyToComment(event: {
    comment: CommentModel;

    revisionId: string;
  }) {
    this.privateCatalogsService
      .createComment(event.comment)
      .subscribe((res2) => {
        this.getComments(event.comment.url, event.revisionId);
      });
  }
  editComment(event: { solutionId: string; comment: CommentModel }) {
    this.privateCatalogsService
      .updateComment(event.comment, event.solutionId)
      .subscribe((res) => {
        this.getComments(this.solutionId, this.revisionId);
      });
  }
  deleteComment(comment: CommentModel) {
    this.privateCatalogsService
      .deleteComment(comment.threadId, comment.commentId ?? '')
      .subscribe((res) => {
        this.getComments(this.solutionId, this.revisionId);
      });
  }
  getComments(solutionId: string, revisionId: string) {
    this.privateCatalogsService
      .getComments(solutionId, revisionId)
      .subscribe((res) => {
        this.comments = this.transformComments(res);
      });
  }

  transformComments(comments: CommentModel[]): CommentReplyModel[] {
    const commentMap: { [key: string]: CommentReplyModel } = {};

    // First, map all comments by their IDs to facilitate lookup, assuming commentId is defined.
    comments.forEach((comment) => {
      if (comment.commentId) {
        // Ensure commentId is not undefined
        commentMap[comment.commentId] = {
          ...comment,
          replies: [],
        } as CommentReplyModel;
      }
    });

    // associate each comment with its parent.
    const rootComments: CommentReplyModel[] = [];
    comments.forEach((comment) => {
      if (
        comment.parentId &&
        commentMap[comment.parentId] &&
        comment.commentId
      ) {
        // Ensure both parentId and commentId are not undefined and parent exists in map
        commentMap[comment.parentId].replies.push(
          commentMap[comment.commentId],
        );
      } else if (!comment.parentId && comment.commentId) {
        // Only push root comments (those without a parentId)
        rootComments.push(commentMap[comment.commentId]);
      }
    });

    return rootComments;
  }

  disableEdit(userId: string, solution: PublicSolutionDetailsModel) {
    // Allow editing if the solution is not active.
    if (!solution.active) {
      this.editModel = true;
      return; // No further checks needed if the solution is inactive.
    }

    // If the solution is active and the user is the owner, allow editing.
    if (solution.ownerId === userId) {
      this.editModel = true;
      return;
    }

    // If there are other owners and the user is one of them, allow editing.
    const isCoOwner = solution.ownerListForSol?.some(
      (owner) => owner.userId === userId,
    );
    if (isCoOwner) {
      this.editModel = true;
    }
  }

  OnlClickManageMyModel() {
    this.router.navigate([
      '/dashboard/manageMyModel/',
      this.solutionId,
      this.revisionId,
    ]);
  }
}
