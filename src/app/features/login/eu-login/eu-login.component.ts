import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'gp-eu-login',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './eu-login.component.html',
  styleUrl: './eu-login.component.scss'
})
export class EuLoginComponent {

}
