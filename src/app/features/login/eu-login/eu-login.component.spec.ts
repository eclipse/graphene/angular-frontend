import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EuLoginComponent } from './eu-login.component';

describe('EuLoginComponent', () => {
  let component: EuLoginComponent;
  let fixture: ComponentFixture<EuLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EuLoginComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EuLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
