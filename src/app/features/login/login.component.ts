import { Input, Component, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalLoginComponent } from './local-login/local-login.component';

@Component({
  selector: 'gp-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    LocalLoginComponent
  ],
})
export class LoginComponent {
  

}
