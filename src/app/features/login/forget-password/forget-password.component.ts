import { Component, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { LocalLoginService } from 'src/app/core/services/auth/local-login.service';
import { MatIconModule } from '@angular/material/icon';
import { AlertService } from 'src/app/core/services/alert.service';
import { EmailIDValidationDirective } from 'src/app/shared/directives/email-id-validation.directive';
import { Alert, AlertType } from 'src/app/shared/models/alert.model';

@Component({
  selector: 'gp-forget-password',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    EmailIDValidationDirective,
  ],
  templateUrl: './forget-password.component.html',
  styleUrl: './forget-password.component.scss',
})
export class ForgetPasswordComponent {
  constructor(
    private localLoginService: LocalLoginService,
    private dialogRef: MatDialogRef<ForgetPasswordComponent>,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
  ) {}

  form: FormGroup = this.formBuilder.group({
    email: [null, Validators.required],
  });

  get controls() {
    return this.form.controls;
  }

  isSubmitted = false;
  isEmailComplete = false;

  submit() {
    this.isSubmitted = true;

    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
    }
    this.localLoginService
      .requestPasswordReset(this.form.value.email)
      .pipe()
      .subscribe((result) => {
        const alert: Alert = {
          message: '',
          type: AlertType.Success,
        };

        if (result.error_code === '100') {
          alert.message = 'Temporary password has been sent to your email id';
        } else if (result.error_code === '500') {
          alert.message = result.response_detail;
          alert.type = AlertType.Warning;
        } else {
          alert.message = 'Email id not registered';
          alert.type = AlertType.Warning;
        }

        this.alertService.notify(alert, 3000);
        this.dialogRef.close();
      });
  }
  onEmailInput() {
    this.form.get('email')?.valueChanges.subscribe((value) => {
      this.isEmailComplete = this.form.get('email')?.valid || false;
    });
  }
  @Output() submitEM = new EventEmitter();
}
