import { CommonModule } from '@angular/common';
import { Component, OnInit, OnDestroy, Optional } from '@angular/core';

import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { LocalLoginService } from 'src/app/core/services/auth/local-login.service';
import { Router } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {
  MatCheckboxChange,
  MatCheckboxModule,
} from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { Subject, switchMap, takeUntil, tap } from 'rxjs';
import { ForgetPasswordComponent } from '../forget-password/forget-password.component';
import { SignupComponent } from '../../signup/signup.component';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { JwtTokenService } from 'src/app/core/services/auth/jwt-token.service';

@Component({
  selector: 'gp-local-login',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    MatDividerModule,
  ],
  templateUrl: './local-login.component.html',
  styleUrl: './local-login.component.scss',
})
export class LocalLoginComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject<void>();
  isSubmitted = false;
  enableSubmit = false;
  isLoading = false;
  form: FormGroup;
  hidePassword = true;

  constructor(
    private authService: AuthService,
    private localLoginService: LocalLoginService,
    @Optional() private dialogRef: MatDialogRef<LocalLoginComponent>,
    private formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    private browserStorageService: BrowserStorageService,
    private jwtTokenService: JwtTokenService,
  ) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: [false],
    });
  }
  ngOnInit(): void {
    this.form.valueChanges.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.checkFormValidity();
    });
  }

  checkFormValidity() {
    const { username, password } = this.form.value;
    this.enableSubmit = username && password;
  }

  get controls() {
    return this.form.controls;
  }

  submit(): void {
    this.isSubmitted = true;
    this.isLoading = true;
    if (this.form.valid) {
      const { username, password, remember } = this.form.value;
      this.localLoginService
        .login(username, password)
        .pipe(
          tap((result) => {
            this.jwtTokenService.setToken(result.authToken);
          }),
          switchMap(() => this.jwtTokenService.getUserDetailsObservable()),
          takeUntil(this.onDestroy),
        )
        .subscribe({
          next: (userDetails) => {
            this.isLoading = false;
            this.browserStorageService.setUserDetail({
              userId: userDetails?.userId,
              firstName: userDetails?.firstName || '',
              username: userDetails?.username || '',
              emailId: userDetails?.emailId || '',
              lastName: userDetails?.lastName || '',
            });
            sessionStorage.setItem('rm', remember ? 'true' : 'false');
            this.router.navigate(['/dashboard/home']);
            if (this.dialogRef) this.dialogRef.close();
          },
          error: (error) => {
            this.isLoading = false;
            console.error('Login failed:', error);
          },
        });
    }
  }

  logout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  OnRememberMe(event: MatCheckboxChange) {
    event.checked
      ? sessionStorage.setItem('rm', 'true')
      : sessionStorage.setItem('rm', 'false');
  }

  OnForgetPassWord() {
    this.dialogRef.close();
    const dialogRefForgetPassword: MatDialogRef<ForgetPasswordComponent> =
      this.dialog.open(ForgetPasswordComponent);

    dialogRefForgetPassword.afterClosed().subscribe((result) => {});
  }

  isForgetPasswordForm() {
    return this.dialogRef.componentInstance instanceof ForgetPasswordComponent;
  }

  openSignUpDialog() {
    this.dialogRef.close();

    const dialogRef: MatDialogRef<SignupComponent> =
      this.dialog.open(SignupComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }
}
