import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
  PageEvent,
} from '@angular/material/paginator';

import { MatSidenavModule } from '@angular/material/sidenav';
import { Router, RouterModule } from '@angular/router';
import { FiltersComponent } from 'src/app/shared/components/filters/filters.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { CardItemComponent } from 'src/app/shared/components/card-item/card-item.component';
import { CustomPaginator } from 'src/app/core/config/custom-paginator-configuration';
import { PublicSolutionsService } from 'src/app/core/services/public-solutions.service';
import {
  Catalog,
  CatalogFilter,
  CategoryCode,
  FieldToDirectionMap,
  Filter,
  PublicSolution,
  PublicSolutionsRequestPayload,
  SortOption,
  Tag,
  categoryCodeMap,
} from 'src/app/shared/models';
import { environment } from 'src/environments/environment';
import { SortByComponent } from 'src/app/shared/components/sort-by/sort-by.component';
import { FiltersService } from 'src/app/core/services/filters.service';
import { SolutionsToggleViewComponent } from 'src/app/shared/components/card-list-view/solutions-toggle-view.component';
import { ListItemComponent } from 'src/app/shared/components/list-item/list-item.component';
import { BrowserStorageService } from 'src/app/core/services/storage/browser-storage.service';
import { Observable, Subscription, map } from 'rxjs';
import { PrivateCatalogsService } from 'src/app/core/services/private-catalogs.service';
import { MatButtonModule } from '@angular/material/button';
import { HeadlineComponent } from 'src/app/shared/components/headline/headline.component';
import { BreadcrumbNavigationComponent } from 'src/app/shared/components/breadcrumb-navigation/breadcrumb-navigation.component';

@Component({
  selector: 'gp-marketplace',
  standalone: true,
  imports: [
    CommonModule,
    MatSidenavModule,
    RouterModule,
    FiltersComponent,
    CardItemComponent,
    MatPaginatorModule,
    MatGridListModule,
    SortByComponent,
    SolutionsToggleViewComponent,
    ListItemComponent,
    MatButtonModule,
    HeadlineComponent,
    BreadcrumbNavigationComponent,
  ],
  templateUrl: './marketplace.component.html',
  styleUrl: './marketplace.component.scss',
  providers: [{ provide: MatPaginatorIntl, useValue: CustomPaginator() }],
})
export class MarketplaceComponent {
  userId$: Observable<string | undefined>;
  private destroy$: Subscription = new Subscription();
  loginUserId!: string;
  publicSolutions!: PublicSolution[];
  favoriteSolutions!: PublicSolution[];
  pageSizeOptions = [25, 50, 100];
  pageSize = 25;
  pageIndex = 0;
  totalItems = 0;
  tags: Filter[] = [];
  categories: Filter[] = [];
  solutionPictureUrl = '';
  catalogIds: string[] = [];
  selectedTags: string[] = [];
  selectedCategories: string[] = [];
  searchKeyWord: string[] = [];
  sortByOptions: SortOption[] = [
    { name: 'Most Liked', value: 'ML' },
    { name: 'Fewest Liked', value: 'FL' },
    { name: 'Most Downloaded', value: 'MD' },
    { name: 'Fewest Downloaded', value: 'FD' },
    { name: 'Highest Reach', value: 'HR' },
    { name: 'Lowest Reach', value: 'LR' },
    { name: 'Most Recent', value: 'MR' },
    { name: 'Older', value: 'OLD' },
    { name: 'Name', value: 'name' },
    { name: 'Created Date', value: 'created' },
  ];
  sortByFilter: string = '';
  fieldToDirectionMap: FieldToDirectionMap = { viewCount: 'DESC' };
  viewTile: boolean = true;

  private isFirstLoad = true;
  favoriteSolutionsMap: { [key: string]: boolean } = {};
  isLoading: boolean = false;
  isOnMyFavoriteSelected = false;

  constructor(
    private publicSolutionsService: PublicSolutionsService,
    private privateCatalogsService: PrivateCatalogsService,
    private filtersService: FiltersService,
    private router: Router,
    private browserStorageService: BrowserStorageService,
  ) {
    this.userId$ = this.browserStorageService
      .getUserDetails()
      .pipe(map((details) => details?.userId));
  }

  loadPublicSolutions() {
    this.isLoading = true;
    try {
      const response1 = this.publicSolutionsService
        .getPublicCatalogs()
        .subscribe((catalogs) => {
          this.catalogIds = catalogs?.map(
            (catalog: Catalog) => catalog.catalogId ?? '',
          );

          const requestPayload: PublicSolutionsRequestPayload = {
            catalogIds: this.catalogIds,
            modelTypeCodes: this.selectedCategories,
            tags: this.selectedTags,
            nameKeyword: this.searchKeyWord,
            sortBy: this.sortByFilter,
            pageRequest: {
              fieldToDirectionMap: this.fieldToDirectionMap,
              page: this.pageIndex,
              size: this.pageSize,
            },
          };

          this.publicSolutionsService
            .getPublicSolutionsOfPublicCatalogs(requestPayload)
            .subscribe((publicSolutions) => {
              this.publicSolutions = publicSolutions.publicSolutions ?? [];
              this.totalItems = publicSolutions?.totalElements ?? 0;
              if (this.isFirstLoad)
                this.tags = this.extractTagsFromPublicSolutions(
                  this.publicSolutions,
                );
              this.isLoading = false;
            });
        });
    } catch (error) {
      // Handle errors
      console.error('Error:', error);
      this.isLoading = false;
    }
  }

  updateFavorite(solutionId: string): void {
    const dataObj = {
      request_body: {
        solutionId: solutionId,
        userId: this.loginUserId,
      },
      request_from: 'string',
      request_id: 'string',
    };

    if (!this.favoriteSolutionsMap[solutionId]) {
      this.privateCatalogsService.createFavorite(dataObj).subscribe(
        (response) => {
          this.favoriteSolutionsMap[solutionId] =
            !this.favoriteSolutionsMap[solutionId];

          // Handle response if needed
        },
        (error) => {
          // Handle error if needed
        },
      );
    } else {
      this.privateCatalogsService.deleteFavorite(dataObj).subscribe(
        (response) => {
          // Handle response if needed
          this.favoriteSolutionsMap[solutionId] =
            !this.favoriteSolutionsMap[solutionId];
        },
        (error) => {
          // Handle error if needed
        },
      );
    }
  }

  ngOnInit(): void {
    this.destroy$.add(
      this.browserStorageService.getUserDetails().subscribe((details) => {
        this.loginUserId = details?.userId ?? '';
      }),
    );
    this.sortByFilter = environment.ui_system_config.marketplace_sort_by;
    this.loadPublicSolutions();
    this.subscribeToUserId();
    this.filtersService.getModelTypes().subscribe(
      (res: CatalogFilter[]) => {
        this.categories = res.map((catalog: CatalogFilter) => ({
          name: catalog.name,
          selected: false,
        }));
      },
      (err: any) => {},
    );
  }

  onPageChange(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.loadPublicSolutions();
  }

  calculateStartIndex(): number {
    return this.pageIndex * this.pageSize + 1;
  }

  calculateEndIndex(): number {
    const endIndex = (this.pageIndex + 1) * this.pageSize;
    return endIndex > this.totalItems ? this.totalItems : endIndex;
  }

  private extractTagsFromPublicSolutions(
    solutions: PublicSolution[],
  ): Filter[] {
    let tags: Filter[] = [];

    solutions.forEach((solution) => {
      if (solution.solutionTagList && solution.solutionTagList.length > 0) {
        solution.solutionTagList.forEach((tagObj: Tag) => {
          if (tagObj.tag) {
            const tag: Filter = {
              name: tagObj.tag.toLowerCase(),
              selected: false,
            };
            tags.push(tag);
          }
        });
      }
    });
    // Remove duplicates by name
    const uniqueTags: Filter[] = Array.from(
      new Map(tags.map((tag) => [tag.name, tag])).values(),
    );

    // Sort tags alphabetically by name
    uniqueTags.sort((a, b) => a.name.localeCompare(b.name));
    return uniqueTags;
  }

  handleSelectedTagsChange(selectedTags: string[]): void {
    this.isFirstLoad = false;
    this.selectedTags = selectedTags;
    this.loadPublicSolutions();
  }

  handleSelectedCategoriesChange(selectedCategories: string[]): void {
    this.isFirstLoad = false;
    // Convert selected categories to their codes
    const selectedCategoryCodes: CategoryCode[] =
      this.convertCategoriesToCodes(selectedCategories);

    // Update the selectedCategories property
    this.selectedCategories = selectedCategoryCodes;
    this.loadPublicSolutions();
  }

  convertCategoriesToCodes(selectedCategories: string[]): CategoryCode[] {
    return selectedCategories.map(
      (categoryName) => categoryCodeMap[categoryName] as CategoryCode,
    );
  }

  onSearchValueChanged(searchValue: string): void {
    this.isFirstLoad = false;
    this.searchKeyWord.push(searchValue);
    this.loadPublicSolutions();
  }

  getFieldToDirectionMap(sortBy: string): FieldToDirectionMap {
    switch (sortBy) {
      case 'FD':
        return { downloadCount: 'ASC' };
      case 'MD':
        return { downloadCount: 'DESC' };
      case 'FL':
        return { ratingAverageTenths: 'ASC' };
      case 'ML':
        return { ratingAverageTenths: 'DESC' };
      case 'HR':
        return { viewCount: 'DESC' };
      case 'LR':
        return { viewCount: 'ASC' };
      case 'MR':
        return { modified: 'DESC' };
      case 'OLD':
        return { modified: 'ASC' };
      case 'name':
        return { name: 'ASC' };
      case 'created':
        return { created: 'DESC' };
      default:
        throw new Error(`Invalid sortBy value: ${sortBy}`);
    }
  }

  updateFieldToDirectionMap(sortByFilter: string): void {
    this.fieldToDirectionMap = this.getFieldToDirectionMap(sortByFilter);
    this.isFirstLoad = false;
    this.loadPublicSolutions();
  }

  onViewTileChange(value: boolean): void {
    this.viewTile = value;
  }

  onHomeClick() {
    this.userId$.subscribe((userId) => {
      if (userId) {
        this.router.navigate(['/dashboard/home']);
      } else {
        this.router.navigate(['/home']);
      }
    });
  }

  loadFavoriteCatalogs(userId: string) {
    this.privateCatalogsService.getFavoriteSolutions(userId).subscribe(
      (solutions) => {
        this.favoriteSolutions = solutions;
        this.favoriteSolutionsMap = {};
        solutions.forEach((solution) => {
          this.favoriteSolutionsMap[solution.solutionId] = true;
        });
        // Handle your solutions here
      },
      (error) => console.error('Error loading favorite solutions:', error),
    );
  }

  subscribeToUserId() {
    this.userId$.subscribe((userId) => {
      if (userId) {
        this.loadFavoriteCatalogs(userId);
      }
    });
  }

  isFavoriteSolution(solutionId: string): boolean {
    return !!this.favoriteSolutionsMap[solutionId];
  }

  onAllCatalogsClick() {
    this.isOnMyFavoriteSelected = false;
    this.loadPublicSolutions();
  }

  onMyFavoriteCatalogsClick() {
    this.isOnMyFavoriteSelected = true;
    this.isLoading = true;
    this.privateCatalogsService
      .getFavoriteSolutions(this.loginUserId)
      .subscribe(
        (solutions) => {
          this.favoriteSolutions = solutions;
          this.publicSolutions = solutions;
          // Update the favorite solutions map for quick lookup
          this.favoriteSolutionsMap = {};
          solutions.forEach((solution) => {
            this.favoriteSolutionsMap[solution.solutionId] = true;
          });
          // Handle your solutions here
          this.isLoading = false;
        },
        (error) => console.error('Error loading favorite solutions:', error),
      );
  }
}
