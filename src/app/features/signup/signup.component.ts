import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';

import { LocalLoginService } from 'src/app/core/services/auth/local-login.service';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { Subject, combineLatest, takeUntil } from 'rxjs';
import { PasswordStrengthComponent } from './password-strength/password-strength.component';
import { AlertService } from 'src/app/core/services/alert.service';
import { Alert, AlertType } from 'src/app/shared/models/alert.model';
import { NameValidationDirective } from 'src/app/shared/directives/name-validation.directive';
import { PasswordValidationDirective } from 'src/app/shared/directives/password-validation.directive';
import { MatchingPasswordDirective } from 'src/app/shared/directives/matching-password.directive';
import { EmailIDValidationDirective } from 'src/app/shared/directives/email-id-validation.directive';
import { UserDetails } from 'src/app/shared/models';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'gp-signup',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    MatDividerModule,
    PasswordStrengthComponent,
    NameValidationDirective,
    PasswordValidationDirective,
    MatchingPasswordDirective,
    EmailIDValidationDirective,
    MatchingPasswordDirective,
  ],
  templateUrl: './signup.component.html',
  styleUrl: './signup.component.scss',
})
export class SignupComponent {
  constructor(
    private alertService: AlertService,
    private localLoginService: LocalLoginService,
    private dialogRef: MatDialogRef<SignupComponent>,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
  ) {
    combineLatest([
      this.controls['firstName'].valueChanges,
      this.controls['lastName'].valueChanges,
      this.controls['username'].valueChanges,
      this.controls['emailId'].valueChanges,
      this.controls['password'].valueChanges,
      this.controls['confirmPassword'].valueChanges,
    ]).subscribe(
      ([firstName, lastName, username, emailId, password, confirmPassword]) => {
        this.enableSubmit =
          firstName &&
          lastName &&
          emailId &&
          username &&
          password &&
          confirmPassword;
      },
    );

    this.controls['password'].valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        // Force the validation to update
        this.controls['confirmPassword'].updateValueAndValidity();
      });
  }

  isSubmitted = false;
  hidePassword = true;
  hideConfirmPassword = true;
  enableSubmit = false;
  isLoading = false;
  passwordStrength = 0;
  passwordIsValid = false;

  form: FormGroup = this.formBuilder.group({
    firstName: [undefined, Validators.required],
    lastName: [undefined, Validators.required],
    username: [undefined, Validators.required],
    emailId: [undefined, [Validators.required]],
    password: ['', Validators.required],
    confirmPassword: ['', [Validators.required]],
  });

  get controls() {
    return this.form.controls;
  }

  onDestroy = new Subject<void>();

  submit() {
    this.isSubmitted = true;
    this.isLoading = true;
    const userDetails: UserDetails = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      username: this.form.value.username,
      emailId: this.form.value.emailId,
      password: this.form.value.password,
      active: true,
    };

    if (this.form.valid) {
      this.localLoginService
        .register(userDetails)
        .pipe()
        .subscribe(
          (result) => {
            this.isLoading = false;
            const alert: Alert = {
              message: '',
              type: AlertType.Success,
            };

            if (result.error_code === '100') {
              alert.message =
                'Signed-Up successfully\nPlease Verify Your Account to log into the portal.';
              this.dialogRef.close();
            } else if (result.error_code === '202') {
              alert.message = 'Username Already Exists';
              alert.type = AlertType.Error;
            } else if (result.error_code === '203') {
              alert.message = 'Email Already Exists';
              alert.type = AlertType.Error;
            }
            this.alertService.notify(alert, 5000);
          },
          (error) => {
            // Handle login error
            this.isLoading = false;
          },
        );
    }
  }

  ngOnDestroy() {
    this.onDestroy.next();
  }

  passwordValid(event: boolean) {
    this.passwordIsValid = event;
  }

  openSignInDialog() {
    this.dialogRef.close();

    const dialogRef: MatDialogRef<LoginComponent> =
      this.dialog.open(LoginComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }
}
