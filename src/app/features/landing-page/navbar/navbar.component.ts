import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LocalLoginComponent } from '../../login/local-login/local-login.component';
import { MatDividerModule } from '@angular/material/divider';
import { SignupComponent } from '../../signup/signup.component';
import { ExpandedSearchComponent } from 'src/app/shared/components/expanded-search/expanded-search.component';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'gp-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatDividerModule,
    ExpandedSearchComponent,
    FormsModule,
  ],
})
export class NavbarComponent {
  constructor(
    public dialog: MatDialog,
    private router: Router,
  ) {}

  openDialog() {
    const dialogRef: MatDialogRef<LocalLoginComponent> =
      this.dialog.open(LocalLoginComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }

  openSignUpDialog() {
    const dialogRef: MatDialogRef<SignupComponent> =
      this.dialog.open(SignupComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }

  openExternalLink() {
    window.open(
      'https://gitlab.eclipse.org/eclipse/graphene/tutorials',
      '_blank',
    );
  }

  navbarSearchTerm: string = '';

  toggleSearch() {}

  onHomeClick() {
    this.router.navigate(['/home']);
  }

  onMarketPlaceClick() {
    this.router.navigate(['/marketplace']);
  }
}
