import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FooterComponent } from '../../shared/components/footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from 'src/app/shared/components/home/home.component';
import { MarketplaceComponent } from '../marketplace/marketplace.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'gp-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrl: './landing-page.component.scss',
  standalone: true,
  imports: [
    CommonModule,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    MarketplaceComponent,
    RouterModule,
  ],
})
export class LandingPageComponent {}
