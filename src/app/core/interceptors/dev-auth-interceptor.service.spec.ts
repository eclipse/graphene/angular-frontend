import { TestBed } from '@angular/core/testing';

import { DevAuthInterceptor } from './dev-auth-interceptor.service';

describe('HttpInterceptorService', () => {
  let service: DevAuthInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DevAuthInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
