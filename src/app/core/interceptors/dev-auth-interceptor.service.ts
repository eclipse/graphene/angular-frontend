import { inject } from '@angular/core';
import { HttpHandlerFn, HttpInterceptorFn, HttpRequest } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { JwtTokenService } from '../services/auth/jwt-token.service';

export const devAuthInterceptor: HttpInterceptorFn = (
  request: HttpRequest<unknown>,
  next: HttpHandlerFn,
) => {
  const jwtTokenService = inject(JwtTokenService);

  if (environment.production) {
    return next(request);
  }

  return jwtTokenService.getToken().pipe(
    switchMap((token) => {
      if (token) {
        // Ensure token is a non-null string
        const authReq = request.clone({
          headers: request.headers.set('Authorization', `Bearer ${token}`),
        });
        return next(authReq);
      }
      // If no token, forward the original request
      return next(request.clone());
    }),
  );
};
