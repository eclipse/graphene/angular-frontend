import { environment } from 'src/environments/environment';

export const apiConfig = {
  apiBackendURL: environment.apiBackendURL,
  // eslint-disable-next-line max-len
  authorization: '/api/auth/jwtToken',
  publicCatalogs: '/api/catalogs/public',
  forgetPasswordURL: '/api/users/forgetPassword',
  signUpURL: '/api/users/register',
  urlSolutions: '/api/solutions',
  urlPublicPortalSolutions: '/api/portal/solutions/public',
  urlPortalSolutions: '/api/portal/solutions',
  urlPublicCatalogs: '/api/catalogs/public',
  getPublicSolutionPicture: (publicSolutionId: string) =>
    `/api/solutions/${publicSolutionId}/picture`,
  urlGetSolutionDetails: (solutionId: string, revisionId: string) =>
    `/api/solutions/${solutionId}/${revisionId}`,
  urlGetCatalogsOfSolution: (solutionId: string) =>
    `/api/catalog/solution/${solutionId}`,
  urlGetModelAuthors: (solutionId: string, revisionId: string) =>
    `/api/solution/${solutionId}/revision/${revisionId}/authors`,
  urlGetComment: (solutionId: string, revisionId: string) =>
    `/api/thread/${solutionId}/${revisionId}/comments`,
  urlGetArtifacts: (solutionId: string, revisionId: string) =>
    `/api/solutions/${solutionId}/revisions/${revisionId}`,
  urlGetModelSignature: (solutionId: string, versionId: string) =>
    `/api/getProtoFile?solutionId=${solutionId}&version=${versionId}`,
  urlGetSolutionDocuments: (
    solutionId: string,
    revisionId: string,
    selectedCatalogId: string,
  ) =>
    `/api/solution/${solutionId}/revision/${revisionId}/${selectedCatalogId}/document`,
  urlGetLicenseFile: (solutionId: string, versionId: string) =>
    `/api/getLicenseFile?solutionId=${solutionId}&version=${versionId}`,
  urlGetSolutionDescription: (revisionId: string, selectedCatalogId: string) =>
    `/api/solution/revision/${revisionId}/${selectedCatalogId}/description`,
  urlDescription: '/api/solution/revision',
  urlGetSolutionRatings: '/api/solutions/ratings/',
  urlPreferredTag: '/api/preferredTags',
  urlCreateFavorite: '/api/solution/createFavorite',
  urlDeleteFavorite: '/api/solution/deleteFavorite',
  urlPrivateCatalogsList: '/api/catalogs',
  urlModelTypes: '/api/filter/modeltype',
  urlToolkitTypes: '/api/filter/toolkitType',
  urlSearchSolutions: '/api/searchSolutionBykeyword',
  urlFavoriteSolution: '/api/solution/getFavoriteSolutions',
  urlUserAccountDetails: '/api/users/userAccountDetails',
  getRelatedMySolutions: '/api/getRelatedMySolutions',
  urlUpdateViewCount: '/api/solution/updateViewCount',
  urlUploadLicenseProfile: '/api/license/upload',
  urlAvgRating: '/api/solution/avgRating/',
  urlGetRating: '/api/solution/getRating/',
  urlCreateRating: '/api/solution/createRating',
  urlThread: '/api/thread',
  urlComment: '/api/comments',
  urlGetActiveUsers: '/api/users/activeUserDetails',
  urlShareWithTeam: '/api/solution/userAccess',
  urlAddToCatalog: '/api/webBasedOnBoarding/addToCatalog',
  urlMessagingStatus: '/api/webBasedOnBoarding/messagingStatus',
  urlUserSolutions: '/api/user/solutions',
  urlSearchSolutionsByName: '/api/onboardingDocker/dockerSearchSolutions',
  urlAllCatalogsList: '/api/catalogs',
  urlCreateTags: '/api/tags/create',
  urlDeleteTag: '/api/dropTag',
  urlAddTag: '/api/addTag',
  urlGetAllTag: '/api/tags',
  urlPublishSolution: '/api/publish',
  urlSearchPublishRequest: '/api/publish/request/search/revision',
  withdrawPublishRequestUrl: '/api/publish/request/withdraw/',
  urlCatalogPath: '/api/catalog',
  urlSiteConfig: '/api/admin/config',
};
