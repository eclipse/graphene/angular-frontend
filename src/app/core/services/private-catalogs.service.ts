import { Injectable } from '@angular/core';
import { apiConfig } from '../config';
import { Observable, catchError, map, tap } from 'rxjs';
import { HttpSharedService } from '../http-shared/http-shared.service';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import {
  AuthorPublisherModel,
  Catalog,
  CommentModel,
  DocumentModel,
  PublicSolution,
  PublicSolutionDetailsModel,
  PublicSolutionsRequestPayload,
  PublishSolutionRequest,
  ThreadModel,
  UserDetails,
} from 'src/app/shared/models';
import {
  CreateRatingRequestPayload,
  UpdateSolutionRequestPayload,
} from 'src/app/shared/models/request-payloads';
import { MessageStatusModel } from 'src/app/shared/models/message-status.model';

@Injectable({
  providedIn: 'root',
})
export class PrivateCatalogsService {
  constructor(
    private readonly _httpSharedService: HttpSharedService,
    private http: HttpClient,
  ) {}

  getPrivateCatalogs(pageNumber: number, requestResultSize: number) {
    const url = apiConfig.apiBackendURL + apiConfig.urlPrivateCatalogsList;

    const body = {
      request_body: {
        fieldToDirectionMap: {
          created: 'DESC',
        },
        page: pageNumber,
        size: requestResultSize,
      },
      request_from: 'string',
      request_id: 'string',
    };

    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getFavoriteSolutions(userId: string): Observable<PublicSolution[]> {
    const url =
      apiConfig.apiBackendURL + apiConfig.urlFavoriteSolution + '/' + userId;

    return this._httpSharedService.get(url, undefined).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getPreferredTag(userId: any) {
    let body = {
      request_body: {
        fieldToDirectionMap: {},
        page: 9,
        size: 0,
      },
      request_from: 'string',
      request_id: 'string',
    };
    const urlPreferredTag = apiConfig.apiBackendURL + apiConfig.urlPreferredTag;

    return this._httpSharedService
      .put(`${urlPreferredTag}/${userId}`, undefined, body)
      .pipe(
        tap(),
        catchError((error) => {
          throw error;
        }),
      );
  }

  getSolutionRatings(solutionId: string, userId: string) {
    let body = {
      request_body: {
        fieldToDirectionMap: {},
        page: 0,
        size: 20,
      },
      request_from: 'string',
      request_id: 'string',
    };
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetSolutionRatings +
      solutionId +
      '/user/' +
      userId;

    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getUserAccountDetails(userId: string) {
    const url = apiConfig.apiBackendURL + apiConfig.urlUserAccountDetails;

    const body = {
      request_body: {
        userId,
      },
    };

    return this.http
      .post(url, body, {
        responseType: 'json',
      })
      .pipe(
        tap(),
        catchError((error) => {
          throw error;
        }),
      );
  }

  uploadExistingModelLicenseProfile(
    loginUserID: string,
    solutionId: string,
    revisionId: string,
    versionId: string,
    file: File,
  ): Observable<HttpEvent<any>> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlUploadLicenseProfile +
      '/' +
      loginUserID +
      '/' +
      solutionId +
      '/' +
      revisionId +
      '/' +
      versionId;

    const formData = new FormData();
    formData.append('file', file);

    const req = new HttpRequest('POST', url, formData, {
      reportProgress: true,
      responseType: 'json',
    });

    return this.http.request(req);
  }

  uploadNewModelLicenseProfile(userId: string, file: File) {
    const url =
      apiConfig.apiBackendURL +
      '/api/model/upload/' +
      userId +
      '/?licUploadFlag=' +
      true;
    const formData = new FormData();
    formData.append('file', file);

    const req = new HttpRequest('POST', url, formData, {
      reportProgress: true,
      responseType: 'json',
    });

    return this.http.request(req);
  }

  createUpdateLicenseProfile(
    loginUserID: string,
    solutionId: string,
    revisionId: string,
    versionId: string,
    licenseProfile: any,
  ): Observable<HttpEvent<any>> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlUploadLicenseProfile +
      '/' +
      loginUserID +
      '/' +
      solutionId +
      '/' +
      revisionId +
      '/' +
      versionId;

    const blob = new Blob([JSON.stringify(licenseProfile)]);

    const formData = new FormData();
    formData.append('file', new File([blob], 'file.json'));

    const req = new HttpRequest('POST', url, formData, {
      reportProgress: true,
      responseType: 'json',
    });

    return this.http.request(req);
  }

  createFavorite(dataObj: any) {
    const urlCreateFavorite =
      apiConfig.apiBackendURL + apiConfig.urlCreateFavorite;

    return this._httpSharedService
      .post(urlCreateFavorite, undefined, dataObj)
      .pipe(
        tap(),
        catchError((error) => {
          throw error;
        }),
      );
  }

  deleteFavorite(dataObj: any) {
    const urlDeleteFavorite =
      apiConfig.apiBackendURL + apiConfig.urlDeleteFavorite;

    return this._httpSharedService
      .post(urlDeleteFavorite, undefined, dataObj)
      .pipe(
        tap(),
        catchError((error) => {
          throw error;
        }),
      );
  }

  createRatingSolution(request_body: CreateRatingRequestPayload) {
    const urlCreateFavorite =
      apiConfig.apiBackendURL + apiConfig.urlCreateRating;

    const dataObj = {
      request_body,
      request_from: 'string',
      request_id: 'string',
    };

    return this._httpSharedService
      .post(urlCreateFavorite, undefined, dataObj)
      .pipe(
        tap(),
        catchError((error) => {
          throw error;
        }),
      );
  }

  performSVScan(
    solutionId: string,
    revisionId: string,
    workflowId: string,
  ): Observable<any> {
    const url = `${apiConfig.apiBackendURL}${apiConfig.urlSolutions}/${solutionId}/revisions/${revisionId}/verify/${workflowId}`;
    return this.http.get<any>(url);
  }

  createThread(
    solutionId: string,
    revisionId: string,
  ): Observable<ThreadModel> {
    const url = `${apiConfig.apiBackendURL}${apiConfig.urlThread}/create`;
    const threadObj = {
      request_body: {
        revisionId,
        solutionId,
      },
    };
    return this._httpSharedService.post(url, undefined, threadObj).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  createComment(comment: CommentModel): Observable<CommentModel> {
    const url = `${apiConfig.apiBackendURL}${apiConfig.urlComment}/create`;
    const commentObj = {
      request_body: {
        text: comment.text,
        threadId: comment.threadId,
        url: comment.url,
        userId: comment.userId,
        parentId: comment.parentId,
      },
    };
    return this._httpSharedService.post(url, undefined, commentObj).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  deleteComment(threadId: string, commentId: string) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlComment +
      '/delete/' +
      threadId +
      '/' +
      commentId;

    return this._httpSharedService.post(url).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  updateComment(comment: CommentModel, solutionId: string) {
    const commentObj = {
      request_body: {
        text: comment.text,
        commentId: comment.commentId,
        threadId: comment.threadId,
        url: solutionId,
        userId: comment.userId,
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlComment + '/update';

    return this._httpSharedService.put(url, undefined, commentObj).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  getComments(
    solutionId: string,
    revisionId: string,
  ): Observable<CommentModel[]> {
    const reqObj = {
      request_body: {
        page: 0,
        size: 100,
      },
    };

    const url = `${apiConfig.apiBackendURL}${apiConfig.urlThread}/${solutionId}/${revisionId}/comments`;
    return this._httpSharedService.post(url, undefined, reqObj).pipe(
      map((res) => res.response_body.content),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getAllActiveUsers(activeStatus: boolean): Observable<UserDetails[]> {
    const url = `${apiConfig.apiBackendURL}${apiConfig.urlGetActiveUsers}/${activeStatus}`;
    return this._httpSharedService.get(url, undefined).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getShareWithTeam(solutionId: string): Observable<UserDetails[]> {
    const url = `${apiConfig.apiBackendURL}${apiConfig.urlShareWithTeam}/${solutionId}`;
    return this._httpSharedService.get(url, undefined).pipe(
      map((res) => res.response_body?.userList),
      catchError((error) => {
        throw error;
      }),
    );
  }

  insertMultipleShare(userIds: string[], solutionId: string) {
    const reqObj = {
      request_body: userIds,
    };
    const url = `${apiConfig.apiBackendURL}${apiConfig.urlShareWithTeam}/${solutionId}/add`;
    return this._httpSharedService.post(url, undefined, reqObj).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  deleteShareWithTeam(userId: string, solutionId: string) {
    const url = `${apiConfig.apiBackendURL}${apiConfig.urlShareWithTeam}/${solutionId}/delete/${userId}`;
    return this._httpSharedService.delete(url, undefined).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  getAuthors(
    solutionId: string,
    revisionId: string,
  ): Observable<AuthorPublisherModel[]> {
    const url = `${apiConfig.apiBackendURL}/api/solution/${solutionId}/revision/${revisionId}/authors`;
    return this._httpSharedService.get(url, undefined).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  addAuthor(
    solutionId: string,
    revisionId: string,
    author: AuthorPublisherModel,
  ): Observable<AuthorPublisherModel[]> {
    const url = `${apiConfig.apiBackendURL}/api/solution/${solutionId}/revision/${revisionId}/authors`;
    const obj = {
      request_body: author,
    };

    return this._httpSharedService.put(url, undefined, obj).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  removeAuthor(
    solutionId: string,
    revisionId: string,
    author: AuthorPublisherModel,
  ): Observable<AuthorPublisherModel[]> {
    const url = `${apiConfig.apiBackendURL}/api/solution/${solutionId}/revision/${revisionId}/removeAuthor`;
    const obj = {
      request_body: author,
    };

    return this._httpSharedService.put(url, undefined, obj).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getPublisher(solutionId: string, revisionId: string): Observable<string> {
    const url = `${apiConfig.apiBackendURL}/api/solution/${solutionId}/revision/${revisionId}/publisher`;
    return this._httpSharedService.get(url, undefined).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  updatePublisher(
    solutionId: string,
    revisionId: string,
    publisherName: string,
  ) {
    const url = `${apiConfig.apiBackendURL}/api/solution/${solutionId}/revision/${revisionId}/publisher`;
    const obj = publisherName;

    return this._httpSharedService.put(url, undefined, obj).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  addCatalog(
    userId: string,
    solutionName: string,
    dockerUrl: string,
  ): Observable<string> {
    const urlCreateFavorite =
      apiConfig.apiBackendURL + apiConfig.urlAddToCatalog + '/' + userId;

    const addToReqObj = {
      request_body: {
        name: solutionName,
        dockerfileURI: dockerUrl,
      },
    };

    return this._httpSharedService
      .post(urlCreateFavorite, undefined, addToReqObj)
      .pipe(
        map((res) => res.response_detail),
        catchError((error) => {
          throw error;
        }),
      );
  }
  uploadProtoBufFile(file: File) {
    const url =
      apiConfig.apiBackendURL + '/api/proto/upload/' + '?protoUploadFlag=true';

    const formData = new FormData();
    formData.append('file', file);

    const req = new HttpRequest('POST', url, formData, {
      reportProgress: true,
      responseType: 'json',
    });

    return this.http.request(req);
  }

  getMessagingStatus(
    userId: string,
    trackId: string,
  ): Observable<MessageStatusModel[]> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlMessagingStatus +
      '/' +
      userId +
      '/' +
      trackId;
    return this._httpSharedService.post(url, undefined).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  fetchPublishedAndUnpublishedSolutions(
    requestPayload: PublicSolutionsRequestPayload,
  ): Observable<{ publicSolutions: PublicSolution[]; totalElements: number }> {
    const url = apiConfig.apiBackendURL + apiConfig.urlUserSolutions;
    /**
     * active:true  => fetching active solutions
     * active:false => fetching deleted solutions
     */
    const dataObj = {
      request_body: {
        active: true,
        published: requestPayload.published,
        tags: requestPayload.tags,
        nameKeyword: requestPayload.nameKeyword,
        modelTypeCodes: requestPayload.modelTypeCodes,
        userId: requestPayload.userId,
        sortBy: requestPayload.sortBy,
        pageRequest: requestPayload.pageRequest,
      },
    };

    return this._httpSharedService.post(url, undefined, dataObj).pipe(
      map((res) => ({
        publicSolutions: res.response_body.content,
        totalElements: res.response_body.totalElements,
      })),
      catchError((error) => {
        throw error;
      }),
    );
  }

  searchSolutionsByName(
    modelName: string,
  ): Observable<{ publicSolutions: PublicSolution[]; totalElements: number }> {
    const url = apiConfig.apiBackendURL + apiConfig.urlSearchSolutionsByName;
    /**
     * active:true  => fetching active solutions
     * active:false => fetching deleted solutions
     */
    const request = {
      request_body: {
        description: 'exactSearch',
        searchTerm: modelName,
      },
    };

    return this._httpSharedService.post(url, undefined, request).pipe(
      map((res) => ({
        publicSolutions: res.response_body.content,
        totalElements: res.response_body.totalElements,
      })),
      catchError((error) => {
        throw error;
      }),
    );
  }

  deleteSolution(solution: PublicSolutionDetailsModel, revisionId: string) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlSolutions +
      '/' +
      solution.solutionId +
      '/' +
      revisionId;

    let body = {
      request_body: {
        active: false,
        created: solution.created,
        name: solution.name,
        ownerId: solution.ownerId,
        solutionId: solution.solutionId,
        revisionId,
        tookitType: null,
        modelType: null,
      },
    };

    return this._httpSharedService.put(url, undefined, body).pipe(
      tap(),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getAllAvailableCatalogs(): Observable<Catalog[]> {
    const request = {
      request_body: {
        fieldToDirectionMap: {
          created: 'DESC',
        },
        page: 0,
        size: 10000,
      },
      request_from: 'string',
      request_id: 'string',
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlAllCatalogsList;

    return this._httpSharedService.post(url, undefined, request).pipe(
      map((res) => res.response_body.content),
      catchError((error) => {
        throw error;
      }),
    );
  }

  updateCompanyDescription(
    description: string,
    solutionId: string,
    revisionId: string,
    selectedCatalogId: string,
  ) {
    const request = {
      request_body: {
        description,
      },
    };
    const url =
      apiConfig.apiBackendURL +
      '/api/solution/revision/' +
      solutionId +
      '/' +
      revisionId +
      '/' +
      selectedCatalogId +
      '/description';

    return this._httpSharedService.post(url, undefined, request).pipe(
      map((res) => res.response_body.description),
      catchError((error) => {
        throw error;
      }),
    );
  }

  /**
   * update solution name or model category(modelType) or toolkitType
   * A solution toolkitType could not be updated without select a modelType beforehand and vice versa
   * @param solution
   * @returns model details
   */

  updateSolution(solution: UpdateSolutionRequestPayload) {
    const req = {
      request_body: {
        solution,
      },
    };
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlSolutions +
      '/' +
      solution.solutionId;

    return this._httpSharedService.put(url, undefined, req).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  createTags(tag: string) {
    const req = {
      request_body: {
        tag,
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlCreateTags;

    return this._httpSharedService.post(url, undefined, req).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  deleteTag(tag: string, solutionId: string) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlDeleteTag +
      '/' +
      solutionId +
      '/tag/' +
      tag;

    return this._httpSharedService.delete(url).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  addTag(tag: string, solutionId: string) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlAddTag +
      '/' +
      solutionId +
      '/tag/' +
      tag;

    return this._httpSharedService.put(url).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  getAllTag(): Observable<string[]> {
    const url = apiConfig.apiBackendURL + apiConfig.urlGetAllTag;

    const req = {
      request_body: {
        page: 0,
      },
    };

    return this._httpSharedService.put(url, undefined, req).pipe(
      map((res) => res.response_body.tags),
      catchError((error) => {
        throw error;
      }),
    );
  }

  setSolutionPicture(solutionId: string, image: File) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlSolutions +
      '/' +
      solutionId +
      '/picture';
    const formData = new FormData();
    formData.append('file', image);
    return this._httpSharedService.post(url, undefined, formData).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  updateSolutionFiles(
    solutionId: string,
    revisionId: string,
    selectedCatalogId: string,
    file: File,
  ): Observable<DocumentModel> {
    const uploadUrl =
      apiConfig.apiBackendURL +
      '/api/solution/' +
      solutionId +
      '/revision/' +
      revisionId +
      '/' +
      selectedCatalogId +
      '/document';
    const formData = new FormData();
    formData.append('file', file);

    return this._httpSharedService.post(uploadUrl, undefined, formData).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getSolutionsFiles() {}

  deleteSolutionsFiles(
    solutionId: string,
    revisionId: string,
    selectedCatalogId: string,
    documentId: string,
  ) {
    const url =
      apiConfig.apiBackendURL +
      '/api/solution/' +
      solutionId +
      '/revision/' +
      revisionId +
      '/' +
      selectedCatalogId +
      '/document/' +
      documentId;

    return this._httpSharedService.delete(url).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  publishSolution(
    solutionId: string,
    visibility: string,
    userId: string,
    revisionId: string,
    ctlg: string,
  ) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlPublishSolution +
      '/' +
      solutionId +
      '?';

    const data = {
      visibility,
      userId,
      revisionId,
      ctlg,
    };

    return this._httpSharedService.put(url, data, undefined).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  searchPublishRequestWithCatalogIds(
    revisionId: string,
    catalogId: string,
  ): Observable<PublishSolutionRequest> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlSearchPublishRequest +
      '/' +
      revisionId +
      '/' +
      catalogId;

    return this._httpSharedService.get(url, undefined).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  withdrawPublishRequest(
    publishRequestId: number,
  ): Observable<PublishSolutionRequest> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.withdrawPublishRequestUrl +
      publishRequestId;
    return this._httpSharedService.put(url).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  loadCatalogs(pageNumber: number, totalCatalogsElements: number) {
    const request = {
      request_body: {
        fieldToDirectionMap: {
          created: 'ASC',
        },
        page: pageNumber,
        size: totalCatalogsElements,
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlAllCatalogsList;

    return this._httpSharedService.post(url, undefined, request).pipe(
      //map((res) => res.response_body.content),
      catchError((error) => {
        throw error;
      }),
    );
  }

  loadTotalCatalog(): Observable<number> {
    const request = {
      request_body: {
        fieldToDirectionMap: {
          created: 'ASC',
        },
        page: 0,
        size: 1,
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlAllCatalogsList;

    return this._httpSharedService.post(url, undefined, request).pipe(
      map((res) => res.response_body.totalElements),
      catchError((error) => {
        throw error;
      }),
    );
  }

  createCatalog(catalog: Catalog) {
    const requestObj = {
      request_body: {
        ...catalog,
        url: 'http://localhost',
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlCatalogPath;

    return this._httpSharedService.post(url, undefined, requestObj).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  updateCatalog(catalog: Catalog) {
    const requestObj = {
      request_body: {
        ...catalog,
        url: 'http://localhost',
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlCatalogPath;

    return this._httpSharedService.put(url, undefined, requestObj).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  deleteCatalog(catalog: Catalog) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlCatalogPath +
      '/' +
      catalog.catalogId;

    return this._httpSharedService.delete(url).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }

  getSiteConfig() {
    const url =
      apiConfig.apiBackendURL + apiConfig.urlSiteConfig + '/site_config';

    return this._httpSharedService.get(url, undefined).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }
}
