import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDetails } from 'src/app/shared/models';

@Injectable({
  providedIn: 'root',
})
export class BrowserStorageService {
  private userDetailSource = new BehaviorSubject<UserDetails | null>(
    this.loadInitialUserDetails(),
  );
  private authTokenSource = new BehaviorSubject<string | null>(
    localStorage.getItem('auth_token') || null,
  );
  constructor() {}

  private loadInitialUserDetails(): UserDetails | null {
    const userDetailString =
      localStorage.getItem('userDetail') ||
      sessionStorage.getItem('userDetail');
    return userDetailString ? JSON.parse(userDetailString) : null;
  }

  getUserDetails(): Observable<UserDetails | null> {
    return this.userDetailSource.asObservable();
  }

  setUserDetail(userDetail: UserDetails): void {
    const storage = sessionStorage.getItem('rm')
      ? sessionStorage
      : localStorage;
    storage.setItem('userDetail', JSON.stringify(userDetail));
    this.userDetailSource.next(userDetail);
  }

  removeUserDetail(): void {
    sessionStorage.removeItem('userDetail');
    localStorage.removeItem('userDetail');
    this.userDetailSource.next(null);
  }

  setUserRole(role: string): void {
    const storage = sessionStorage.getItem('rm')
      ? sessionStorage
      : localStorage;
    storage.setItem('userRole', role);
    document.cookie = 'userRole=' + role;
  }

  setAuthToken(token: string): void {
    const storage = sessionStorage.getItem('rm')
      ? sessionStorage
      : localStorage;
    storage.setItem('auth_token', token);
    document.cookie = 'authToken= Bearer ' + token;
  }

  getUserRole(): string | null {
    return (
      sessionStorage.getItem('userRole') ?? localStorage.getItem('userRole')
    );
  }

  setAdmin(admin: string): void {
    const storage = sessionStorage.getItem('rm')
      ? sessionStorage
      : localStorage;
    storage.setItem('isAdmin', admin);
  }

  setLicenseAdmin(licenseAdmin: string): void {
    const storage = sessionStorage.getItem('rm')
      ? sessionStorage
      : localStorage;
    storage.setItem('isLicenseAdmin', licenseAdmin);
  }

  setPublisher(publisher: string): void {
    const storage = sessionStorage.getItem('rm')
      ? sessionStorage
      : localStorage;
    storage.setItem('isPublisher', publisher);
  }

  isAdmin(): string | null {
    return sessionStorage.getItem('isAdmin') ?? localStorage.getItem('isAdmin');
  }

  isLicenseAdmin(): string | null {
    return (
      sessionStorage.getItem('isLicenseAdmin') ??
      localStorage.getItem('isLicenseAdmin')
    );
  }

  isPublisher(): string | null {
    return (
      sessionStorage.getItem('isPublisher') ??
      localStorage.getItem('isPublisher')
    );
  }

  getAuthToken(): string | null {
    return (
      sessionStorage.getItem('auth_token') ?? localStorage.getItem('auth_token')
    );
  }

  clearUserRole(): void {
    sessionStorage.setItem('userRole', '');
    localStorage.setItem('userRole', '');
  }

  removeAuthToken(): void {
    sessionStorage.removeItem('auth_token');
    localStorage.removeItem('auth_token');
  }

  setMktPlaceStorage(obj: any): void {
    sessionStorage.setItem('mktPlaceStorage', JSON.stringify(obj));
  }

  getMktPlaceStorage(): any {
    return JSON.parse(sessionStorage.getItem('mktPlaceStorage') ?? '{}');
  }

  removeMktPlaceStorage(): void {
    sessionStorage.removeItem('mktPlaceStorage');
  }

  setFavCatStorage(obj: any): void {
    sessionStorage.setItem('favCatStorage', JSON.stringify(obj));
  }

  getFavCatStorage(): any {
    return JSON.parse(sessionStorage.getItem('favCatStorage') ?? '{}');
  }

  removeFavCatStorage(): void {
    sessionStorage.removeItem('favCatStorage');
  }
}
