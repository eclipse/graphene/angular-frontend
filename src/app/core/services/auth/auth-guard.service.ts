import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  createUrlTreeFromSnapshot,
} from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

export const authGuard = (next: ActivatedRouteSnapshot) => {
  const service = inject(AuthService);
  if (environment.skipAuth) {
    return true;
  } else {
    return service.isAuthenticatedUser()
      ? true
      : createUrlTreeFromSnapshot(next, ['/', '']);
  }
};

export const adminAuthGuard = (next: ActivatedRouteSnapshot) => {
  const service = inject(AuthService);
  return service.isAuthenticatedUser()
    ? true
    : createUrlTreeFromSnapshot(next, ['/', '']);
};
