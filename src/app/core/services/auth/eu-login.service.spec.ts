import { TestBed } from '@angular/core/testing';

import { EuLoginService } from './eu-login.service';

describe('EuLoginService', () => {
  let service: EuLoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EuLoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
