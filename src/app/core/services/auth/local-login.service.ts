import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpSharedService } from '../../http-shared/http-shared.service';
import { apiConfig } from 'src/app/core/config';
import { Observable, map } from 'rxjs';
import { UserDetails } from '../../../shared/models';

@Injectable({
  providedIn: 'root',
})
export class LocalLoginService {
  constructor(
    private authService: AuthService,
    private httpSharedService: HttpSharedService,
  ) {}

  login(
    username: string,
    password: string,
  ): Observable<{
    isAuthenticated: boolean;
    authToken: string;
  }> {
    const url = apiConfig.apiBackendURL + apiConfig.authorization;
    return this.httpSharedService
      .post(url, undefined, {
        request_body: { username: username, password: password },
      })
      .pipe(map((response) => this.authService.loginUser(response.jwtToken)));
  }
  requestPasswordReset(email: string) {
    const url = apiConfig.apiBackendURL + apiConfig.forgetPasswordURL;
    return this.httpSharedService.put(url, undefined, {
      request_body: { emailId: email },
    });
  }

  register(userDetails: UserDetails) {
    const url = apiConfig.apiBackendURL + apiConfig.signUpURL;

    return this.httpSharedService.post(url, undefined, {
      request_body: userDetails,
    });
  }
}
