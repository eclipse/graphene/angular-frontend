import { Injectable } from '@angular/core';
import { LocalStorageService } from 'src/app/core/services/storage/localstorage.service';
import { JwtTokenService } from './jwt-token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuthenticated = false;
  private authToken!: string | null;
  public authSecretKey = 'BearerToken';

  constructor(
    private localStorageService: LocalStorageService,
    private jwtTokenService: JwtTokenService,
  ) {
    // getting current token from the browser storage.
    this.getAuthToken();
    // checking if the token is available and check its expirery day.
    if (this.authToken) {
      this.jwtTokenService.setToken(this.authToken);

      // checking from backend if the token is not inspired
      if (!this.jwtTokenService.isTokenExpired()) {
        this.isAuthenticated = this.checkAuthToken();
      }
    }
  }

  loginUser(authToken: string): {
    isAuthenticated: boolean;
    authToken: string;
  } {
    this.isAuthenticated = true;
    this.authToken = authToken;
    this.localStorageService.setItem(this.authSecretKey, authToken);
    return { authToken, isAuthenticated: this.isAuthenticated };
  }

  getAuthToken() {
    this.authToken = this.localStorageService.getItem(this.authSecretKey);
  }

  /*
  This functions checks in the backend if the user is authenticated 
  TODO: Write the logic for the function
  */
  checkAuthToken() {
    return true;
  }

  isAuthenticatedUser(): boolean {
    return this.isAuthenticated;
  }

  logout(): void {
    this.localStorageService.removeItem(this.authSecretKey);
    this.localStorageService.removeItem('userDetails');
    this.isAuthenticated = false;
  }

  isLoggedIn(): boolean {
    return (
      !!this.authToken &&
      this.isAuthenticated &&
      !this.jwtTokenService.isTokenExpired()
    );
  }
}
