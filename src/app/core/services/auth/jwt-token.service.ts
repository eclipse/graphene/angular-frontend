import { Injectable } from '@angular/core';
import { jwtDecode } from 'jwt-decode';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { UserDetails } from 'src/app/shared/models';

interface DecodedToken {
  sub: string;
  mlpuser: {
    firstName: string;
    userId: string;
    email: string;
    lastName: string;
  };
  exp: number;
}

@Injectable({
  providedIn: 'root',
})
export class JwtTokenService {
  private jwtToken: BehaviorSubject<string | null> = new BehaviorSubject<
    string | null
  >(null);
  private decodedToken: BehaviorSubject<DecodedToken | null> =
    new BehaviorSubject<DecodedToken | null>(null);

  constructor() {}

  setToken(token: string): void {
    this.jwtToken.next(token); // Store the raw token
    try {
      const decoded: DecodedToken = jwtDecode<DecodedToken>(token); // Decode the token
      this.decodedToken.next(decoded); // Store the decoded token
    } catch (error) {
      console.error('Failed to decode token', error);
      this.decodedToken.next(null); // Reset or handle decoding error
    }
  }

  getToken(): Observable<string | null> {
    return this.jwtToken.asObservable();
  }

  getDecodedToken(): Observable<DecodedToken | null> {
    return this.decodedToken.asObservable();
  }

  getUserDetailsObservable(): Observable<UserDetails | null> {
    return this.getDecodedToken().pipe(
      map((token) =>
        token
          ? {
              username: token.sub,
              userId: token.mlpuser.userId,
              firstName: token.mlpuser.firstName,
              emailId: token.mlpuser.email,
              lastName: token.mlpuser.lastName,
            }
          : null,
      ),
    );
  }

  getExpiryTime(): number | null {
    return this.decodedToken.getValue()?.exp || null;
  }

  isTokenExpired(): boolean {
    const currentTimeInSeconds = Math.floor(new Date().getTime() / 1000);
    const expiryTimeInSeconds = this.getExpiryTime();
    return expiryTimeInSeconds !== null
      ? expiryTimeInSeconds < currentTimeInSeconds
      : true;
  }
}
