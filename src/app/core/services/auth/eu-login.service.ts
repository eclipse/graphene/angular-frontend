import { Injectable } from '@angular/core';
import { LocalStorageService } from '../storage/localstorage.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class EuLoginService {
  constructor(
    private localStorageService: LocalStorageService,
    private authService: AuthService,
  ) {}

  login(username: string, password: string) {
    const casLoginUrl = 'https://ecas.ec.europa.eu/cas/login?service=';
    const retUrl = window.location.origin;
    window.open(casLoginUrl + retUrl);

    if (username === 'test' && password === 'test') {
      const authToken =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpheWRlZXAgUGF0aWwiLCJpYXQiOjE1MTYyMzkwMjJ9.yt3EOXf60R62Mef2oFpbFh2ihkP5qZ4fM8bjVnF8YhA'; // Generate or receive the token from your server
      this.localStorageService.set(this.authService.authSecretKey, authToken);
    }
  }
}
