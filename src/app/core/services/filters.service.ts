import { Injectable } from '@angular/core';
import { Observable, catchError, map, tap } from 'rxjs';
import { apiConfig } from '../config';
import { HttpSharedService } from '../http-shared/http-shared.service';
import { CatalogFilter } from 'src/app/shared/models';

@Injectable({
  providedIn: 'root',
})
export class FiltersService {
  constructor(private readonly _httpSharedService: HttpSharedService) {}

  getModelTypes(): Observable<CatalogFilter[]> {
    const url = apiConfig.apiBackendURL + apiConfig.urlModelTypes;
    return this._httpSharedService.get(url, undefined).pipe(
      tap(),
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getToolkitTypes(): Observable<CatalogFilter[]> {
    const url = apiConfig.apiBackendURL + apiConfig.urlToolkitTypes;
    return this._httpSharedService.get(url, undefined).pipe(
      tap(),
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }
}
