import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  AuthorPublisherModel,
  LicenseProfileModel,
  PublicSolution,
  Revision,
} from 'src/app/shared/models';

@Injectable({
  providedIn: 'root',
})
export class SharedDataService {
  private _versionIdSubject: BehaviorSubject<string> =
    new BehaviorSubject<string>('');
  private _selectedCatalogIdSubject: BehaviorSubject<string> =
    new BehaviorSubject<string>('');
  private _solutionSubject: BehaviorSubject<PublicSolution> =
    new BehaviorSubject<PublicSolution>({
      name: '',
      ownerName: '',
      active: false,
      sourceId: '',
      authors: [],
      mPeer: { name: '' },
      modified: '',
      created: '',
      solutionRatingAvg: 0,
      commentsCount: 0,
      viewCount: 0,
      downloadCount: 0,
      selectFav: '',
      solutionId: '',
      solutionTagList: [],
      latestRevisionId: '',
      modelTypeName: '',
      modelType: '',
      tookitType: '',
      tookitTypeName: '',
      revisions: [],
      catalogNames: [],
      publisher: '',
      ownerId: '',
      ownerListForSol: [],
    });
  private _imageToShowSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null,
  );

  private _authorListSubject: BehaviorSubject<AuthorPublisherModel[]> =
    new BehaviorSubject<AuthorPublisherModel[]>([]);

  private _selectedRevisionSubject: BehaviorSubject<Revision> =
    new BehaviorSubject<Revision>({
      version: '',
      revisionId: '',
      onBoarded: '',
    });

  private _ratingSubject: BehaviorSubject<number> = new BehaviorSubject<number>(
    0,
  );

  private _licenseProfileSubject =
    new BehaviorSubject<LicenseProfileModel | null>(null);

  constructor() {}

  get versionId$(): Observable<string> {
    return this._versionIdSubject.asObservable();
  }

  set versionId(value: string) {
    this._versionIdSubject.next(value);
  }

  get selectedCatalogId$(): Observable<string> {
    return this._selectedCatalogIdSubject.asObservable();
  }

  set selectedCatalogId(value: string) {
    this._selectedCatalogIdSubject.next(value);
  }

  get solution$(): Observable<PublicSolution> {
    return this._solutionSubject.asObservable();
  }

  set solution(value: PublicSolution) {
    this._solutionSubject.next(value);
  }

  get imageToShow$(): Observable<any> {
    return this._imageToShowSubject.asObservable();
  }

  set imageToShow(value: any) {
    this._imageToShowSubject.next(value);
  }

  get authorList$(): Observable<AuthorPublisherModel[]> {
    return this._authorListSubject.asObservable();
  }

  get authorListValue(): AuthorPublisherModel[] {
    return this._authorListSubject.getValue();
  }

  set authorList(value: AuthorPublisherModel[]) {
    this._authorListSubject.next(value);
  }

  get selectedRevision$(): Observable<Revision> {
    return this._selectedRevisionSubject.asObservable();
  }

  set selectedRevision(revision: Revision) {
    this._selectedRevisionSubject.next(revision);
  }

  get rating$(): Observable<number> {
    return this._ratingSubject.asObservable();
  }

  set rating(value: number) {
    this._ratingSubject.next(value);
  }

  get licenseProfile$(): Observable<LicenseProfileModel | null> {
    return this._licenseProfileSubject.asObservable();
  }

  set licenseProfile(value: LicenseProfileModel | null) {
    this._licenseProfileSubject.next(value);
  }
}
