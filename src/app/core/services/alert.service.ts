// alert.service.ts
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { AlertComponent } from '../../shared/components/alert/alert.component';
import { Alert } from '../../shared/models/alert.model';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private snackBar: MatSnackBar) {}

  notify(alert: Alert, duration?: number): void {
    const config: MatSnackBarConfig<Alert> = {
      duration: duration,
      verticalPosition: 'top',
      panelClass: [alert.type],
      data: alert,
    };

    this.snackBar.openFromComponent(AlertComponent, config);
  }
}
