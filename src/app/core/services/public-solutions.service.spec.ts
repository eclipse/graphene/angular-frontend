import { TestBed } from '@angular/core/testing';

import { PublicSolutionsService } from './public-solutions.service';

describe('PublicSolutionsService', () => {
  let service: PublicSolutionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PublicSolutionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
