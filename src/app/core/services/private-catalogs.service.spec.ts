import { TestBed } from '@angular/core/testing';

import { PrivateCatalogsService } from './private-catalogs.service';

describe('PrivateCatalogsService', () => {
  let service: PrivateCatalogsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrivateCatalogsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
