import { Injectable } from '@angular/core';
import { apiConfig } from 'src/app/core/config';
import { HttpSharedService } from '../http-shared/http-shared.service';
import { Observable, catchError, map, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {
  AllUserRating,
  ArtifactDownloadModel,
  AuthorPublisherModel,
  AverageRatings,
  Catalog,
  DocumentModel,
  LicenseProfileModel,
  PublicSolution,
  PublicSolutionDetailsModel,
  PublicSolutionsRequestPayload,
} from '../../shared/models';

@Injectable({
  providedIn: 'root',
})
export class PublicSolutionsService {
  constructor(
    public _httpSharedService: HttpSharedService,
    private http: HttpClient,
  ) {}

  getPublicSolutions(): Observable<PublicSolution[]> {
    let accessTypeFilter = ['PB'];

    let body = {
      request_body: {
        sortBy: 'MR',
        active: true,
        accessTypeCodes: accessTypeFilter,
        pageRequest: {
          fieldToDirectionMap: { modified: 'DESC' },
          page: 0,
          size: 8,
        },
      },
    };

    const url = apiConfig.apiBackendURL + apiConfig.urlPublicPortalSolutions;

    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      map((response) => response.response_body.content),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getPictureOfSolution(solutionId: string): Observable<Blob> {
    const url =
      apiConfig.apiBackendURL + apiConfig.getPublicSolutionPicture(solutionId);
    return this.http.get(url, { responseType: 'blob' }).pipe(tap());
  }

  getPublicCatalogs(): Observable<Catalog[]> {
    let body = {
      request_body: {
        fieldToDirectionMap: {
          created: 'DESC',
        },
        page: 0,
        size: 1000,
      },
      request_from: 'string',
      request_id: 'string',
    };

    const url = apiConfig.apiBackendURL + apiConfig.urlPublicCatalogs;

    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      map((res) => res.response_body.content),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getPublicSolutionsOfPublicCatalogs(
    requestPayload: PublicSolutionsRequestPayload,
  ): Observable<{ publicSolutions: PublicSolution[]; totalElements: number }> {
    let body = {
      request_body: {
        modelTypeCodes: requestPayload.modelTypeCodes,
        active: true,
        catalogIds: requestPayload.catalogIds,
        accessTypeCodes: ['PB'],
        nameKeyword: requestPayload.nameKeyword,
        sortBy: requestPayload.sortBy,
        tags: requestPayload.tags,
        published: true,
        pageRequest: requestPayload.pageRequest,
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlPublicPortalSolutions;
    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      map((res) => ({
        publicSolutions: res.response_body.content,
        totalElements: res.response_body.totalElements,
      })),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getSolutionDetails(
    solutionId: string,
    revisionId: string,
  ): Observable<PublicSolutionDetailsModel> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetSolutionDetails(solutionId, revisionId);

    return this._httpSharedService.get(url, undefined).pipe(
      tap(),
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getCatalogsOfSolution(solutionId: string): Observable<Catalog[]> {
    const url =
      apiConfig.apiBackendURL + apiConfig.urlGetCatalogsOfSolution(solutionId);

    return this._httpSharedService.get(url, undefined).pipe(
      map((response) => response.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getModelAuthors(
    solutionId: string,
    revisionId: string,
  ): Observable<AuthorPublisherModel[]> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetModelAuthors(solutionId, revisionId);

    return this._httpSharedService.get(url, undefined).pipe(
      tap(),
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getAllRatings(solutionId: string): Observable<AllUserRating[]> {
    let body = {
      request_body: {
        page: 0,
        size: 0,
      },
    };
    const url = apiConfig.apiBackendURL + apiConfig.urlGetRating + solutionId;

    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      map((resp) => resp.response_body.content),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getComment(solutionId: string, revisionId: string) {
    let body = {
      request_body: {
        page: 0,
        size: 100,
      },
    };
    const url =
      apiConfig.apiBackendURL + apiConfig.urlGetComment(solutionId, revisionId);

    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getArtifacts(
    solutionId: string,
    revisionId: string,
  ): Observable<ArtifactDownloadModel[]> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetArtifacts(solutionId, revisionId);

    return this._httpSharedService.get(url, undefined).pipe(
      tap(),
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getAverageRatings(solutionId: string): Observable<AverageRatings> {
    const url = apiConfig.apiBackendURL + apiConfig.urlAvgRating + solutionId;

    return this._httpSharedService.get(url, undefined).pipe(
      tap(),
      map((resp) => resp.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getModelSignature(solutionId: string, versionId: string) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetModelSignature(solutionId, versionId);

    return this.http.get(url, { responseType: 'text' }).pipe(tap());
  }

  getSolutionDocuments(
    solutionId: string,
    revisionId: string,
    selectedCatalogId: string,
  ): Observable<DocumentModel[]> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetSolutionDocuments(
        solutionId,
        revisionId,
        selectedCatalogId,
      );

    return this._httpSharedService.get(url, undefined).pipe(
      map((res) => res.response_body),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getLicenseFile(
    solutionId: string,
    versionId: string,
  ): Observable<LicenseProfileModel> {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetLicenseFile(solutionId, versionId);

    return this._httpSharedService.get(url, undefined).pipe(
      tap(),
      catchError((error) => {
        throw error;
      }),
    );
  }

  relatedSolutions(solution: PublicSolution): Observable<PublicSolution[]> {
    let body = {
      request_body: {
        modelType: solution.modelType,
        page: 0,
        size: 6,
      },
      request_from: 'string',
      request_id: 'string',
    };
    const url = apiConfig.apiBackendURL + apiConfig.getRelatedMySolutions;

    return this._httpSharedService.post(url, undefined, body).pipe(
      tap(),
      map((res) => res.response_body.content),
      catchError((error) => {
        throw error;
      }),
    );
  }

  updateViewCount(solutionId: string) {
    const url =
      apiConfig.apiBackendURL + apiConfig.urlUpdateViewCount + '/' + solutionId;

    return this._httpSharedService.put(url, undefined).pipe(
      tap(),
      catchError((error) => {
        throw error;
      }),
    );
  }

  getSolutionDescription(revisionId: string, selectedCatalogId: string) {
    const url =
      apiConfig.apiBackendURL +
      apiConfig.urlGetSolutionDescription(revisionId, selectedCatalogId);

    return this._httpSharedService.get(url, undefined).pipe(
      catchError((error) => {
        throw error;
      }),
    );
  }
}
