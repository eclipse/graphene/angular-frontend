import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HttpSharedService {
  constructor(private http: HttpClient) {}

  private _convertQueryParams(queryParams?: any): HttpParams {
    let params = new HttpParams();
    if (queryParams) {
      // Iterate over object keys
      Object.keys(queryParams).forEach((key) => {
        // Check if the property is indeed a property of queryParams
        if (queryParams.hasOwnProperty(key)) {
          // Add each key to the HttpParams object
          params = params.set(key, queryParams[key]);
        }
      });
    }
    return params;
  }

  get(url: string, queryParams?: any): Observable<any> {
    let params;
    if (queryParams) {
      params = this._convertQueryParams(queryParams);
    }
    return this.http.get(url, { params });
  }

  post(url: string, queryParams?: any, body?: any): Observable<any> {
    let params;
    if (queryParams) {
      params = this._convertQueryParams(queryParams);
    }

    return this.http.post(url, body, { params });
  }

  put(url: string, queryParams?: any, body?: any): Observable<any> {
    let params;
    if (queryParams) {
      params = this._convertQueryParams(queryParams);
    }
    return this.http.put(url, body, { params });
  }

  delete(url: string, queryParams?: any): Observable<any> {
    let params;
    if (queryParams) {
      params = this._convertQueryParams(queryParams);
    }
    return this.http.delete(url, { params });
  }
}
