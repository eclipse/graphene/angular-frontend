export const environment = {
  // local with session
  production: false,
  skipAuth: false,
  apiBackendURL: 'https://dev02.ki-lab.nrw',
  isDebugMode: true,
  logging: {
    level: {
      root: 'INFO',
    },
  },
  server: {
    port: 9200,
  },
  qanda: {
    url: 'https://stackoverflow.com/questions/tagged/acumos',
  },
  ui_system_config: {
    deploy_menu: [
      {
        title: 'Local Kubernetes',
        icon_url: '/images/kubernetes-local.svg',
        local_service_path: '/package/getSolutionZip/',
        produces_download: true,
      },
      {
        title: 'KI-Lab Playground',
        icon_url: '/images/kinrw-pg.svg',
        local_service_path: '/ki-lab-playground-deployer/deploy/',
        produces_download: false,
      },
    ],
    marketplace_sort_by: 'MR',
  },
};
