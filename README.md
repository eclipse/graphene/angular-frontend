# GrapheneUi

This project builds on Angular and 18 makes use of the new standalone components that were recently introduced to simplify component re-use.   

## Development server

Run `npm run ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `npm run ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Making Docker and running it for production

Build an image with:

`docker build -t <image name> .`

and run that image with (in this case on port 8080):

`docker run -p 8080:80 <image name>`

access the frontend under:
`http://localhost:8080/`

## Running unit tests

Run `npm run ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `npm run ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Running container

build the image

`docker build -t cicd.ai4eu-dev.eu:7444/graphene/anuglar-frontend:latest .`

Push the image
`docker push cicd.ai4eu-dev.eu:7444/graphene/anuglar-frontend:latest`

Run the image detached mode on port 7659
`docker run  -d -p 7659:80 cicd.ai4eu-dev.eu:7444/graphene/anuglar-frontend:latest`
docker run -p 7659:80 cicd.ai4eu-dev.eu:7444/graphene/anuglar-frontend:latest

docker run -p 7659:80 -it cicd.ai4eu-dev.eu:7444/graphene/anuglar-frontend:latest /bin/bash